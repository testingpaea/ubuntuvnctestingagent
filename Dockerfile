FROM rcgcoder/ubuntu-vnc:amd64
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y nano git imagemagick iputils-ping

COPY resources/chromiumGatewayConfig.tgz /tmp/chromiumGatewayConfig.tgz

COPY nodeapp/ /usr/src/app/
WORKDIR "/usr/src/app"

COPY runcontainer_vnctestingagent /usr/bin/runcontainer_vnctestingagent
RUN chmod 777 -R /usr/bin/runcontainer_vnctestingagent

COPY containervnctestingagent-setup.sh /usr/bin/containervnctestingagent-setup.sh
RUN chmod 777 -R /usr/bin/containervnctestingagent-setup.sh


ENTRYPOINT ["/bin/bash", "-c", "/usr/bin/runcontainer_vnctestingagent"]