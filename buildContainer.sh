#!/bin/bash

export containername=$1
#export archType=$2
#export version=$3
#export containername="ubuntuvnctestingagent"
#export containername="ubuntuvncnodejs"
#export containername="ubuntuvncautofirma"
#export containername="ubuntuvncsmartcards"
#export containername="ubuntuvncbrowsers"
#export containername="ubuntuvncserver"

export archType="amd64"
export version="latest"

# --no-cache --force-rm
 
docker build --platform linux/$archType \
                https://saedga@bitbucket.org/testingpaea/$containername.git \
                -t saearagon/$containername:$version

docker push saearagon/$containername:$version

sleep 10

#docker manifest rm saearagon/$containername:$version
	
docker manifest create saearagon/$containername:$version

#docker manifest create saearagon/$containername:manifest-latest \
#                --amend saearagon/$containername:$version\
#                --amend saearagon/$containername:$archType

docker manifest push saearagon/$containername:$version
