var {CucumberMessage,AgentMessage} = require('./message');
const ScStore = require('kimbo/scenarioStore');
const agentManager=require('../agentManager/agentManager');
var MessageHandler=class MessageHandler{
	constructor(){
		var self=this;
		self.enabled=(typeof process.send==="function");
		self.iCount=0;
		self.receptorId="";
		self.receptorName="";
		self.listTests=[];
		self.totalScreens=0;
		self.eventHandlers=new Map();
		self.promises=new Map();
		self.receivedMessagesList=[];
		self.processingMessage="";

		if (self.enabled){
			process.on("message",(msg)=>{
				self.processMessage(msg);
				if ((typeof msg.responseOf!=="undefined")&&(msg.responseOf!=="")){
					//console.log("PREVIOUS:"+JSON.stringify(self.promises));
					if (typeof self.promises.has(msg.responseOf)){
						var fncResolve=self.promises.get(msg.responseOf).resolve;
						self.promises.delete(msg.responseOf);
						fncResolve(msg);
					}
				}
				
			});
		}

	}
	setIdentification(receptorId,receptorName){
		var self=this;
		self.receptorId=receptorId;
		self.receptorName=receptorName;
	}
	addEventHandler(action,fncHandler){
		this.eventHandlers.set(action,fncHandler);
	}
	processMessage(msg){
		var self=this;
//		console.log(self.receptorId+" Received Message:"+JSON.stringify(msg));
		if (typeof msg.action==="undefined"){
			return;
		}
		debugger;
		if (self.eventHandlers.has(msg.action)){
			if (self.processingMessage!==""){
				self.receivedMessagesList.push(msg);
			} else {
				self.processingMessage=msg;
				self.eventHandlers.get(msg.action)(msg.value);
			}
		}
	}
	finishProcessingActualMessage(){
		var self=this;
		debugger;
		if (self.receivedMessagesList.length>0){
			var msg=self.receivedMessagesList.shift();
			self.processingMessage=msg;
			self.eventHandlers.get(msg.action)(msg.value);
		} else {
			self.processingMessage="";
		}
	}
	
	sendMessage(action,value,haveResponse){
		var self=this;
		if (self.enabled){
			var prmResult=new Promise((resolve, reject) => {
				var msg=new CucumberMessage(action,value);
				if ((typeof haveResponse==="undefined")||(haveResponse)){
					self.promises.set(msg.id,{message:msg,resolve:resolve,reject:reject});
				} else {
					resolve("sended");
				}
				debugger;
				process.send(msg);
			});
			return prmResult;
		} 
	}
}

var messageHandler=new MessageHandler();
module.exports=messageHandler;