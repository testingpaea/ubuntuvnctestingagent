'use strict';

var testingMessage=class testingMessage{
	constructor(direction,action,value,responseOf){
		var self=this;
		self.direction=direction;
		self.id=direction+"->"+(new Date()).getTime();
		self.action=action;
		self.value=value;
		self.responseOf="";
		if (typeof responseOf!=="undefined"){
			self.responseOf=responseOf.id;
		}
	}
}

var AgentMessage=class AgentMessage extends testingMessage{
	constructor(action,value,responseOf){
		super("ACC2Agent",action,value,responseOf);
	}
}
var CucumberMessage=class CucumberMessage extends testingMessage{
	constructor(action,value,responseOf){
		super("Agent2ACC",action,value,responseOf);
	}
}
module.exports={
	AgentMessage:AgentMessage,
	CucumberMessage:CucumberMessage
}