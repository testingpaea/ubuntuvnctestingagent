const ScStore = require('kimbo/scenarioStore');
const agentSelectors = require('./agentSelectors_extension.lib');

const stepDefinitions = [
    {   stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add agent selector {string} with',
		stepFunction: async function(selectorId,jsString){
			debugger;			
			console.log("add selector "+selectorId);
			console.log("JS code:\n"+jsString);
			
			var objTags=ScStore.getActualTags();
			var sSelectorJS=await ScStore.processExpression(jsString);
			var pSelectorId=await ScStore.processExpression(selectorId);
			var oInputJson={id:pSelectorId,selectorJS:sSelectorJS};
			agentSelectors.addFromJson(objTags,oInputJson);
			return;
		}
	},{
		stepMethod: 'Given',
		stepPattern: 'the scenario {string} using another agent where',
		stepTimeout: 40000,
		stepFunction: async function(inScenarioName,sCallInfo){
			debugger;
			var oCallInfo=JSON.parse(sCallInfo);
			var selector="";
			if (typeof oCallInfo.CALL_INFO.agentSelectorID!=="undefined"){
				var selectorId=oCallInfo.CALL_INFO.agentSelectorID;
				var auxAgentSelector=agentSelectors.getById(selectorId);
				var selectorJS=auxAgentSelector.selectorJS;
				selector=selectorJS;
			} else {
				selector=oCallInfo.CALL_INFO.agentSelectorJSLines;
			}
			var environment="";
			if (typeof oCallInfo.CALL_INFO.environment==="undefined"){
				//use the same environment of actual scenario
				environment=undefined;
			} else {
				environment=await ScStore.processExpression(oCallInfo.CALL_INFO.environment);
			}
			var application="";
			if (typeof oCallInfo.CALL_INFO.application==="undefined"){
				//use the same environment of actual scenario
				application=undefined;
			} else {
				application=await ScStore.processExpression(oCallInfo.CALL_INFO.application);
			}
			
			var scenarioName=await ScStore.processExpression(inScenarioName);
			var agentSelectorJS=await ScStore.processExpression(selector);
			var dResult=await agentSelectors.executeDeferredCall(scenarioName,agentSelectorJS,oCallInfo.PARAMS,environment,application);
			debugger;
			console.log("Deferred Call result:"+JSON.stringify(dResult));
		}
	},{
		stepMethod: 'Given',
		stepPattern: 'the scenario {string} using agent selector {string} where',
		stepTimeout: 40000,
		stepFunction: async function(scenarioName,agentSelectorId,sCallInfo){
			debugger;
			var oCallInfo=JSON.parse(sCallInfo);
			var selector="";
			var auxAgentSelector=agentSelectors.getById(agentSelectorId);
			var selectorJS=auxAgentSelector.selectorJS;
			selector=selectorJS;			
			var scenarioName=await ScStore.processExpression(inScenarioName);
			var agentSelectorJS=await ScStore.processExpression(selector);
			var dResult=await agentSelectors.executeDeferredCall(scenarioName,agentSelectorJS,oCallInfo);
			debugger;
			console.log("Deferred Call result:"+JSON.stringify(dResult));
		}
	},{
		stepMethod: 'Given',
		stepPattern: 'the scenario {string} in environment {string} using agent selector {string} where',
		stepTimeout: 40000,
		stepFunction: async function(inScenarioName,inEnvironment,agentSelectorId,sCallInfo){
			debugger;
			var oCallInfo=JSON.parse(sCallInfo);
			var auxAgentSelector=agentSelectors.getById(agentSelectorId);
			var selectorJS=auxAgentSelector.selectorJS;
			var scenarioName=await ScStore.processExpression(inScenarioName);
			var agentSelectorJS=await ScStore.processExpression(selectorJS);
			var environment=await ScStore.processExpression(inEnvironment);
			var dResult=await agentSelectors.executeDeferredCall(scenarioName,agentSelectorJS,oCallInfo,environment);
			debugger;
			console.log("Deferred Call result:"+JSON.stringify(dResult));
		}
	},{
		stepMethod: 'Given',
		stepPattern: 'the scenario {string} in environment {string} and application {string} using agent selector {string} where',
		stepTimeout: 40000,
		stepFunction: async function(inScenarioName,inEnvironment,inApp,agentSelectorId,sCallInfo){
			debugger;
			var oCallInfo=JSON.parse(sCallInfo);
			var auxAgentSelector=agentSelectors.getById(agentSelectorId);
			var selectorJS=auxAgentSelector.selectorJS;
			var scenarioName=await ScStore.processExpression(inScenarioName);
			var agentSelectorJS=await ScStore.processExpression(selectorJS);
			var environment=await ScStore.processExpression(inEnvironment);
			var application=await ScStore.processExpression(inApp);
			var dResult=await agentSelectors.executeDeferredCall(scenarioName,agentSelectorJS,oCallInfo,environment,application);
			debugger;
			console.log("Deferred Call result:"+JSON.stringify(dResult));
		}
	}
];
module.exports=stepDefinitions;