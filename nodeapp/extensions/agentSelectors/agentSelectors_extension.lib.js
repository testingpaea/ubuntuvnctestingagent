const ScStore = require('kimbo/scenarioStore');
ScStore.enableExtension("storable");
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var AgentSelectorList=class AgentSelectorList extends StorableObjectListClass{
	constructor(){
		super("agentSelector");
		var self=this;
		self.addIndexField("id");
		self.waitingDeferred=new Map();
//		self.addIndexField("name");
	}
	getById(objectId){
		console.log("getById:"+objectId);
		var self=this;
		return self.getBestMatch("id",objectId);
	}
/*	getByName(name){
		console.log("getByName:"+name);
		var self=this;
		self.getBestMatch("name",name);
	}*/
	
	
	async executeDeferredCall(scenarioName,agentSelectorJS,params,environment,application){
		var self=this;
		debugger;
		await ScStore.getMultiscreen().openProgressBox("Running deferred scenario");
		var actApp=ScStore.getApplications().get();
		var actEnvironment=actApp.environment;
		var actAppName=actApp.name;
		if (typeof environment!=="undefined"){
			actEnvironment=environment;
		}
		if (typeof application!=="undefined"){
			actAppName=application;
		}
		
		// getting the scenario key
		var rootScenarios=ScStore.getRootScenarios();
		var bLocated=false;
		var scenario="";
		for (var i=0;(!bLocated)&&(i<rootScenarios.length);i++){
			scenario=rootScenarios[i];
			if (scenario.name==scenarioName){
				bLocated=true;
			}
		}
		var auxParams=[];
		if (Array.isArray(params)){
			auxParams=params;
		} else {
			var keys = Object.keys(params);
			for (var i=0;i<keys.length;i++){
				var keyName=keys[i];
				var auxParamName=await ScStore.processExpression(keyName);
				var auxParamValue=await ScStore.processExpression(params[keyName]);
				auxParams.push({name:auxParamName,value:auxParamValue});
			};
		}
		
		var inputValues={
			scenarioKey:scenario.key,
			scenario:scenarioName,
			agentSelector:agentSelectorJS,
			environment:actEnvironment,
			application:actAppName,
			srcAgent:ScStore.agentName,
			deferred:true,
			parameters:auxParams
		};
		debugger;
		console.log("ExecuteDeferredCall Starts. "+JSON.stringify(inputValues));
		result=await ScStore.getEvents().call("Run deferred scenario",inputValues);
		debugger;
		
		var fncResolve;
		var fncReject;
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		
		console.log("ExecuteDeferredCall Ends. "+JSON.stringify(result));
		if (typeof result.value!=="undefined"){
			var deferredInfo=result.value;
			var recAppendDefferredChilds=function(dResults){
				var execNode=ScStore.newExecutionNode(dResults.name,dResults.params);
				execNode.state=dResults.state;
				execNode.initTime=(new Date()).getTime()-dResults.time;
				execNode.endTime=(new Date()).getTime();
				for (const action of dResults.actions){
					recAppendDefferredChilds(action);
				};
				ScStore.doneExecutionNode();
			}
			deferredInfo.onFinish=async function(deferredResult){
				debugger;
				recAppendDefferredChilds(deferredResult);
				await ScStore.getMultiscreen().closeProgressBox();
				fncResolve(deferredResult);
			}
			if (!deferredInfo.executed){
				self.waitingDeferred.set(deferredInfo.executionId,deferredInfo);
				return prmResult;
			}
		}
	}	
}
var agentSelectorListManager=new AgentSelectorList();
storableObjectManager["loadAgentSelectorListManager"]=function(){
	return agentSelectorListManager;
};

module.exports=agentSelectorListManager;