const ScStore = require('reuse-cucumber-scenarios/scenarioStore');

var ParallelManager=class ParallelManager{
	constructor(){
		var self=this;
		self.totalScreens=0;
		self.actualScreen=0;
		self.enabled=false;
	}
	setTotalScreens(totalScreens){
		this.totalScreens=totalScreens;
	}
	setActualScreen(actualScreen){
		this.actualScreen=actualScreen;
	}
}


var parallelManager=new ParallelManager();
module.exports=parallelManager;