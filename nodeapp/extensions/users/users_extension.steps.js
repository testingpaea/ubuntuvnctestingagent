const ScStore = require('kimbo/scenarioStore');
const userManager = require('./users_extension.lib');

const stepDefinitions = [{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'add user {string} with',
    stepFunction: async function(theId,jsonValues){
			return await userManager.step_addNewObject(jsonValues,theId);
		}
	},{ stepMethod: 'Given', stepPattern: 'extract user attributes of {string}', 
		stepFunction: async function(sId){
			debugger;
			return await userManager.step_extractAttributes("id",sId);
		}
	},{ stepMethod: 'Given', stepPattern: 'extract user attributes which {string} is {string}', 
		stepFunction: async function(idxField,idxValue){
			debugger;
			return await userManager.step_extractAttributes(idxField,idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the attribute {string} value of user {string}', 
		stepFunction: async function(tgtVariableName,attName,userId){
			return await userManager.step_assignAttributeToVariable(tgtVariableName,attName,"id",userId);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the attribute {string} value of user which {string} is {string}',
		stepFunction: async function(tgtVariableName,attName,idxField,idxValue){
			return await userManager.step_assignAttributeToVariable(tgtVariableName,attName,idxField,idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the result of {string} function of user {string}', 
		stepFunction: async function(tgtVariableName,fncName,userId){
			return await userManager.step_assignMethodResultToVariable(tgtVariableName,fncName,"id",userId);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the result of {string} function of user which {string} is {string}', 
		stepFunction: async function(tgtVariableName,fncName,idxField,idxValue){
			return await userManager.step_assignMethodResultToVariable(tgtVariableName,fncName,idxField,idxValue);
		}
	}
];
module.exports=stepDefinitions;