const ScStore = require('kimbo/scenarioStore');
ScStore.enableExtension("storable");
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var UserList=class UserList extends StorableObjectListClass{
	constructor(){
		super("user");
		var self=this;
		self.addIndexField("id");
//		self.addIndexField("name");
	}
/*	getByName(name){
		console.log("getByName:"+name);
		var self=this;
		self.getBestMatch("name",name);
	}*/
}
var userListManager=new UserList();
storableObjectManager["loadUserListManager"]=function(){
	return userListManager;
};

module.exports=userListManager;