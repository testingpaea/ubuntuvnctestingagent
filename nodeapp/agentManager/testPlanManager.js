'use strict'

var TestPlanManager=class TestPlanManager{
	constructor(){
		var self=this;
		self.mapTestPlans=new Man();
		self.runningTestPlans=[];
	}
	addScenario(scenarioId,params,environment){
		self.scenarios.push({scenario:scenarioId,params:params,environment:environment});
	}
	getStatus(){
		var self=this;
		var oResult={
				id:self.id
				,status:self.status
				,scenarios:self.scenarios
				,agents:self.agents
				,agentSelector:self.agentSelector
				};
		return oResult;
	}
}
module.exports=TestPlan;