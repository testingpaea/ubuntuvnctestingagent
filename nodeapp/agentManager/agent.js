'use strict'
var { AgentMessage } = require('../messages/message.js');
const { spawn,fork } = require('child_process')
const screenshotManager=require("../screenshotManager/screenshotManager");
const fs = require('fs');



var Agent=class Agent{
	constructor(agentName,agentNumber,screenNumber,theManager,mapProperties){
		var self=this;
		self.process="";
		self.accId="";
		self.name=agentName;
		self.agentNumber=agentNumber;
		self.manager=theManager;
		self.actualScreen=screenNumber;
		self.mapScenarios=new Map();
		self.arrScenariosToRun=[];
		self.arrProcessedScenarios=[];
		self.dynamicAssign=false;
		self.busy=false;
		self.status="UNLOADED";
		self.runningScenario="";
		self.feedback="";
		self.bPriorityWaiting=0;
		self.mapProperties=new Map();
		if (typeof mapProperties!=="undefined"){
			mapProperties.forEach((value,key)=>{
				self.mapProperties.set(key,value);
			})
		}
	}
	getStatus(){
		var self=this;
		var oResult={};
		oResult.accId=self.accId;
		oResult.name=self.name;
		oResult.actualScreen=self.actualScreen;
		oResult.dynamicAssign=self.dynamicAssign;
		oResult.busy=self.busy;
		oResult.status=self.status;
		oResult.runningScenario=self.runningScenario;
		oResult.scenarios=[];
		oResult.feedback=self.feedback;
		oResult.takingScreenshots=self.bTakingScreenshot;
		oResult.screenshotFrequency=self.screenshotFrequency;

		self.mapScenarios.forEach((scenario)=>{
			oResult.scenarios.push(scenario);
		});
		oResult.processedScenarios=[];
		self.arrProcessedScenarios.forEach(scenario=>{
			oResult.processedScenarios.push(scenario);
		});
		oResult.properties=[];
		self.mapProperties.forEach(property=>{
			oResult.properties.push(property);
		});
		return oResult;
	}
	setProperties(mapProperties){
		var self=this;
		if (typeof mapProperties!=="undefined"){
			mapProperties.forEach((value,key)=>{
				self.mapProperties.set(key,value);
			});
		}
	}
	addProperty(property){
		var self=this;
		self.mapProperties.set(property.name,property);
	}
	delProperty(propertyName){
		var self=this;
		if (self.mapProperties.has(propertyName)) {
			self.mapProperties.delete(propertyName);
		}
	}
	
	addScenarioToRun(scenario,bFirst){
		if ((typeof bFirst!=="undefined")&& bFirst){
			this.arrScenariosToRun.unshift(scenario);
		} else {
			this.arrScenariosToRun.push(scenario);
		}
	}
	addScenario(scenario){
		if (!this.mapScenarios.has(scenario.id)){
			this.mapScenarios.set(scenario.id,scenario);
		}
	}
	setScenarios(lstScenarios){
		var self=this;
		lstScenarios.forEach(function(scenario){
			self.addScenario(scenario);
		});
	}
	sendMessage(action,value,prevMsg){
		//console.log("Sending message:"+action);
		//console.log("Msg to send:"+JSON.stringify(value));
		var self=this;
		var msg=new AgentMessage(action,value,prevMsg);
		self.process.send(msg);
		//console.log("Msg sended");
	}
	async processMessage(received){
		var self=this;
		//console.log("Cucumber "+self.name+" Sends Msg:"+JSON.stringify(received));
		if (typeof received.action==="undefined"){
			console.log("omitted");
			return;
		}
		switch (received.action) {
			case "GET NEXT SCENARIO":
				self.status="DONE";
				self.busy=false;
				self.runningScenario="";
				if (!self.dynamicAssign){
					self.sendMessage("DONE","",received);
				} else {
					// first... call for a priority scenario appears....
					if (self.bPriorityWaiting>0){
						var auxScenario=self.getPriorityScenarioPending();
						if (auxScenario!==""){
							self.addScenarioToRun(scenario,true); //if exists... add as first scenario to run.... 
						}
						self.bPriorityWaiting--;
					}
					// second... continue running previously asigned scenarios (or priority escenarios)....
					if (self.arrScenariosToRun.length>0){
						debugger;
						self.run();
					} else { // no scenarios assigned to the agent..... get scenarios fron de ACC or from Controller
						debugger;
						var newScenario=self.manager.getScenarioToRun(self);
						if (newScenario!==""){
							self.runScenario(newScenario);
						} else { // no scenarios in the ACC.... it�s done.... 
							self.runningScenario="";
							self.sendMessage("DONE","",received);
						}
					}
				}
				break;
			case "FINISHED":
				//console.log("End by message:"+JSON.stringify(received));
				if ((typeof received.value.endCompilation!=="undefined")&&(received.value.endCompilation)){
					self.status="READY";
					self.busy=false;
					console.log("Agent "+self.name+" READY");
					received.value.scenarios.forEach(function(scenario){
						self.mapScenarios.set(scenario.key,scenario);
					});
					var arrProperties=[];
					for (let prop of self.mapProperties.keys()){
						arrProperties.push([prop,self.mapProperties.get(prop)]);
					}
					
					var agentProps={
						name:self.name,
						properties:arrProperties
					}
					self.sendMessage("SET AGENT NAME",agentProps);
				} else {
					debugger;
					var planExecutionId=self.runningScenario.planExecutionId;
					var scenarioExecutionId=self.runningScenario.executionId;
					var executionResult="";
					received.value.results.forEach(auxArrResults=>{
						self.arrProcessedScenarios.push(auxArrResults);
						var arrResults=auxArrResults;
						if (!Array.isArray(auxArrResults)) {
							var arrResults=[auxArrResults];
						}
						arrResults.forEach(aResult=>{
							if (typeof aResult.params!=="undefined"){
								aResult.params.forEach(param=>{
									if ((param.name=="executionId")&&(param.value==scenarioExecutionId)){
										executionResult=aResult;
									}
								});
							}
						});
					});
					var srcAgent=self.runningScenario.srcAgent;
					self.manager.finishedScenarioExecution(
														planExecutionId,
														scenarioExecutionId,
														self.manager.agentNamePrepend,
														self.name,
														self.agentNumber,
														executionResult);
					if ((self.manager.controllerPushMessager!=="")
						&&(typeof self.manager.controllerPushMessager.source!=="undefined")
						&& self.manager.isACC){
						self.manager.controllerPushMessager.sendMessage("SCENARIO FINISHED"
								,{
									planExecutionId:planExecutionId,
									executionId:scenarioExecutionId,
									result:executionResult,
									agentName:self.name,
									agentNumber:self.agentNumber,
									accNamePrepend:self.manager.agentNamePrepend
								});
					} else if ((typeof srcAgent!=="undefined")&&(srcAgent!==self.name)){
						self.manager.finishedDeferredScenario(scenarioExecutionId,executionResult,srcAgent);
					}
				}
				break;
			case "PROCESSED":
				debugger;
				var msgBody=received.value;
				var scenarioId=msgBody.id;
				var result=msgBody.result;
				var scenario=self.runningScenario;
				if (scenario.id!=scenarioId){
					console.log("The Scenario IDs not match");
				} else {
					self.runningScenario="";
					self.arrProcessedScenarios.push(scenario);
					self.manager.processedScenario(scenarioId,result);
				}
				self.sendMessage("OK","",received);
				break;
			case "GET TEST LIST":
				console.log("Will send Issue list or dynamic:"+self.dynamicAssign);
				if (self.dynamicAssign){
					self.sendMessage("TEST LIST",["DYNAMIC"],received);
				} else {
					self.sendMessage("TEST LIST",self.lstScenarios,received);
				}
				break;
			case "GET SCREENS INFO":
				self.sendMessage("SCREENS INFO",
									{totalScreens:self.manager.totalScreens
									,actualScreen:self.actualScreen}
									,received);
				break;
			case "GET CREDENTIALS":
				if (received.value.id=="orclTestDirect"){
					self.sendMessage("CREDENTIALS",{
											user          : "testing",
											password      : "testing",
//											privilege     : 2, 
											connectString : "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.100.3)(PORT = 49161))(CONNECT_DATA =(SID= XE)))"
										},received);
				} else if (received.value.id=="test_mongodb"){ 
					self.sendMessage("CREDENTIALS",{
											user          : "usrMongo",
											password      : "mngdbMongo2023",
//											privilege     : 2, 
											url : "mongodb://CRED_USER:CRED_PASS@192.168.1.120:27017",
											dbName: "test_mongo"
										},received);
				} else if (received.value.id=="orclTestIndirect"){ 
					self.sendMessage("CREDENTIALS",{
											indirect     : true,
											credentials  : "NOT NEEDED"
										},received);
				} else {
					//if not is the simple orclTestCredentials it use the name to read a swarm secret with this name
					try {
					  const data = fs.readFileSync('/run/secrets/'+received.value.id, 'utf8');
					  var jsonCredentials=JSON.parse(data);
					  self.sendMessage("CREDENTIALS",jsonCredentials,received);
					  
					} catch (err) {
					  console.error(err);
					}					
					
				}
				
				break;

			case "LOCK SYSTEM":
				self.status="WAITING";
				await self.manager.lockSystem(self);
				self.status="RUNNING";
				self.sendMessage("LOCKED","",received);
				break;
			case "UNLOCK SYSTEM":
				await self.manager.unlockSystem(self);
				self.status="RUNNING";
				self.sendMessage("UNLOCKED","",received);
				break;
			case "PROGRESSBOX":
				var msgBody=received.value;
				if (msgBody.action=="OPEN"){
					self.feedback={type:"message",text:msgBody.message};
				} else {
					self.feedback="";
				}
				break;
			case "INPUTBOX":
				var msgBody=received.value;
				if (msgBody.action=="OPEN"){
					self.feedback={type:"inputbox",password:msgBody.password,text:msgBody.message};
				} else {
					self.feedback="";
				}
				break;
			case "RUN DEFERRED SCENARIO":
				debugger;
				self.status="WAITING";
				var scenarioInfo=received.value;
				scenarioInfo.agentName=self.name;
				console.log(JSON.stringify(scenarioInfo));
				var dResult=await self.manager.planDeferredScenario(scenarioInfo);
				self.status="RUNNING";
				self.sendMessage("DEFERRED RESPONSE",dResult,received);
				break;
			case "SCREENSHOT":
				debugger;
				var rValue=received.value;
				await screenshotManager.setScreenshot(rValue.screenshot,self.name);
				// if the ACC is attached to controller.... send the screenshot to controller
				self.sendMessage("SCREENSHOT RESPONSE",dResult,received);
				break;
		}
		self.manager.sendUpdateStatus();
	}
	setProcess(process){
		var self=this;
		self.process=process;
		process.on('message',received=>{
			//console.log("message received:"+JSON.stringify(received));
			self.processMessage(received);
			}
		);
		process.on('exit', code => {
			console.log(`Exit code is: ${code}`);
			self.busy=false;
			self.status="UNLOADED";
			self.procesing="";
		});
	}
	reload(){
		var self=this;
		self.status="RELOADING";
		self.manager.sendUpdateStatus();
		setTimeout(function(){
			self.abort();
			self.run();
		},2000);
	}
	takeScreenshot(){
		var self=this;
		self.sendMessage("TAKE SCREENSHOT");
	}
	traceExecutionTree(){
		var self=this;
		self.sendMessage("TRACE EXECUTION TREE");
	}
	saveSeleniumInterface(interfaceName){
		var self=this;
		debugger;
		self.sendMessage("SAVE SELENIUM INTERFACE",{filename:interfaceName});
	}
	runScenario(scenario){
		var self=this;
		if (Array.isArray(scenario)){
			scenario.forEach(oScenario=>{
				self.addScenarioToRun(oScenario);
			});
		} else {
			self.addScenarioToRun(scenario);
		}
		self.run();
	}
	run(){
		var self=this;
		if (self.busy){
			console.log("The Agent "+self.name+" is Busy yet... ");
			return;
		}
		if  ((self.status=="ABORTED")
			||(self.status=="UNLOADED")
			||(self.status=="RELOADING")
			) {
			self.busy=true;
			self.status="LOADING";

			//console.log("Launch scenario processor");
			
			var args = process.argv.slice(2);
			var isDebug = false;
			var inspectArg="";
			var inspectIP="0.0.0.0";
			var inspectPort="";
			if (Array.isArray(process.execArgv)&&(process.execArgv.length>0)){
				var iArg=0;
				while (iArg<process.execArgv.length){
					if (	(process.execArgv[iArg].indexOf('--inspect')>=0) ||
							(process.execArgv[iArg].indexOf('--inspect-brk')>=0)
						){
						isDebug=true;
						//the inspectarg can be an --inspect=127.0.0.1
						// have the ip (local or 0.0.0.0) and not 
						var arrInspectParts=process.execArgv[iArg].split("=");
						inspectArg=arrInspectParts[0];
						if (arrInspectParts.length>1){
							arrInspectParts=arrInspectParts[1].split(":");
							if (arrInspectParts[0].indexOf('.')>=0){ //the first is a IP
								inspectIP=arrInspectParts[0];
								if (arrInspectParts.length==2){ // the second part is a port
									inspectPort=arrInspectParts[1];
								}
							} else { //the first is a port.... there is not an IP
								inspectPort=arrInspectParts[0];
							}
						}
						process.execArgv.splice(iArg,1);
					} else {
						iArg++;
					}
				}
			}
			if (isDebug) {
				//execArgs.splice(isDebug, 1);
				if (inspectPort!==""){
					inspectPort=parseInt(inspectPort);
				} else {
					inspectPort=9230;
				}
				inspectPort+=self.agentNumber;
				process.execArgv.push(inspectArg+"="+inspectIP+":"+inspectPort);
			}
			debugger;
			var cucumberParams=require("../cucumbercfg");
			process.env["chromiumBinary"]=cucumberParams.chromiumBinary;
			process.env["DISPLAY"]=":2";
			console.log("====>>>> LAUNCHING CUCUMBER <<<<=====");
			console.log(args.toString());

			const child = fork('node_modules/@cucumber/cucumber/bin/cucumber-js',args);//, ['--exit']);
			//var ccb=require('cucumber/lib/cli/run.js');
			//ccb.default();
			self.setProcess(child);
		} else if ((self.status=="DONE")||(self.status=="READY")){
			if (self.arrScenariosToRun.length>0){
				var scenarioToRun=self.arrScenariosToRun.shift();
				self.runningScenario=scenarioToRun;
				self.status="RUNNING";
				self.busy=true;
				self.sendMessage("NEXT SCENARIO",scenarioToRun);
			}
		}
		self.manager.sendUpdateStatus();
	}
	abort(){
		var self=this;
		self.process.kill('SIGKILL');
		self.status="ABORTED";
		self.busy=false;
		self.processing="";
	}

}
module.exports=Agent;