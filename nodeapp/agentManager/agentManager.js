'use strict'
var Agent = require('./agent');
var TestPlan = require('./testPlan');
var LZUTF8 = require('lzutf8');
var messageHandler = require('../messages/messageHandler');
const path = require('path');
const ping = require('ping');
const fs = require('fs');
const PushMessager=require("../webui/pushModes/webpushPush");
const genIdentification = require('../helpers/identification.lib');
const request = require("request"); 
const screenshotManager=require("../screenshotManager/screenshotManager");
const { execSync } = require( 'child_process' );

var AgentManager=class AgentManager{
	constructor(instanceId){
		var self=this;
		self.isController=false;
		self.isACC=true;
		self.agentNamePrepend="AGENT_";
		self.sourceElement=instanceId;
		self.nAgents=0;
		self.totalScreens=0;

		self.mapACCs=new Map();
		self.mapAgents=new Map();
		self.mapScenarios=new Map();

		self.chromiumGatewayProcess="";
		self.chromiumGatewayPushMessager="";
		self.chromiumGatewayRegistered=false;

		self.controllerPushMessager="";
		self.controllerRegistered=false;

		self.chromiumRegisteredWebUIs=new Map();
		self.chromiumUnregisteredWebUIs=new Map();

		self.lockWaiting=[];
		self.lockRunning=[];
		self.mapStoredTestPlans=new Map();
		self.loadTestPlans();
		
		self.runningTestPlans=new Map();
		self.scheduledTestPlans=new Map();
		self.dynamicTestPlans=new Map();
		self.priorityTestPlans=new Map();
		self.testPlanTypes=["normal","priority","scheduled","dynamic"];
		self.finishedTestPlans={normal:new Map(),
								priority:new Map(),
								scheduled:new Map(),
								dynamic:new Map()};
	}
	getReadyAgents(){
		var self=this;
		var arrResult=[];
		self.mapAgents.forEach(agent=>{
			if ((agent.status=="READY")||(agent.status=="DONE")){
				arrResult.push(agent);
			}
		});
		return arrResult;
	}
	getScenarioToRun(agent){
		var self=this;
		var slcTestPlan="";
		// selecting scenario from ACC test plans.....
		for (const [key, testPlan] of self.runningTestPlans) {
			if (testPlan.isAgentSelected(agent)){
				var nextScenario=testPlan.nextScenario(agent);
				if (nextScenario!==""){
					return nextScenario;
				}
			}
		};
		// if there is not scenarios in ACC.... call to controller (if exists)
		if (self.controllerPushMessager!==""){
			//debugger;
			self.controllerPushMessager.sendMessage("NEXT SCENARIO",{acc:self.agentNamePrepend,agentName:agent.name});
		}
		return "";
	}
	async dispatchScenarios(){
		var self=this;
		var arrReadyAgents=self.getReadyAgents();
		for (var i=0;i<arrReadyAgents.length;i++){
			var agent=arrReadyAgents[i];
			var scenario=await self.getScenarioToRun(agent);
			if (scenario!==""){
				if (self.isACC){
					//debugger;
					agent.runScenario(scenario);
				} else if (self.isController){
					//debugger;
					self.sendScenarioToAgent(agent,scenario);
				}
			}
		};
	}
	async sendScenarioToAgent(agent,scenario){
		var self=this;
		agent.status="RUNNING";
		var pmAcc=self.mapACCs.get(agent.accId).pushMessager;
		await pmAcc.sendMessage(
				"RUN-SCENARIO",
				{
					agent:agent.name,
					scenario:scenario
				},
				true);
	}
	runTestPlan(testPlan){
		var self=this;
		self.runningTestPlans.set(testPlan.executionId,testPlan);
		testPlan.startRunning(); // all scenarios in pending
		self.dispatchScenarios();
	}
	loadTestPlans(){
		var self=this;
		const directoryPath = "./testPlans";
		//passsing directoryPath and callback function
		fs.readdir(directoryPath, function (err, files) {
			//handling error
			if (err) {
				return console.log('Unable to scan directory: ' + err);
			} 
			//listing all files using forEach
			files.forEach(function (file) {
				// Do whatever you want to do with the file
				console.log(directoryPath+"/"+file);
				let rawdata = fs.readFileSync(directoryPath+"/"+file);  
				let jsonTestPlan = JSON.parse(rawdata);  
				var auxTP=new TestPlan(jsonTestPlan);
				self.mapStoredTestPlans.set(auxTP.id,auxTP);				
			});
		});		
	}
	initializeAsACC(nAgents,agentProperties,bDynamics){
		var self=this;
		console.log("INITIALIZING AS ACC");
		//debugger;
		messageHandler.setIdentification("ACC","Agents Container Controller");
		self.isController=false;
		self.isACC=true;
		self.nAgents=nAgents;
		self.totalScreens=nAgents+1;
		
		const os = require('os'); 
		var dockerHostName=os.hostname();
		console.log(dockerHostName);
		self.agentNamePrepend=dockerHostName;
		
		for (var i=0;i<nAgents;i++){
			var agent=new Agent(self.agentNamePrepend+"_"+i,i,i,self);
			agent.dynamicAssign=true;
			agent.setProperties(agentProperties);
			self.mapAgents.set(agent.name,agent);
			if (i==0){
				agent.run();
			}
		}
		
		messageHandler.addEventHandler("ACK",async function(value){
			//debugger;
			var srcMsgId=value.msgContent.srcMsgId;
			var ackDest=value.msgDestination;
			var ctrlPM=agentManager.controllerPushMessager;
			var ackProcessed=false;
			if ((typeof ctrlPM!=="undefined")&&(ctrlPM!=="")&&(ctrlPM.source==ackDest)){
				if (ctrlPM.mapWaitingACK.has(srcMsgId)) {
					await ctrlPM.ack(srcMsgId);
					ackProcessed=true;
				}
			} 
			if (!ackProcessed) {
				var gtwPM=agentManager.chromiumGatewayPushMessager;
				if ((typeof gtwPM!=="undefined")&&(gtwPM!=="")&&
						((gtwPM.source==ackDest)
						||
						(gtwPM.destination==ackDest))
						){
					if (gtwPM.mapWaitingACK.has(srcMsgId)) {
						await gtwPM.ack(srcMsgId);
						ackProcessed=true;
					}
				} 
			}
			self.finishEventProcessing("ACK of message",false);
		});
		
		messageHandler.addEventHandler("TEST LIST",value=>{
			self.listTests=value;
			self.finishEventProcessing("New List of Tests:"+JSON.stringify(self.listTests));
		});
		messageHandler.addEventHandler("SCREENS INFO",value=>{
			self.totalScreens=value.totalScreens;
			self.finishEventProcessing("Screen info");;
		});
		messageHandler.addEventHandler("FEEDBACK",value=>{
			//debugger;
			var oValue=value;
			ScStore.getMultiscreen().closeInputBox(oValue);
			self.finishEventProcessing("Sending Feedback");
		});
		messageHandler.addEventHandler("LOAD_AGENT",async function(value){
			//debugger;
			var agentName=value.msgContent.agentName;
			self.loadAgent(agentName);
			self.finishEventProcessing("Loading Agent " + agentName);
		});
		messageHandler.addEventHandler("ADDPROPERTY",async function(value){
			//debugger;
			var agentName=value.msgContent.agentName;
			var agent=self.getAgent(agentName);
			agent.addProperty(value.msgContent.property);
			self.finishEventProcessing("Adding Property:"+agentName+" "+JSON.stringify(value.msgContent.property));
		});
		messageHandler.addEventHandler("DELPROPERTY",async function(value){
			//debugger;
			var agentName=value.msgContent.agentName;
			var agent=self.getAgent(agentName);
			agent.delProperty(value.msgContent.propertyName);
			self.finishEventProcessing("Deleting Property:"+agentName+" "+JSON.stringify(value.msgContent.property));
		});
		
		
		messageHandler.addEventHandler("ACC-CONNECTION-REGISTERED",value=>{
			//debugger;
			console.log("ACC "+self.sourceElement+" RECEIVED CONTROLLER REGISTRATION OF ACC."+JSON.stringify(value));
			self.controllerRegistered=true;
			//self.sendUpdateToWebUIs("Registration in controller sucesfully");
			self.finishEventProcessing("Registration in controller sucesfully");
		});
		messageHandler.addEventHandler("RUN-SCENARIO",value=>{
			debugger;
			//console.log(JSON.stringify(value));
			var agentName=value.msgContent.agent;
			var scenario=value.msgContent.scenario;
			var agent=self.getAgent(agentName);
			var testPlan=self.getTestPlanExecution(scenario.planExecutionId);
			if (testPlan===""){
				testPlan=new TestPlan();
				testPlan.executionId=scenario.planExecutionId;
				if ((typeof scenario.deferred!=="undefined")&& scenario.deferred) {
					testPlan.deferredScenarios=true;
				}
				testPlan.status="RUNNING";
				self.runningTestPlans.set(scenario.planExecutionId,testPlan);
			}
			testPlan.runningScenarios.set(scenario.executionId,scenario);
			agent.runScenario(scenario);
			self.finishEventProcessing("Run scenario:"+ scenario.executionId);
		});
		messageHandler.addEventHandler("NEXT SCENARIO",value=>{
			//debugger;
			var scenarioId=value.id;
			var selector=ScStore.getSelected();
			selector.refresh();
			var scenario=selector.mapScenariosByTag.get(scenarioId);
			console.log("SCENARIO:"+JSON.stringify(scenario));
			selector.addScenario(scenarioId);
			selector.dynActualScenario=scenario;
			while (ScStore.getClosures().closures.length>1){
				ScStore.getClosures().closures.pop();
			}
			while (ScStore.getClosures().runtimeClosures.length>1){
				ScStore.getClosures().runtimeClosures.pop();
			}
			ScStore.actExecutionNode="";
			ScStore.executionTree=[];
			ScStore.indirectScenarioCall(scenario);
			self.finishEventProcessing("Indirect Calling of Scenario:"+scenarioId);
		});
		messageHandler.addEventHandler("TESTPLAN FINISHED",value=>{
			//debugger;
			var testPlanExecutionId=value.msgContent.testPlanExecutionId;
			var arrAgentNames=value.msgContent.agentNames;
			self.testPlanTypes.forEach(theType=>{
				var auxTestPlan=self.finishedTestPlans[theType];
				if (auxTestPlan.has(testPlanExecutionId)){
					auxTestPlan.delete(testPlanExecutionId);
				}
			});
			var i=0;
			arrAgentNames.forEach(agentName=>{
				var theAgent=self.getAgent(agentName);
				while(i<theAgent.arrProcessedScenarios.length){
					var scenario=theAgent.arrProcessedScenarios[i];
					while (Array.isArray(scenario)){
						scenario=scenario[0];
					}
					//aResult.scenarioExecutionId=scenarioExecutionId;
					//aResult.testPlanExecutionId=testPlanExecutionId;
					if (scenario.testPlanExecutionId==testPlanExecutionId){
						theAgent.arrProcessedScenarios.splice(i,1);
					} else {
						i++;
					}
				}
			});
			self.finishEventProcessing("TESTPLAN FINISHED:"+testPlanExecutionId);
		});
		messageHandler.addEventHandler("DEFERRED SCENARIO FINISHED",async function(value){
			var testPlanExecutionId=value.msgContent.testPlanExecutionId;
			var executionId=value.msgContent.executionId;
			var srcAgentName=value.msgContent.srcAgent;
			var executionResult=value.msgContent.result;
			await self.finishedDeferredScenario(executionId,executionResult,srcAgentName);
			self.finishEventProcessing("DEFERRED SCENARIO FINISHED:"+testPlanExecutionId);
		});
		messageHandler.addEventHandler("TAKE SCREENSHOT",async function(value){
			var agentName=value.msgContent.agentName;
			var freq=value.msgContent.freq;
			var fromController=value.msgContent.isController;
			debugger;
			var shotData=screenshotManager.getScreenshotData(agentName);
			shotData.controllerRequestsCounter++;
			screenshotManager.updateScreenshot(agentName,freq);
			self.finishEventProcessing("TAKE SCREENSHOT LAUNCHED");
		});
		messageHandler.addEventHandler("TRACE EXECUTION TREE",async function(value){
			var agentName=value.msgContent.agentName;
			var freq=value.msgContent.freq;
			var fromController=value.msgContent.isController;
			debugger;
			var shotData=screenshotManager.getScreenshotData(agentName);
			shotData.controllerRequestsCounter++;
			screenshotManager.updateScreenshot(agentName,freq);
			self.finishEventProcessing("TRACE EXECUTION TREE LAUNCHED");
		});
		
		
		
		
		var controllerLink=process.env.withControllerLink;
		if (typeof controllerLink!=="undefined"){
			console.log("Controller Link :"+controllerLink);
			self.connectToController(controllerLink);
		}

	}
	
	
	
	
	addAgentScenarios(agent,accId){
		var self=this;
		var scnList;
		var agentName=agent.name;
		if (typeof agent.scenarios!=="undefined"){
			scnList=agent.scenarios;
			agent.mapScenarios=new Map();
		} else {
			scnList=agent.mapScenarios;
		}
		if (typeof scnList!=="undefined"){
			scnList.forEach(scenario=>{
				var scnKey=scenario.key;
				var scnName=scenario.name;
				var scnParams=scenario.params;
				if (!self.mapScenarios.has(scnKey)){
					self.mapScenarios.set(scnKey,{key:scnKey,name:scnName,params:scnParams,agents:new Map()});
				}
				var theScenario=self.mapScenarios.get(scnKey);
				theScenario.agents.set(agentName,{agentName:agentName,acc:accId});
//				//debugger;
				if (!agent.mapScenarios.has(scenario.key)){
					agent.mapScenarios.set(scenario.key,scenario);
				}
			});
		}
	}
	initializeAsController(){
		var self=this;
		messageHandler.setIdentification("CTRLR","Testing Controller");
		self.isController=true;
		self.isACC=false;
		self.nAgents=0;
		self.totalScreens=0;
		var fncUpdateACC=function(accId,accDetail){
			debugger;
			var mapACCs=self.mapACCs;
			var theAcc=mapACCs.get(accId);
			theAcc.statusInfo=accDetail;
			//debugger;
			theAcc.statusInfo.agents.forEach(agent=>{
				var agentName=agent.name;
				
				agent.accId=accId;
				/*var auxAgent=self.mapAgents.get(agentName);
				if (typeof auxAgent!=="undefined"){
					agent.screenshot=auxAgent.screenshot;
					agent.screenshotFrequency=auxAgent.screenshotFrequency;
				}*/
				self.mapAgents.set(agentName,agent);
				self.addAgentScenarios(agent,accId);
			});
		}
		messageHandler.addEventHandler("ACC-CONNECT",value=>{
			debugger;
			console.log("Processing ACC-CONNECT with values:"+JSON.stringify(value));
			var sLink=value.msgContent.gatewayLink;
			if (typeof sLink!=="undefined"){
				var output = LZUTF8.decompress(sLink, {inputEncoding:"Base64"});
				var oLinkInfo=JSON.parse(output);
				const PushMessager=require("../webui/pushModes/webpushPush");
				var pushMessager=new PushMessager(
										oLinkInfo.subscription
										,oLinkInfo.vapidKeys
										,oLinkInfo.destination
										,oLinkInfo.destination
										,oLinkInfo.ip);
				pushMessager.httpConnection=value.msgContent.httpConnection;
				self.mapACCs.set(oLinkInfo.destination,{pushMessager:pushMessager,statusInfo:""});
				//fncUpdateACC(oLinkInfo.destination,value.msgContent.detail);
				pushMessager.sendMessage("ACC-CONNECTION-REGISTERED");	
			} else {
				console.log("No gateway values passed....");
			}
			self.finishEventProcessing("Added new ACC..or not");
		});
		messageHandler.addEventHandler("CTRL-UPDATE",value=>{
			//debugger;
			//console.log("Processing acc update status:"+JSON.stringify(value));
			var accId=value.msgContent.gateway;
			fncUpdateACC(accId,value.msgContent);
			self.finishEventProcessing("Updated "+accId+" status");
		});
		
		messageHandler.addEventHandler("NEXT SCENARIO",async function(value){
			//debugger;
			console.log(self.sourceElement+" Getting next scenario for agent:"+JSON.stringify(value));
			var agent=self.getAgent(value.msgContent.agentName);
			var scenario=await self.getScenarioToRun(agent);
			if (scenario!=="") {
				agent.runningScenario=scenario;
				self.sendScenarioToAgent(agent,scenario);
				console.log(self.sourceElement+" Sended new scenario to agent. "+JSON.stringify(value));
			} else {
				console.log("There is not more scenarios to agent:"+JSON.stringify(value));
			}			
			self.finishEventProcessing("Updated status");
		});
		messageHandler.addEventHandler("SCENARIO FINISHED",async function(value){
			//debugger;
			console.log(self.sourceElement+" Agent says than scenario is finished:"+JSON.stringify(value));
			var scenarioInfo=value.msgContent;
			var accNamePrepend=scenarioInfo.accNamePrepend;
			var agentName=scenarioInfo.agentName;
			var agentNumber=scenarioInfo.agentNumber;
			var planExecutionId=scenarioInfo.planExecutionId;
			var executionId=scenarioInfo.executionId;
			var allResults=scenarioInfo.result;
			//	var testPlan=self.getTestPlanExecution(planExecutionId);
			self.finishedScenarioExecution(
								planExecutionId,
								executionId,
								accNamePrepend,
								agentName,
								agentNumber,
								allResults);
			self.finishEventProcessing("Finished one scenario");
		});
		messageHandler.addEventHandler("RUN DEFERRED SCENARIO",async function(value){
			debugger;
			console.log(self.sourceElement+" Controller has received a deferred scenario run petition:"+JSON.stringify(value));
			var scenarioInfo=value.msgContent;
			var dResult=self.prepareDeferredTestPlan(scenarioInfo);
			self.finishEventProcessing("Finished Deferred Run Scenario petition");
		});
		messageHandler.addEventHandler("SCREENSHOT",async function(value){
			debugger;
			//console.log(self.sourceElement+" Controller has received a screenshot:"+JSON.stringify(value));
			var screenshotInfo=value.msgContent;
			screenshotManager.setScreenshot(screenshotInfo.screenshot,screenshotInfo.agentName);

			//var dResult=self.prepareDeferredTestPlan(scenarioInfo);
			self.finishEventProcessing("Finished storing screenshot");
		});
		
		
		
	}
	getTestPlanExecution(testPlanId){
		var self=this;
		if (self.runningTestPlans.has(testPlanId)){
			return self.runningTestPlans.get(testPlanId);
		} else if (self.scheduledTestPlans.has(testPlanId)){
			return self.scheduledTestPlans.get(testPlanId);
		} else if (self.priorityTestPlans.has(testPlanId)){
			return self.priorityTestPlans.get(testPlanId);
		} else if (self.dynamicTestPlans.has(testPlanId)){
			return self.dynamicTestPlans.get(testPlanId);
		}
		for (var i=0;i<self.testPlanTypes.length;i++){
			var theType=self.testPlanTypes[i];
			var auxMapTPs=self.finishedTestPlans[theType];
			if (auxMapTPs.has(testPlanId)){
				return auxMapTPs.get(testPlanId);
			} 
		}
		return "";
	}
	updateFinishedTestPlan(testPlan){
		var self=this;
		var testPlanExecutionId=testPlan.executionId;
		if (self.runningTestPlans.has(testPlanExecutionId)){
			self.runningTestPlans.delete(testPlanExecutionId);
			self.finishedTestPlans.normal.set(testPlanExecutionId,testPlan);
		} else if (self.scheduledTestPlans.has(testPlanExecutionId)){
			self.scheduledTestPlans.delete(testPlanExecutionId);
			self.finishedTestPlans.scheduled.set(testPlanExecutionId,testPlan);
		} else if (self.priorityTestPlans.has(testPlanExecutionId)){
			self.priorityTestPlans.delete(testPlanExecutionId);
			self.finishedTestPlans.priority.set(testPlanExecutionId,testPlan);
		} else if (self.dynamicTestPlans.has(testPlanExecutionId)){
			self.dynamicTestPlans.delete(testPlanExecutionId);
			self.finishedTestPlans.dynamic.set(testPlanExecutionId,testPlan);
		}
	}
	finishedScenarioExecution(testPlanExecutionId,scenarioExecutionId,accNamePrepend,agentName,agentNumber,results){
		var self=this;
		var testPlan=self.getTestPlanExecution(testPlanExecutionId);
		var bUpdated=false;
		if (testPlan!==""){
			//there is a running test Plan
			if (testPlan.runningScenarios.has(scenarioExecutionId)){
				//the finished scenario is in the running list
				var theRunningScenario=testPlan.runningScenarios.get(scenarioExecutionId);
				testPlan.runningScenarios.delete(scenarioExecutionId);
				//debugger;
				testPlan.finishedScenarios.set(scenarioExecutionId,theRunningScenario);
				var allResults=[];
				if (typeof results!=="undefined"){
					if (Array.isArray(results)){
						results.forEach(aResult=>{
							allResults.push(aResult);
						});
					} else {
						allResults.push(results);
					}
				}
				while (Array.isArray(allResults)&&allResults.length==1){
					allResults=allResults[0];
				}
				if (Array.isArray(allResults)){
					allResults.forEach(aResult=>{
						if (typeof aResult.state!=="undefined"){
							aResult.executedInACC=accNamePrepend;
							aResult.executedByAgent=agentName;
							aResult.executedByAgentNumber=agentNumber;
							aResult.scenarioExecutionId=scenarioExecutionId;
							aResult.testPlanExecutionId=testPlanExecutionId;
						}
					});
				} else if (typeof allResults.state!=="undefined"){
							allResults.executedInACC=accNamePrepend;
							allResults.executedByAgent=agentName;
							allResults.executedByAgentNumber=agentNumber;
							allResults.scenarioExecutionId=scenarioExecutionId;
							allResults.testPlanExecutionId=testPlanExecutionId;
				}
				
				theRunningScenario.executionResult=allResults;
				
				//if the scenario is deferred.... have to send message to the src agent
				if (theRunningScenario.deferred&&self.isController){
					var executionId=theRunningScenario.executionId;
					var srcAgent=theRunningScenario.srcAgent;
					var theAgent=self.getAgent(srcAgent);
					var accId=theAgent.accId;
					var pmAcc=self.mapACCs.get(accId).pushMessager;
					pmAcc.sendMessage(
							"DEFERRED SCENARIO FINISHED",
							{
								testPlanExecutionId:testPlanExecutionId,
								executionId:executionId,
								srcAgent:srcAgent,
								result:theRunningScenario.executionResult
							},
							true);
				}
				if ((testPlan.runningScenarios.size+testPlan.pendingScenarios.size)==0){
					// set the testplan in the "finished" maps 
					self.updateFinishedTestPlan(testPlan);
					
					if (self.isController){ // if in controller
						var mapACCs=new Map();
						// locate all agents/ACCs involved in the execution
						testPlan.finishedScenarios.forEach(scenario=>{
							var scnResult=scenario.executionResult;
							var theAgent=self.getAgent(scnResult.executedByAgent);
							var accId=theAgent.accId;
							if (!mapACCs.has(accId)){
								mapACCs.set(accId,{acc:scnResult.executedInACC,accId:accId,agents:new Map()});
							}
							var accData=mapACCs.get(accId);
							if (!accData.agents.has(scnResult.executedByAgent)){
								accData.agents.set(scnResult.executedByAgent,theAgent);
							}
						});
						// Send a message to ACCs/Agents to delete the results of the scenarios of testplan
						mapACCs.forEach(theACC=>{
							var accId=theACC.accId;
							var pmAcc=self.mapACCs.get(accId).pushMessager;
							var arrAgentNames=[];
							theACC.agents.forEach(theAgent=>{
								arrAgentNames.push(theAgent.name);
							});
							pmAcc.sendMessage(
									"TESTPLAN FINISHED",
									{
										testPlanExecutionId:testPlanExecutionId,
										agentNames:arrAgentNames
									},
									true);
						});

					}
				};
			}
		}
		return bUpdated;
	}

	newTestPlan(){
		var self=this;
		var tpNew=new TestPlan();
		var newTestPlanExecutionId=genIdentification("TestPlan-Deferred");
		tpNew.executionId=newTestPlanExecutionId;
		self.dynamicTestPlans.set(newTestPlanExecutionId,tpNew);
		return tpNew;
	}
	newDeferredResult(scenarioInfo){
		var self=this;
		var dResult={
			running:false,
			executed:false,
			executionId:"",
			testPlanExecutionId:"",
			results:"",
			inAgent:"",
			srcAgent:scenarioInfo.srcAgent,
			inController:self.isController
		};
		return dResult;
	}
	prepareDeferredTestPlan(scenarioInfo){
		var self=this;
		var newExecutionId=genIdentification("Deferred");


		var dResult=self.newDeferredResult(scenarioInfo);
		var tp=self.newTestPlan();
		tp.useSelector=true;
		tp.deferredScenarios=true;
		tp.agentSelector=scenarioInfo.agentSelector;
		dResult.executionId=newExecutionId;
		dResult.planExecutionId=tp.executionId;

		var auxParams=scenarioInfo.parameters;


		var newScenario=tp.addScenario(
								scenarioInfo.scenarioKey,
								auxParams,
								scenarioInfo.environment,
								newExecutionId);
		newScenario.srcAgent=scenarioInfo.srcAgent;
		tp.pendingScenarios.set(newExecutionId,newScenario);
		tp.status="RUNNING";
		var arrReadyAgents=self.getReadyAgents();  
		for (var i=0;(!dResult.running)&&(i<arrReadyAgents.length);i++){
			var agent=arrReadyAgents[i];
			if (agent.name!==scenarioInfo.agentName){ // ensure don´t run the scenario in same agent....
				if (tp.isAgentSelected(agent)){ // can this agent execute the scenario?
					var scenario=tp.nextScenario(agent);
					scenario.deferred=true;
					debugger;
					if (scenario!==""){
						dResult.running=true;
						dResult.inAgent=agent.name;
						if (self.isACC) {
							agent.runScenario(scenario);
						} else {
							self.sendScenarioToAgent(agent,scenario);
						}
					}
				}
			}
		};
		if (!dResult.running){
			self.priorityTestPlans.set(tp.executionId,tp);
			self.notifyPriorityPlans();
		}
		return dResult;
	}
	async planDeferredScenario(scenarioInfo){
		var self=this;
		var dResult="";
		debugger;
		if ((self.controllerPushMessager.source==="")||
		    (typeof self.controllerPushMessager.source==="undefined")){ // there is not controller the ACC have to select the agent
			dResult=self.prepareDeferredTestPlan(scenarioInfo);
		} else { // there is a controller..... the controller have to select the agent in this ACC or in other ACC 
			dResult=self.newDeferredResult(scenarioInfo);
			var ctrlResult=await self.controllerPushMessager.sendMessage("RUN DEFERRED SCENARIO",scenarioInfo,true);			
		}
		return dResult;
	}
	async finishedDeferredScenario(scenarioExecutionId,executionResult,srcAgent){
		var self=this;
		var agent=self.mapAgents.get(srcAgent);
		agent.sendMessage("DEFERRED SCENARIO FINISHED",{executionId:scenarioExecutionId,result:executionResult});
	}

	notifyPriorityPlans(){
		var self=this;
		self.mapAgents.forEach(agent=>{
			var tpCounter=0;
			self.priorityTestPlans.forEach(testPlan=>{ 
				if (testPlan.isAgentSelected(agent)){
					tpCounter++;
				}
			})
			agent.sendMessage("UPDATE PRIORITY",tpCounter);
		});

	}
	async postToURL(url,data,bMultiPart){
		var self=this;
		var postData=data;
		if (typeof data==="undefined"){
			postData={};
		}
		postData.srcIp=self.ip;
		postData.accName=self.agentNamePrepend;
		postData.sourceElement=self.sourceElement;
		
		var fncResolve;
		var fncReject;
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		debugger;
		var postOptions={
		  url:     url,
		  rejectUnauthorized: false
		}
		if ((typeof bMultiPart!=="undefined")&&bMultiPart){
			var sData=JSON.stringify(postData);
			var formData  = {
				jsonContent:sData
			}
			postOptions.formData=formData;
			//postOptions.headers= {"Content-Type": "multipart/form-data"};
        
		} else {
			postOptions.headers= {'content-type' : 'application/json'};
			postOptions.json=postData;
		}
		
		request.post(postOptions, function(error, response, body){
			debugger;
			if (error!==null) {
				fncReject(error);
			} else {
				console.log(body);
				fncResolve(body);
			}
		});		
		
		
		return prmResult;
	}		

	async connectToController(controllerLink){
		var self=this;
		var output = LZUTF8.decompress(controllerLink, {inputEncoding:"Base64"});
		var oControllerInfo=JSON.parse(output);
		var pushMessager=new PushMessager(
								oControllerInfo.subscription
								,oControllerInfo.vapidKeys
								,self.chromiumGatewayPushMessager.destination
								,oControllerInfo.destination
								,oControllerInfo.ip);
		self.controllerPushMessager=pushMessager;
		console.log("Sending message to The  controller");
		var sysInfo=self.getSystemInfo();
		var statusInfo=self.getStatus();
		var isPing=false;
		try {
			debugger;
		    let res = await ping.promise.probe(oControllerInfo.ip);
		    console.log(res);
		    if (res.alive) {
		    	isPing=true;
		    } else {
		    	console.log("There is not PING to:"+oControllerInfo.ip);
		    }
		} catch (theError){
			consolelog(theError);
		}
		if (isPing){
			var controllerBaseUrl="https://"+oControllerInfo.ip+":8080";
			var connResult="";
			try {
				console.log("Testing direct https connection to controller (may take 3 minutes)");
				connResult=await self.postToURL(
									controllerBaseUrl+"/actions/directConnectionCheck",
									{message:"Checking HTTPS Direct Comms"});
			} catch (theError) {
				console.log(theError);
				connResult={enabled:false};
			}
		} else {
			connResult={enabled:false};
	    	console.log("It will no try to test HTTPS to:"+oControllerInfo.ip);
		}
		console.log("There is direct https connection to controller? "+connResult.enabled);
		debugger;
		if (connResult.enabled){
			connResult.controllerBaseUrl=controllerBaseUrl;
			if (connResult.bidirectional){
				connResult.accBaseUrl="https://"+connResult.srcIp+":8080";
			}
		}
		self.controllerPushMessager.httpConnection=connResult;
		var gtwLink=sysInfo.gatewayLink;
		if (typeof gtwLink==="undefined"){
			gtwLink=controllerLink;
		}
		await self.controllerPushMessager.sendMessage("ACC-CONNECT",{gatewayLink:gtwLink,httpConnection:connResult},false,false,false);
	}
	
	saveSeleniumInterface(agentName,itfcName){
		var self=this;
		debugger;
		if (self.isACC){
			var agent=self.getAgent(agentName);
			agent.saveSeleniumInterface(itfcName);
		}
	}
	
	async lockSystem(agent){
		var self=this;
		var fncResolve;
		var fncReject;
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		if (self.lockRunning.length==0){
			self.lockRunning.push({name:agent.name,agent:agent,resolve:fncResolve,reject:fncReject});
			setTimeout(function(){
				fncResolve("Locked by the agent:"+agent.name);
			},500);
		} else {
			var actLock=self.lockRunning[0];
			if (actLock.name==agent.name){
				self.lockRunning.push({name:agent.name,agent:agent,resolve:fncResolve,reject:fncReject});
				setTimeout(function(){
					fncResolve("Relocked by the agent:"+agent.name);
				},500);
			} else {
				self.lockWaiting.push({name:agent.name,agent:agent,resolve:fncResolve,reject:fncReject});
			}
		}
		return prmResult;
	}
	async unlockSystem(agent){
		var self=this;
		if (self.lockRunning.length==0){
			throw "The system is not locked";
			return;
		}
		var rootLock=self.lockRunning[0];
		if (rootLock.name!=agent.name){
			throw "The system is locked by "+self.rootLock.name+" and "+agent.name +" is trying to unlock";
			return;
		}
		var actLock=self.lockRunning.pop();
		if ((self.lockRunning.length==0)&&(self.lockWaiting.length>0)){
			var nextLock=self.lockWaiting.shift();
			self.lockRunning.push(nextLock);
			setTimeout(function(){
				var fncResolve=nextLock.resolve;
				fncResolve("Locked by the agent:"+nextLock.name);
			},500);
		}
	}

	async waitForAllLoaded(){
		console.log("waiting all cucumber to load");
		var self=this;
		var prmResolve="";
		var fncCheckAllLoaded=function(){
			var nUnloaded=0;
			self.mapAgents.forEach(function(agent){
				if ((agent.status=="UNLOADED")||(agent.status=="LOADING")){
					nUnloaded++;
				}
			});
			return (nUnloaded==0);
		}
		var retryCheck=function(){
			setTimeout(function(){
				var bAllLoaded=fncCheckAllLoaded();
				if (bAllLoaded){
					console.log("All cucumbers loaded");
					prmResolve("allLoaded");
				} else {
					console.log("Not All cucumbers loaded... retry check");
					retryCheck();
				}
			},1000);
		};
		var prmResult=new Promise((resolve, reject) => {
			prmResolve=resolve;
			retryCheck();
		});
		return prmResult;
	}
	getAgent(agentName){
		var self=this;
		if (self.mapAgents.has(agentName)){
			var agent=self.mapAgents.get(agentName);
			return agent;
		}
		return "";
	}
	addPropertyToAgent(agentName,property){
		var self=this;
		if (self.mapAgents.has(agentName)){
			var agent=self.mapAgents.get(agentName);
			if ((agent.status=="UNLOADED")&&(agent.dynamicAssign)){
				if (self.isACC) {
					agent.addProperty(property);
					self.sendUpdateStatus();
				} else {
					//debugger;
					var pushMessager=self.mapACCs.get(agent.accId).pushMessager;
					pushMessager.sendMessage("ADDPROPERTY",{agentName:agent.name,property:property},false,undefined,false);
				}
			}
		}
	}
	delAgentProperty(agentName,propertyName){
		var self=this;
		if (self.mapAgents.has(agentName)){
			var agent=self.mapAgents.get(agentName);
			if ((agent.status=="UNLOADED")&&(agent.dynamicAssign)){
				if (self.isACC) {
					agent.delProperty(propertyName);
					self.sendUpdateStatus();
				} else {
					//debugger;
					var pushMessager=self.mapACCs.get(agent.accId).pushMessager;
					pushMessager.sendMessage("DELPROPERTY",{agentName:agent.name,propertyName:propertyName},false,undefined,false);
				}
			}
		}
	}

	async loadAgent(agentName){
		var self=this;
		if (self.mapAgents.has(agentName)){
			var agent=self.mapAgents.get(agentName);
			if ((agent.status=="UNLOADED")&&(agent.dynamicAssign)){
				if (self.isACC) {
					agent.run();
				} else {
					//debugger;
					var pushMessager=self.mapACCs.get(agent.accId).pushMessager;
					pushMessager.sendMessage("LOAD_AGENT",{agentName:agent.name},false,undefined,false);
				}
			}
		}
	}
	async reloadAgent(agentName){
		var self=this;
		if (self.mapAgents.has(agentName)){
			var agent=self.mapAgents.get(agentName);
			if ((agent.status=="UNLOADED")&&(agent.dynamicAssign)){
				agent.run();
			} else {
				agent.reload();
			}
		}
	}
	loadAllForDynamic(){
		var self=this;
		console.log("Loading all agents for Dynamic");
		self.mapAgents.forEach(agent=>{
			self.loadAgent(agent.name);
		});
	}

	
	async finishEventProcessing(textToLog,withUpdateStatus){
		var self=this;			
		if ((typeof withUpdateStatus==="undefined")
			 ||((typeof withUpdateStatus!=="undefined")&&withUpdateStatus)){
			self.sendUpdateStatus(textToLog);
		}
		messageHandler.finishProcessingActualMessage();
	}
	
	async sendUpdateStatus(textToLog){
		var self=this;
		if ((typeof textToLog!=="undefined")&&(textToLog!=="")){
			console.log(textToLog);
		}
		self.sendUpdateToWebUIs(textToLog);
		self.sendStatusToController();
	}
	async sendStatusToController(){
		var self=this;
		var oStatus=self.getStatus();
		var pushMessager="";
		if (self.controllerPushMessager!==""){
			pushMessager=self.controllerPushMessager;
		}
		if (pushMessager!==""){
			pushMessager.sendMessage("CTRL-UPDATE",oStatus,false,undefined,false);
		}
	}
	sendUpdateToWebUIs(txtMsg){
		var self=this;
		setTimeout(function(){
			var pushMessager="";
			if (self.chromiumGatewayPushMessager!==""){
				pushMessager=self.chromiumGatewayPushMessager;
			} else if (self.chromiumRegisteredWebUIs.size>0){
				pushMessager=self.chromiumRegisteredWebUIs.values().next().value;
			} else if (self.chromiumUnregisteredWebUIs.size>0){
				pushMessager=self.chromiumUnregisteredWebUIs.values().next().value;
			}
			if (pushMessager!==""){
				pushMessager.sendMessage(
					"ACC-UPDATEUI",
					{
						theEvent:txtMsg
					},
					false,undefined,true
					);
			}
		},1000);
	}
	getWebUI(webuiId){
		var self=this;
		if (
			(self.chromiumGatewayPushMessager!=="")&&
			(self.chromiumGatewayPushMessager.destination==webuiId)
			){
			return self.chromiumGatewayPushMessager;
		} else if (self.chromiumRegisteredWebUIs.has(webuiId)){
			return self.chromiumRegisteredWebUIs.get(webuiId);
		} else if (self.chromiumUnregisteredWebUIs.has(webuiId)){
			return self.chromiumUnregisteredWebUIs.get(webuiId);
		}
		return "";
	}

	getStatus(){
		var self=this;
		var oResult={};
		oResult.isACC=self.isACC;
		oResult.isController=self.isController;

		if (self.chromiumGatewayPushMessager!==""){
			oResult.gateway=self.chromiumGatewayPushMessager.destination;
		} else {
			oResult.gateway="";
		}
		oResult.registeredWebUIs=[];
		self.chromiumRegisteredWebUIs.forEach((value, key, map)=>{
			oResult.registeredWebUIs.push({id:value.destination});
		});
		oResult.unregisteredWebUIs=[];
		self.chromiumUnregisteredWebUIs.forEach((value, key, map)=>{
			oResult.unregisteredWebUIs.push({id:value.destination});
		});
		
		if (self.isACC){
			oResult.agentNamePrepend=self.agentNamePrepend;
			if (self.controllerPushMessager===""){
				oResult.controller="";
			} else {
				oResult.controller=self.controllerPushMessager.destination;
			}
		} else { //is controller
			oResult.nACCs=0;
			oResult.ACCs=[];
			self.mapACCs.forEach(acc=>{
				oResult.nACCs++;
				var thePushMessager=acc.pushMessager;
				oResult.ACCs.push({id:thePushMessager.destination,statusInfo:acc.statusInfo});
			});
		}
		oResult.nAgents=self.nAgents;
		oResult.totalScreens=self.totalScreens;
		oResult.agents=[];
		var nLoaded=0;
		var nBusy=0;
		self.mapAgents.forEach(agent=>{
			var oAgent="";
			if (typeof agent.getStatus!=="undefined"){
				oAgent=agent.getStatus();
				if (oAgent.status!=="UNLOADED"){
					nLoaded++;
				}
				if (oAgent.busy){
					nBusy++;
				}
				self.addAgentScenarios(agent,oResult.gateway);
			} else {
				//debugger;
				oAgent={
					actualScreen: agent.actualScreen
					,busy: agent.busy
					,dynamicAssign: agent.dynamicAssign
					,feedback: agent.feedback
					,name: agent.name
					,processedScenarios: agent.processedScenarios
					,runningScenario: agent.runningScenario
					,status: agent.status
					,accId:agent.accId
					,properties:agent.properties
				};
				if (oAgent.busy) nBusy++;
				if (oAgent.status!=="UNLOADED"){
						nLoaded++;
				}
			};
			oResult.agents.push(oAgent);
		});
		oResult.nScenarios=0;
		oResult.scenarios=[];
		self.mapScenarios.forEach(scenario=>{
			oResult.nScenarios++;
			var oAgents=scenario.agents;
			var arrAgents=[];
			oAgents.forEach(agent=>{
				arrAgents.push(agent.agentName);
			});
			oResult.scenarios.push({key:scenario.key,name:scenario.name,agents:arrAgents,params:scenario.params});
		});
		//debugger;
		oResult.nTestPlans=0;
		oResult.testPlans=[];
		self.mapStoredTestPlans.forEach(testPlan=>{
			oResult.testPlans.push(testPlan.getStatus());
		});
		oResult.executions={
			normal:{
				running:[],
				finished:[]
			},
			priority:{
				running:[],
				finished:[]
			},
			scheduled:{
				running:[],
				finished:[]
			}
		}
		self.priorityTestPlans.forEach(testPlan=>{
			oResult.executions.priority.running.push(testPlan.getStatus());
		});
		self.runningTestPlans.forEach(testPlan=>{
			oResult.executions.normal.running.push(testPlan.getStatus());
		});
		self.scheduledTestPlans.forEach(testPlan=>{
			oResult.executions.scheduled.running.push(testPlan.getStatus());
		});
		var arrTypes=['normal','priority','scheduled'];
		arrTypes.forEach(type=>{
			self.finishedTestPlans[type].forEach(testPlan=>{
				oResult.executions[type].finished.push(testPlan.getStatus());
			});
		});
		
		oResult.loadedAgents=nLoaded;
		oResult.busyAgents=nBusy;
		
		
		return oResult;
	}
	runScenarios(lstScenarios,dinamic,agents){
		var self=this;
		var bDinamic=false;
		if (typeof dinamic!=="undefined"){
			bDinamic=dinamic;
		}
		var mapFreeAgents=new Map();
		var nBusyAgents=0;
		var nFreeAgents=0;
		for (var i=0;i<self.nAgents;i++){
			var agent=self.mapAgents.get(self.agentNamePrepend+i);
			if (!agent.busy){
				nFreeAgents++;
				mapFreeAgents.set(agent.name,agent);
			} else {
				nBusyAgents++;
			}
		}
		console.log("Status.... free:"+nFreeAgents+" busy:"+nBusyAgents);
		if ((typeof agents==="undefined")||(Array.isArray(agents))) {
			var arrFreeAgents=[];
			if (typeof agents==="undefined"){
				var itAgents=mapFreeAgents.values();
				var nAgents=0;
				for (agent of itAgents) {
					console.log(nAgents+" "+agent.name);
					arrFreeAgents.push(agent);
				}
			} else {
				agents.forEach(agent=>{
					if (mapFreeAgents.has(agent.name)){
						arrFreeAgents.push(agent);
					}
				});
			}
			var nFreeAgents=arrFreeAgents.length;
			console.log("Free Agents:"+nFreeAgents);
			if (nFreeAgents==0){
				console.log("There is not free agents");
			} else {
				lstScenarios.forEach(function(scenario,index){
					var idxAgent=(index % nFreeAgents);
					var agent=arrFreeAgents[idxAgent];
					agent.dynamicAssign=bDinamic;
					agent.addScenario(scenario);
				});
				arrFreeAgents.forEach(agent=>{
					agent.run();
				});
			}
		} else if (typeof agents==="string"){
			var agent=self.mapAgents.get(agents);
			lstScenarios.forEach(function(scenario){
				agent.addScenario(scenario);
			});
			if (!agent.busy) {
				agent.dynamicAssign=bDinamic;
				agent.run();
			} else if ((!agent.dynamicAssign)||(!bDinamic)){
				console.log("Cannot assing dinamic:" + bDinamic + " test to a busy "+agent.name+" agent in running dinamic:"+agent.dynamicAssign);
			}
		}
	}
	updateSystem(sNameScript){
		debugger;
		var theCommand='/usr/src/app/'+sNameScript;
		var vResult= execSync(theCommand);
		return vResult;
	}
	getSystemInfo(){
		var self=this;
		var sTitle=(self.isACC?"Agents Container Controller":"Testing Controller");
		var oResult={
			isController:self.isController
			,isACC:self.isACC
			,title:sTitle
		}
		if (self.chromiumGatewayPushMessager!==""){
			var pushMgr=self.chromiumGatewayPushMessager;
			oResult.subscription=pushMgr.pushSubscription;
			var vapidKeys=pushMgr.vapidKeys;
			oResult.vapidKeys=vapidKeys;
			oResult.vapidPublicKey=vapidKeys.publicKey;
			oResult.destination=pushMgr.destination;
			oResult.source=pushMgr.source;
			
			var oLink={
					vapidKeys:vapidKeys
					,subscription:oResult.subscription
					,destination:oResult.destination
					,source:oResult.source
					,ip:self.ip};
			var sLink=JSON.stringify(oLink);
			//console.log("SLINK ("+sLink.length+"):"+sLink);
			var sLinkResult = LZUTF8.compress(sLink,{outputEncoding:"Base64"});
			//console.log("SLINK B64 ("+sLinkResult.length+"):"+sLinkResult);
			oResult.gatewayLink=sLinkResult;
			
			if (self.isController) {
				fs.writeFile("./controllerLink/controllerLink.txt",sLinkResult, function(err) {
				    if(err) {
				        return console.log(err);
				    }
				    console.log("The gateway link file was saved!");
				}); 
			}
		}
		return oResult;
	}
}
var agentManager=new AgentManager();
module.exports=agentManager;