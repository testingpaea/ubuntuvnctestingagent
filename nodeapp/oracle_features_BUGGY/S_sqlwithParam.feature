Feature: Definición de una consulta SQL simple. Obtener registros de la tabla persona con parametro DNI

@SQL
 Scenario: Obtener personas con DNI
 	Given scenario default variables
			"""
			{"$usuario":"SANCHO"}
			"""
	Given extract user attributes of "$usuario"
	Given add sql id "Obtener personas con DNI" with
	"""
	{
	"description":"Obtener todos los registros de la tabla PERSONA que coincidan con el DNI",
	"sql":"SELECT * FROM PERSONA WHERE DNI='$PARAM_DNI'"
	}
	"""