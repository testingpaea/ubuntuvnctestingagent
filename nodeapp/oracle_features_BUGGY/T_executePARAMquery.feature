Feature: Ejecutar una consulta con parametros sobre una base de datos

@ISSUE:TEST-2
  Scenario: Ejecuta la consulta con parametros sobre una base de datos
	Given scenario default variables
			"""
			{"$usuario":"JUAN"}
			"""
	Given the scenario "base de datos de pruebas de concepto"
	Given extract user attributes of "$usuario"
	Given save in variable "sqlResult" the result of execute, in oracle database "orclTestDirect", the SQL "Obtener personas con DNI" with params
			"""
			{"$PARAM_DNI":"(#($DNI)#)"}
			"""
