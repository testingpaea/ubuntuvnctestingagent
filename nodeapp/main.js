'use strict';
const genIdentification = require('./helpers/identification.lib');
var instanceId=genIdentification('Element');
const agentManager = require('./agentManager/agentManager');
const Scenario = require('./agentManager/scenario')

console.log("=====ALL ENVIRONMENT VARIABLES======");
console.log(process.env);
console.log("===========");
console.log("ENV_USER:"+process.env.ENV_USER);
console.log("NUM_AGENTS:"+process.env.withNumAgents);
console.log("Env is Controller:"+process.env.withIsController);


var isGeneralController=false;
var isAgentsContainerController=true;
if ((typeof process.env.withIsController!=="undefined")&&process.env.withIsController){
	isGeneralController=true;
	isAgentsContainerController=false;
} 

var agentProperties=new Map();
if (typeof process.env.withAgentProperties!=="undefined"){
	var jsonProps="{"+process.env.withAgentProperties+"}";
//	var jsonProps=process.env.withAgentProperties;
	var oProps=JSON.parse(jsonProps);
	for(var key in oProps) {
		var value = oProps[key];
		agentProperties.set(key,{name:key,value:value});
	}
}

agentManager.sourceElement=instanceId;
var nDisplays=0;
try {
	require('dns').lookup(require('os').hostname(), function (err, add, fam) {
	  console.log('IP DEL NODO: '+add);
	  agentManager.ip=add;
	})
} catch (theError){
	console.log(theError);
}
//setTimeout(function(){
	if (isAgentsContainerController){
		if ((typeof process.env.withNumAgents=="undefined")
			 ||(process.env.withNumAgents=="")){
			nDisplays=3;
		} else {
			nDisplays=parseInt(process.env.withNumAgents);
		}
		agentManager.initializeAsACC(nDisplays,agentProperties);
		require("./webui/webui.js");
		//setTimeout(function(){
			//agentManager.loadAgent("AGENT_0");
			//agentManager.loadAgent("AGENT_1");
			//agentManager.loadAgent("AGENT_2");
		//},10000);
	} else {
		nDisplays=0;
		agentManager.initializeAsController();
		require("./webui/webui.js");
	}
//	},10000);
//var arrScenarios=[];
//arrScenarios.push(new Scenario("SNT-831"));
//arrScenarios.push(new Scenario("SNT-832"));

//arrScenarios.push(new Scenario("TEST-1"));
//arrScenarios.push(new Scenario("TEST-2"));
//arrScenarios.push(new Scenario("SNT-833"));

//arrScenarios.push(new Scenario("SNT-833"));
//arrScenarios.push(new Scenario("SNT-833"));
//arrScenarios.push(new Scenario("SNT-833"));
/*arrScenarios.push(new Scenario("SNT-833"));
arrScenarios.push(new Scenario("SNT-833"));
arrScenarios.push(new Scenario("SNT-833"));
arrScenarios.push(new Scenario("SNT-833"));
arrScenarios.push(new Scenario("SNT-833"));
*///arrScenarios.push(new Scenario("SNT-834"));
//arrScenarios.push(new Scenario("SNT-835"));
/*var fncLaunch=async function(){
	agentManager.loadAllForDynamic();
	await agentManager.waitForAllLoaded();
	console.log("Run the Scenarios");
	agentManager.runScenarios(arrScenarios,true);
}
*/
//fncLaunch();
