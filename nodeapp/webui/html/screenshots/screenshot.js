var theFrequency=5000;
var theUrl='/actions/getScreenshot';

var url_string = window.location.href;
var url = new URL(url_string);
var agentName=url.searchParams.get("agentName");


async function postToAcc(data){
	var postData=data;
	if (typeof data==="undefined"){
		postData={};
	}
	postData.agentName=agentName;
	let response = await fetch(theUrl, {
		  method: 'post',
		  headers: {
			'Content-type': 'application/json'
		  },
		  body: JSON.stringify(postData)
		});
	let oResult = await response.blob();
    return oResult;
}


async function updateScreenshot(){
	try {
		var sendInfo={
			agentName:agentName
		};
		var dataResult=await postToAcc(sendInfo);
		var blobData = dataResult;
		var url = window.URL || window.webkitURL;
		var src = url.createObjectURL(dataResult);
		
		$('#screenshotImage').attr("src", src);
	} catch (error) {
		// do nothing
	}
	setTimeout(updateScreenshot,theFrequency);
}


function initializeViewer(){
	updateScreenshot();
}