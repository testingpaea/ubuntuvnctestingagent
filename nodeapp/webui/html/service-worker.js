var iMsgSendCounter=0;
console.log("Service Worker Base");
var multiPartMessages=new Map();
// Register event listener for the 'push' event.
self.addEventListener('push', function(event) {
  // Keep the service worker alive until the notification is created.
	var bNoData=false;
	var payload ="";
	if (event.data){
	   payload = event.data.text();
	} else {
	  payload = "No message Body";
	  bNoData=true;
	}
	console.log(payload.substring(0,100)+"...");
	var bMultiPart=false;
	if (!bNoData){
		var oJson=JSON.parse(payload);
		if (false &&(typeof oJson.multipart!=="undefined")){ //NEVER THIS BRANCH
			bMultiPart=true;
			//var oPart={msgId:oPayload.msgId,multipart:true,parts:nParts,partNumber:iPart,partContent:contentPart};
			var msgId=oJson.msgId;
			var nParts=oJson.parts;
			var iPart=oJson.partNumber;
			var partContent=oJson.partContent;
			var oTempPartManager;
			if (!multiPartMessages.has(msgId)){
				oTempPartManager={msgId:msgId,nParts:nParts,nPartsFilled:0,parts:[]};
				multiPartMessages.set(msgId,oTempPartManager);
				for (var i=0;i<nParts;i++){
					oTempPartManager.parts.push({status:"empty",content:""});
				}
			} else {
				oTempPartManager=multiPartMessages.get(msgId);
			}
			oTempPartManager.nPartsFilled++;
			oTempPartManager.parts[iPart].status="filled";
			oTempPartManager.parts[iPart].content=partContent;
			if (oTempPartManager.nPartsFilled==oTempPartManager.nParts){
				multiPartMessages.delete(msgId);
				var fullContent="";
				for (var i=0;i<oTempPartManager.nParts;i++){
					var part=oTempPartManager.parts[i];
					if (part.status!="filled"){
						debugger;
						console.log("Error with multipart message");
					}
					fullContent+=part.content;
				}
				debugger;
				send_message_to_all_clients({
					msgType:"SW-PUSHRECEIVED",
					msgContent:fullContent,
				});
			}
		} else {
			send_message_to_all_clients({
				msgType:"SW-PUSHRECEIVED",
				msgContent:payload,
			});
		}
	} else {
		send_message_to_all_clients({
			msgType:"SW-PUSHRECEIVED",
			msgContent:payload,
		});
	}
});

function send_message_to_client(client, msg){
    return new Promise(function(resolve, reject){
        var msg_chan = new MessageChannel();

        msg_chan.port1.onmessage = function(event){
            if(event.data.error){
                reject(event.data.error);
            }else{
                resolve(event.data);
            }
        };

        client.postMessage(msg, [msg_chan.port2]);
    });
}
function send_message_to_all_clients(msg){
	msg.msgId=iMsgSendCounter;
	iMsgSendCounter++;
    clients.matchAll({
    includeUncontrolled: true
	}).then(clients => {
        clients.forEach(client => {
            send_message_to_client(client, msg);//.then(m => console.log("SW Received Message: "+m));
        })
    })

}
self.addEventListener('message', function(event){
    console.log("SW Received Message: " + event.data);
    event.ports[0].postMessage("SW Says 'Hello back!'");
});
self.addEventListener('activate', function(event) {
	console.log("Activate");
	event.waitUntil(self.clients.claim());
	send_message_to_all_clients({
		msgType:"SW-ALIVE",
		msgContent:"Hello I'm the service worker... i´m alive!",
	});
});
