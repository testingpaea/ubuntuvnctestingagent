var genIdentification = require('../../helpers/identification.lib');
var snappy = require('snappy');
var zlib = require('zlib');

var fncEmitPush=function(pushSubscription,vapidKeys,oPayload){
	//console.log("setting vapid:"+vapidKeys.publicKey+" \n\n"+vapidKeys.privateKey);
	var webpush = require('web-push');
	webpush.setVapidDetails(
	  'https://localhost:8080',
	  vapidKeys.publicKey,
	  vapidKeys.privateKey
	);
	const options = {
	  TTL: 10
	};
	var payload = JSON.stringify(oPayload);
	var payLoadLength=payload.length;
	var fncSendANot=function(thePayLoad){
		webpush.sendNotification(
		  pushSubscription,
		  thePayLoad
		  , options
		).catch(function(err){
			console.log("ERROR:"+err);
		});	
	};
	fncSendANot(payload);
}  
async function pushManagerWatchDog(pushManager){
	var bContinueWatching=true;
	var bSendUpdateUIMessage=false;
	if (!pushManager.isConnected()){
		const agentManager= require('../../agentManager/agentManager');
		if (agentManager.chromiumRegisteredWebUIs.has(pushManager.destination)){
			agentManager.chromiumRegisteredWebUIs.delete(pushManager.destination);
			agentManager.chromiumUnregisteredWebUIs.set(pushManager.destination,pushManager);
			bSendUpdateUIMessage=true;
		} else if (pushManager.getTimeUnregistered()>pushManager.watchDogTimeToDelete){
			if (agentManager.chromiumUnregisteredWebUIs.has(pushManager.destination)){
				agentManager.chromiumUnregisteredWebUIs.delete(pushManager.destination);
				bSendUpdateUIMessage=true;
			}
			bContinueWatching=false;
		}
	}
	if (bContinueWatching){
		setTimeout(function(){
			pushManagerWatchDog(pushManager);
		},pushManager.watchDogTime);
	} else {
		pushManager.watchDogActive=false;
	}
	if (bSendUpdateUIMessage){
		pushManager.sendMessage("ACC-UPDATEUI",{
									theEvent:"Change status of webui:"+pushManager.destination
								},false,undefined,true);
	}

}

var PushMessager=class PushMessager{
	constructor(pushSubscription,vapidKeys,srcElement,dstElement,ip){
		var self=this;
		self.maxMessageLength=2048;
		self.pushSubscription=pushSubscription;
		self.vapidKeys=vapidKeys;
		self.ip=ip;
		self.source=srcElement;
		if (typeof dstElement==="undefined"){
			self.destination=genIdentification("webui");
		} else {
			self.destination=dstElement;
		}
		self.mapWaitingACK=new Map();
		self.lastAck="";
		self.watchDogActive=false;
		self.watchDogTime=1000*60*2;//1000*60*10;
		self.watchDogTimeToDelete=self.watchDogTime*2;
		self.activateWatchDog();
		self.sendingMessageList=[];
		self.sendingMessage="";
		self.sendingMessageIndex=0;
	}
	activateWatchDog(){
		var self=this;
		if (!self.watchDogActive){
			self.watchDogActive=true;
			setTimeout(function(){
				pushManagerWatchDog(self);
			},self.watchDogTime);
		}
	}
	getTimeUnregistered(){
		var self=this;
		var timeUnregistered=
			(new Date()).getTime()
			-(self.lastAck===""?(new Date()).getTime():self.lastAck);
		return timeUnregistered;
	}
	isConnected(){
		var self=this;
		var isConnectionActive=self.getTimeUnregistered()<self.watchDogTime;
		//console.log("Connection is active "+isConnectionActive+" uid:"+self.destination);
		return isConnectionActive;
	}
	prepareNeedAck(oMessage,onAck){
		var self=this;
		var prmResult="";
		if ((typeof oMessage.msgNeedsAck!=="undefined")&&(oMessage.msgNeedsAck)){
			oMessage.msgTimeSend=(new Date()).getTime();
			if (typeof onAck!=="undefined"){ // with callback
				oMessage.ack=onAck;
			} else {  // it will await.... 
				//debugger;
				var fncResolve="";
				var fncReject="";
				var prmResult=new Promise((resolve, reject) => {
					fncResolve=resolve;
					fncReject=reject;
				});
				oMessage.ack=function(){
					fncResolve("Received ack for "+oMessage.msgId);
				}
			}
			self.mapWaitingACK.set(oMessage.msgId,oMessage);
		}
		return prmResult;
	}
	async send(oMessage,onAck){
		var self=this;
		var prmResult="";
		if (typeof oMessage.msgId==="undefined"){
			oMessage.msgId=genIdentification("message");
			oMessage.msgDestination=self.destination;
			oMessage.msgSource=self.source;
			prmResult=self.prepareNeedAck(oMessage,onAck);
		}
		/*console.log(
			"Subscription:"+JSON.stringify(self.pushSubscription)+" \n\n" +
			"keys:"+JSON.stringify(self.vapidKeys)+" \n\n" +
			"message:"+JSON.stringify(oMessage));	
		*/			
		fncEmitPush(
			self.pushSubscription, 
			self.vapidKeys,
			oMessage);
		return prmResult;
	} 
	
	async compress(sCad){
		var fncResolve="";
		var fncReject="";
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		zlib.gzip(sCad, function (error, result) {
			//console.log(result);
		   if (error) {
			   fncReject(error);
		   } else {;
				var b64Result=result.toString('base64');
				fncResolve(b64Result);
		   }
		})
		return prmResult;
	}

	async prepareMessage(oMessage){
		var self=this;
		var payload = JSON.stringify(oMessage);
		var PayLoadMaxLength=self.maxMessageLength;
		var payLoadLength=payload.length;
		//console.log("Message length:"+payLoadLength + " max:"+PayLoadMaxLength);
		//console.log("Message to Send:"+payload);
		var arrMsgs=[];
		if (payLoadLength<PayLoadMaxLength){
			arrMsgs.push(oMessage);
		} else {
			var arrParts=[];
			//let buff = new Buffer(payload);  
			//var tmpPayLoad = buff.toString('base64');
			var tmpPayLoad=await self.compress(payload);
			//debugger;
	/*		snappy.compress(payload, function (err, compressed) {
				
				//console.log('compressed is a Buffer', compressed)
				
				var zippedPayLoad= compressed.toString('base64');
				var tmpPayLoad=zippedPayLoad;
	*/			
				// return it as a string
				/*snappy.uncompress(compressed, { asBuffer: false }, function (err, original) {
					console.log('the original String', original)
				})*/
				while (tmpPayLoad.length>0){
					arrParts.push(tmpPayLoad.substring(0,PayLoadMaxLength));
					tmpPayLoad=tmpPayLoad.substring(PayLoadMaxLength,tmpPayLoad.length);
				}
				
				var nParts=arrParts.length;
				var srcMsgId=genIdentification("message");
				for (var iPart=0;iPart<arrParts.length;iPart++){
					var contentPart=arrParts[iPart];
					var subMsg={ msgType:"MULTIPART_PART"
								,msgNeedsAck:true
								,msgContent:{
									srcMsgId:srcMsgId
									,partContent:contentPart
									,parts:nParts
									,partNumber:iPart
									}
								};
					arrMsgs.push(subMsg);
				};
	//		});
		}
		return arrMsgs;	
	}
	

	
	async sendMessage(type,content,needsAck,onAck,toAllUIs){
		var self=this;
		if ((typeof toAllUIs!=="undefined")&&(toAllUIs)){
			const agentManager= require('../../agentManager/agentManager');
			if (agentManager.chromiumGatewayPushMessager!==""){
				await agentManager.chromiumGatewayPushMessager.sendMessage(type,content,needsAck,onAck);
			}
			agentManager.chromiumRegisteredWebUIs.forEach((webui, key, map)=>{
				 webui.sendMessage(type,content,needsAck,onAck);
			});		
		} else {
			//debugger;
			var oMessage={msgType:type,msgContent:content,msgNeedsAck:needsAck};
			oMessage.msgId=genIdentification("message");
			oMessage.msgDestination=self.destination;
			oMessage.msgSource=self.source;
			self.prepareNeedAck(oMessage,onAck);
			var arrMessageParts=await self.prepareMessage(oMessage);
			self.sendingMessageList.push(arrMessageParts);
			if (self.sendingMessage===""){
				while (self.sendingMessageList.length>0){
					arrMessageParts=self.sendingMessageList.shift();
					self.sendingMessage=arrMessageParts;
					for (var i=0;i<arrMessageParts.length;i++){
						self.sendingMessageIndex=i;
						await self.send(arrMessageParts[i]);
						if (arrMessageParts.length>1){
							console.log("sended part "+(i+1)+" of " + arrMessageParts.length);
						}
					}
				}
				self.sendingMessage="";
			}
			
		}
	}
	async sendACK(msgId){
		var self=this;
		await self.sendMessage("ACK",{srcMsgId:msgId});
	}
	
	async ack(msgId){
		var self=this;
		if (!self.mapWaitingACK.has(msgId)){
			return false;
		}
		//console.log("ACK in "+ self.destination);
		self.lastAck=(new Date()).getTime();
		const agentManager= require('../../agentManager/agentManager');
		if (agentManager.chromiumUnregisteredWebUIs.has(self.destination)){
			agentManager.chromiumUnregisteredWebUIs.delete(self.destination);
			agentManager.chromiumRegisteredWebUIs.set(self.destination,self);
			self.activateWatchDog();
		}
		var theMsg=self.mapWaitingACK.get(msgId);
		self.mapWaitingACK.delete(msgId);
		await theMsg.ack();
		return true;
	}
}

module.exports=PushMessager;