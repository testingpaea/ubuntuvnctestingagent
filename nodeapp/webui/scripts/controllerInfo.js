var ControllerInfo=class ControllerInfo extends AccInfo{
	constructor(oControllerStatus){
		super(oControllerStatus);
		var self=this;
		self.interfaceType="Controller";
		self.scenarios=new Map();
	}
	refresh(oControllerStatus){
		super.refresh(oControllerStatus);
		var self=this;
		self.nAgents=oControllerStatus.nAgents;
		self.loadedAgents=oControllerStatus.loadedAgents;
		self.busyAgents=oControllerStatus.busyAgents;
	}

	updateControllerHtml(){
		var self=this;
		debugger;
		var arrACCs=self.status.ACCs;
		var nAgents=0;
		var nLoaded=0;
		var nBusy=0;
		arrACCs.forEach(acc=>{
			var accStatus=acc.statusInfo;
			nAgents+=accStatus.nAgents;
			nLoaded+=accStatus.loadedAgents;
			nBusy+=accStatus.busyAgents;
		});
		self.nAgents=nAgents;
		self.loadedAgents=nLoaded;
		self.busyAgents=nBusy;
		self.updateAgentsSummaryHtml();
		var oResult="Registered ACCs: "+arrACCs.length;
		var divDetail=$("#controllerDetail");
		divDetail.html(oResult);
	}
	updateHtml(){
		super.updateHtml();
		var self=this;
		self.updateControllerHtml();
	}
}
