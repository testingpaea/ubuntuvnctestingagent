var ControllerInfo=class ControllerInfo extends EngineInfo{
	constructor(oControllerStatus){
		super(oControllerStatus);
	}
	refresh(oControllerStatus){
		super.refresh(oControllerStatus);
		var self=this;
		self.nAgents=oAccStatus.nAgents;
		self.loadedAgents=oAccStatus.loadedAgents;
		self.busyAgents=oAccStatus.busyAgents;
	}

	getAgentScenarioListHtml(agent){
		var scenarios=agent.scenarios;
		var sOptions="";
		if (scenarios.length>0) {
			var selScenariosName="selScenarios_"+agent.name;
			scenarios.forEach(function(option){
				sOptions+='<option value="'+option.key+'">'+option.key+"-"+option.name+'</option>';
			});
			var sHtml="";
			sHtml+='<button onclick="runScenarios(\''+agent.name+'\',\''+selScenariosName+'\')">run</button>';
			sHtml+='<select id="'+selScenariosName+'" multiple  size="10">';
			sHtml+=sOptions;
			sHtml+='</select>';
			sHtml+='<button onclick="runScenarios(\''+agent.name+'\',\''+selScenariosName+'\')">run</button>';
		}
		return sHtml;
	}
	getAgentProcessResultsHtml(agent){
		var sHtml="";
		var i=0;
		agent.processedScenarios.forEach(results=>{
			var j=0;
			if (Array.isArray(results)){
				results.forEach(result=>{
					sHtml+="<hr> Name:"+result.name+" Result:"+result.state+" Time:"+result.time;
					sHtml+='<button onclick="showResultDetail(\''+agent.name+'\','+i+','+j+')">details</button>';
					j++;
				});
			} else {
				var result=results;
				sHtml+="<hr> Name:"+result.name+" Result:"+result.state+" Time:"+result.time;
				sHtml+='<button onclick="showResultDetail(\''+agent.name+'\','+i+','+j+')">details</button>';
				j++;
			}
			i++;
		});
		return sHtml;
	}		
	getAgentFeedbackHtml(agent){
		if (agent.feedback===""){
			return "";
		}
		if (agent.feedback.type=="message"){
			return agent.feedback.text;
		} 
		if (agent.feedback.type=="inputbox"){
			var sResult="";
			if (agent.feedback.password){
				sResult='type="password"';
			}
			sResult='<input '+sResult+' id="inputAgent_'+agent.name+'" ><button id="btnSendInputToAgent_'+agent.name+'" onclick="sendFeedback(\''+agent.name+'\',\'inputAgent_'+agent.name+'\')" >send</button>';
			return agent.feedback.text+sResult;
		} 	
	}
	addAgentRowHtml(agent){
		var self=this;
		$("#tblAgents").append("<tr><td>"
			+agent.name+"</td><td>" 
			+agent.status+'<button id="'+"btnLoadAgent"+"_"+agent.name+'" '
			+(agent.status=="UNLOADED"?
				' onclick="loadAgent(\''+agent.name+'\')" type="button">Load'
				: ' onclick="reloadAgent(\''+agent.name+'\')" type="button">Reload')
			+'</button>'
			+'</td><td>' 
			+agent.busy+"</td><td>" 
			+agent.dynamicAssign+"</td><td>" 
			+self.getAgentScenarioListHtml(agent)+"</td><td>" 
			+(agent.runningScenario!==""?agent.runningScenario.id+ " - " + agent.runningScenario.status:"")+"</td><td>" 
			+self.getAgentProcessResultsHtml(agent)+"</td><td>" 
			+self.getAgentFeedbackHtml(agent)+"</td>" 
			+"</tr>");
	}
	updateAgentListHtml(){
		var self=this;
		var tableRows=$("#tblAgents > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		self.status.agents.forEach(function(agent){
			self.addAgentRowHtml(agent);
		});
	}
	getAgentDetail(agentName){
		var self=this;
		var oResult="";
		self.status.agents.forEach(function(agent){
			if (agent.name==agentName){
				oResult=agent;
			}
		});
		return oResult;
	}
	updateHtml(){
		super.updateHtml();
		var self=this;
		self.updateAgentListHtml();
	}
}
