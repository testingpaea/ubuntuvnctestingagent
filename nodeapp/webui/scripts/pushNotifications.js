/*
*
*  Push Notifications codelab
*  Copyright 2015 Google Inc. All rights reserved.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      https://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License
*
*/

/* eslint-env browser, es6 */

'use strict';
//const applicationServerPublicKey = '<Your Public Key>';
//alert(applicationServerPublicKey);
let isSubscribed = false;
let swRegistration = null;
var pushButton;
var notificationServer="";
var isGateWay=false;


if('serviceWorker' in navigator){
    // Handler for messages coming from the service worker
    navigator.serviceWorker.addEventListener('message', function(event){
		var sData="Browser Received Message from Service Worker: " + event.data;
        console.log(sData);
		$("#gatewayStatus").text(sData);
		var xhr = new XMLHttpRequest();
		var url = '/actions/msgReceived';
		if (isGateWay){
			url+="?gateway=true";
		}
		var params = '';
		xhr.open('POST', url, true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				var json = JSON.parse(xhr.responseText);
				console.log(json);
			}
		};
		var data = sData;
		xhr.send(data);

    });
}

function initializePushSystem(notificationServerUrl,bIsGateway=false){
	console.log("Initializing worker");
	isGateWay=bIsGateway;
	pushButton= document.getElementById("btnEnablePush");
	console.log(pushButton);
	notificationServer=notificationServerUrl;
	if ('serviceWorker' in navigator && 'PushManager' in window) {
	  console.log('Service Worker and Push is supported');

	  navigator.serviceWorker.register('/scripts/sw.js')
	  .then(function(swReg) {
		console.log('Service Worker is registered', swReg);
		swRegistration = swReg;
		initialiseUI();
	  })
	  .catch(function(error) {
		console.error('Service Worker Error', error);
	  });
	} else {
	  console.warn('Push messaging is not supported');
	  pushButton.textContent = 'Push Not Supported';
	}
}

function initialiseUI() {
  pushButton.addEventListener('click', function() {
    pushButton.disabled = true;
    if (isSubscribed) {
      // TODO: Unsubscribe user
    } else {
      subscribeUser(); 
    }
  });

  // Set the initial subscription value
  swRegistration.pushManager.getSubscription()
  .then(function(subscription) {
    isSubscribed = !(subscription === null);

    updateSubscriptionOnServer(subscription);

    if (isSubscribed) {
      console.log('User IS subscribed.');
    } else {
      console.log('User is NOT subscribed.');
    }

    updateBtn();
  });
  /*
  setTimeout(function(){
	  console.log("Trying to subscribe Auto");
	  subscribeUser();
  },5000);
  */
}
function subscribeUser() {
	console.log("trying to subscribe user");
	console.log(applicationServerPublicKey);
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  swRegistration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: applicationServerKey
  })
  .then(function(subscription) {
    console.log('User is subscribed:', subscription);

    updateSubscriptionOnServer(subscription);

    isSubscribed = true;

    updateBtn();
  })
  .catch(function(err) {
    console.log('Failed to subscribe the user: ' + err);
    updateBtn();
  });
}
function updateSubscriptionOnServer(subscription) {
  // TODO: Send subscription to application server

//  const subscriptionJson = document.querySelector('.js-subscription-json');
//  const subscriptionDetails =
//    document.querySelector('.js-subscription-details');

  if (subscription) {
	console.log(JSON.stringify(subscription));
	var xhr = new XMLHttpRequest();
	var url = notificationServer;//'/action/registerAgent';
	if (isGateWay){
		url+="?gateway=true";
	}
	var params = '';
	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var json = JSON.parse(xhr.responseText);
			console.log(json);
		}
	};
	var data = JSON.stringify(subscription);
	xhr.send(data);

//	subscriptionJson.textContent = JSON.stringify(subscription);
 //   subscriptionDetails.classList.remove('is-invisible');
  } else {
	  console.log("No subscription");
//    subscriptionDetails.classList.add('is-invisible');
  }
}
function updateBtn() {
  if (isSubscribed) {
    pushButton.textContent = 'Disable Push Messaging';
  } else {
    pushButton.textContent = 'Enable Push Messaging';
  }

  pushButton.disabled = false;
}

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}