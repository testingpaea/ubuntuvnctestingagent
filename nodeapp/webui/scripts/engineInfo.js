var EngineInfo=class EngineInfo{
	constructor(engineStatus){
		var self=this;
		self.refresh(engineStatus);
		self.htmlStack=[""];
	}
	pushHtml(sHtml){
		var auxHtml="";
		if (typeof sHtml!=="undefined") auxHtml=sHtml;
		this.htmlStack.push(auxHtml);
	}
	popHtml(){
		return this.htmlStack.pop();
	}
	addHtml(sHtml){
		this.htmlStack[this.htmlStack.length-1]+=sHtml;
	}
	refresh(engineStatus){
		var self=this;
		self.gateway=engineStatus.gateway;
		self.registeredWebUIs=engineStatus.registeredWebUIs;
		self.status=engineStatus;
	}
	updateGatewayHtml(){
		var self=this;
		if (self.gateway!=webuiId){
			$("#gatewayStatus").show();
			if (self.gateway!==""){
				$("#lblGatewayStatus").text("Registered "+self.gateway);
			} else {
				$("#lblGatewayStatus").text("Unregistered");
			}
		} else {
			$("#gatewayStatus").hide();
		}
		if (self.gateway!==""){
			var sHtml="";
		}
	}
	updateAgentsSummaryHtml(){
		self=this;
		$("#AgentsTotal").text(self.nAgents);
		$("#AgentsLoaded").text(self.loadedAgents);
		$("#AgentsBusy").text(self.busyAgents);
	}
	updateWebUIsInfoHtml(){
		var self=this;
		var tableRows=$("#tblWebUIs > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		var webUIs=self.registeredWebUIs;
		webUIs.forEach(function (webuiStatus) {
			$("#tblWebUIs").append("<tr><td>"
				+"REGISTERED</td><td>" 
				+ (webuiStatus.id==self.gateway?"Gateway ":"")
				+ (webuiStatus.id==webuiId?"Actual":"Other")
				+" - "+webuiStatus.id + "</td>"
				+"</tr>");
		});
		webUIs=self.unregisteredWebUIs;
		if (typeof webUIs==="undefined") webUIs=[];
		webUIs.forEach(function (webuiStatus) {
			$("#tblWebUIs").append("<tr><td>"
				+"UNREGISTERED</td><td>" 
				+ (webuiStatus.id==self.gateway?"Gateway ":"")
				+ (webuiStatus.id==webuiId?"Actual":"Other")
				+" - "+webuiStatus.id + "</td>"
				+"</tr>");
		});
	}
	updateHtml(){
		var self=this;
		self.updateGatewayHtml();
		self.updateAgentsSummaryHtml();
		self.updateWebUIsInfoHtml();
	}
}
