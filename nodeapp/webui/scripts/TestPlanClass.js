var TestPlan=class TestPlan{
	constructor(oJsonTestPlan){
		var self=this;
		self.id="TestPlan_"+(new Date()).getTime();
		self.status="DESIGN";
		self.scenarios=[];
		self.agents=[];
		self.nGroupScenarios=1;
		self.mapAgents=new Map();
		self.agentSelector="";
		self.useSelector=true;
		self.deferredScenarios=false;
		self.managerType="CONTROLLER";
		self.executionId="";
		self.pendingScenarios=new Map();
		self.finishedScenarios=new Map();
		self.runningScenarios=new Map();
		if (typeof oJsonTestPlan!=="undefined"){
			self.id=oJsonTestPlan.id;
			self.status=oJsonTestPlan.status;
			self.scenarios=oJsonTestPlan.scenarios;
			self.nGroupScenarios=oJsonTestPlan.nGroupScenarios;
			self.agents=oJsonTestPlan.agents;
			self.agentSelector=oJsonTestPlan.agentSelector;
			self.useSelector=oJsonTestPlan.useSelector;
			self.managerType=oJsonTestPlan.managerType;
			self.executionId=oJsonTestPlan.executionId;
			self.agents.forEach(agentName=>{
				self.mapAgents.set(agentName,agentName);
			});
			if (typeof oJsonTestPlan.pendingScenarios!=="undefined"){
				oJsonTestPlan.pendingScenarios.forEach(scenarioInfo=>{
					self.pendingScenarios.set(scenarioInfo.executionId,scenarioInfo);
				});
			}
			if (typeof oJsonTestPlan.runningScenarios!=="undefined"){
				oJsonTestPlan.runningScenarios.forEach(scenarioInfo=>{
					self.runningScenarios.set(scenarioInfo.executionId,scenarioInfo);
				});
			}
			if (typeof oJsonTestPlan.finishedScenarios!=="undefined"){
				oJsonTestPlan.finishedScenarios.forEach(scenarioInfo=>{
					self.finishedScenarios.set(scenarioInfo.executionId,scenarioInfo);
				});
			}
		}
	}
	startRunning(){
		var self=this;
		self.status="RUNNING";
		self.pendingScenarios=new Map();
		self.scenarios.forEach(scenario=>{
			self.pendingScenarios.set(scenario.executionId,scenario);
		});
	}
	scenarioEnded(scenarioExecutionId,executionResult){
		var self=this;
		if (self.runningScenarios.has(scenarioExecutionId)){
			var scenario=self.runningScenarios.get(scenarioExecutionId);
			self.runningScenarios.delete(scenarioExecutionId);
			scenario.executionResult=executionResult;
			self.finishedScenarios.set(scenarioExecutionId,scenario);
			if ((self.runningScenarios.size+self.pendingScenarios.size)==0) {
				self.status="FINISHED";
			}
		} else {
			var bLocated=false;
			var scenario="";
			for (var i=0;(!bLocated) && (i<self.scenarios.length);i++){
				scenario=self.scenarios[i];
				if (scenario.executionId==scenarioExecutionId){
					bLocated=true;
				}
			}
			if (bLocated) {
				console.log("The execution id "+ scenarioExecutionId + " scenario "+scenario.key+ " was not in running status");  
			} else {
				console.log("There is not exists scenario with execution id "+ scenarioExecutionId);  
			}
		}
	}
	addScenario(scenarioId,params,environment,executionId){
		var self=this;
		var auxExecId=executionId;
		if (typeof auxExecId==="undefined"){
			auxExecId="";
		}
		var scenario={key:scenarioId,scenario:scenarioId,params:params,environment:environment,executionId:auxExecId,executionResult:""};
		self.scenarios.push(scenario);
		return scenario;
	}
	processExpressionFunctionSync(sBody,arrVarsValues){
		var self=this;
		var sFncFormula="";
		//debugger;
		var arrParams=[];
		arrVarsValues.forEach(oneVar=>{
			sFncFormula+="\n var "+oneVar.name+"=_arrRefs_["+arrParams.length+"];";
			arrParams.push(oneVar.value);
		});
		sFncFormula+="\n" + sBody;
		var fncFormula = "";
		try {
			fncFormula = new Function(`return function (_arrRefs_) { 
												`+sFncFormula+`
											  }`)();
		} catch (excpt){}
		if (fncFormula!==""){
			return fncFormula(arrParams);
		}
	}
	isAgentSelected(agent){
		var self=this;
		if (self.useSelector){
			var mapProps=new Map();
			if (typeof agent.mapProperties!=="undefined"){
				mapProps=agent.mapProperties;
			} else {
				var properties=agent.properties;
				properties.forEach(prop=>{
					mapProps.set(prop.name,prop.value);
				});
			}
			var arrParameters=[];
			arrParameters.push({name:"agentProperties",value:mapProps});
			var vResult=self.processExpressionFunctionSync(self.agentSelector,arrParameters);
			if (typeof vResult==="undefined") return true;
			if (typeof vResult.value==="undefined") return vResult;
			return vResult.value;
		} else if (self.agents.length==0){
			return true;
		} else {
			var bLocated=true;
			var selectedAgent="";
			for (var i=0;(!bLocated)&&(i<self.agents.length);i++){
				selectedAgent=self.agents[i];
				if (selectedAgent.name==agent.name){
					bLocated=true;
				}
			}
			if (bLocated){
				return true;
			}
			return false;
		}
	}
	nextScenario(agent){
		var self=this;
		if (self.pendingScenarios.size==0) return "";
		var nGroup=self.nGroupScenarios;
		var result="";
		if (nGroup>1){
			result=[];
		}
		for (const [key, scenario] of self.pendingScenarios) {
			if (agent.mapScenarios.has(scenario.key)&&(nGroup>0)){
				self.pendingScenarios.delete(key);
				scenario.planExecutionId=self.executionId;
				self.runningScenarios.set(key,scenario);
				if (self.nGroupScenarios==1){
					return scenario;
				} else {
					result.push(scenario);
					nGroup--;
				}
			}
		}
		if (result.length>0){
			return result;
		}
		return "";
	}

	getStatus(){
		var self=this;
		var arrPendingScenarios=[];
		self.pendingScenarios.forEach(scenarioInfo=>{
			arrPendingScenarios.push(scenarioInfo);
		});
		
		var arrRunningScenarios=[];
		self.runningScenarios.forEach(scenarioInfo=>{
			arrRunningScenarios.push(scenarioInfo);
		});
		
		var arrFinishedScenarios=[];
		self.finishedScenarios.forEach(scenarioInfo=>{
			arrFinishedScenarios.push(scenarioInfo);
		});
		var oResult={
				id:self.id
				,status:self.status
				,scenarios:self.scenarios
				,agents:self.agents
				,agentSelector:self.agentSelector
				,useSelector:self.useSelector
				,managerType:self.managerType
				,executionId:self.executionId
				,pendingScenarios:arrPendingScenarios
				,runningScenarios:arrRunningScenarios
				,finishedScenarios:arrFinishedScenarios
				};
		return oResult;
	}
}
