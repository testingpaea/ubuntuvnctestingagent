var multiPartMessages=new Map();

var PushEventHandler=class PushEventHandler{
	constructor(){
		var self=this;
		self.eventsRegistry=new Map();
		self.defaultHandler="";
		self.preprocessHandler="";
		self.multipartHandler="";
	}
	async raise(actionName,actionValues){
		var self=this;
		var bFullMsgLoaded=true;
		var auxActName=actionName;
		var auxActionValues=actionValues;
		if (self.preprocessHandler!==""){ // the preprocess handler manages the send of ACKs when the received messega needs it.
			debugger;
			var preprocessResult=await self.preprocessHandler(actionName,actionValues);
			bFullMsgLoaded=preprocessResult.bFullMsgLoaded;
			if (bFullMsgLoaded){
				var oFullMsg=preprocessResult.fullMsg;
				auxActName=oFullMsg.action;
				auxActionValues=oFullMsg.values;
			}
		} 
		if (bFullMsgLoaded){
			var bWithHandler=self.eventsRegistry.has(auxActName);
			log("Raised event:"+auxActName+" with handler:"+bWithHandler+" data:"+JSON.stringify(auxActionValues))
			if (bWithHandler){
				await self.eventsRegistry.get(auxActName)(auxActName,auxActionValues);
			} else if (self.defaultHandler!==""){
				log("Launching default Handler");
				await self.defaultHandler(auxActName,auxActionValues);
			}
		}
	}
	setDefaultHandler(fncDefaultHandler){
		var self=this;
		self.defaultHandler=fncDefaultHandler;
	}
	setPreprocessHandler(fncPreprocessHandler){
		var self=this;
		self.preprocessHandler=fncPreprocessHandler;
	}
	setMultipartHandler(fncMultipartHandler){
		var self=this;
		self.multipartHandler=fncMultipartHandler;
	}
	setHandler(actionName,fncEventManage){
		var self=this;
		self.eventsRegistry.set(actionName,fncEventManage);
	}
}
var pushEventHandler=new PushEventHandler();
