var AccInfo=class AccInfo extends EngineInfo{
	constructor(oAccStatus){
		super(oAccStatus);
		var self=this;
		self.interfaceType="ACC";
	}
	planScenario(scenarioId){
		var self=this;
		var scenario="";
		var arrScenarios=self.status.scenarios;
		var bLocated=false;
		for (var i=0;(i<arrScenarios.length)&&(!bLocated);i++){
			var actScenario=arrScenarios[i];
			if (actScenario.key==scenarioId){
				bLocated=true;
				scenario=actScenario;
			}
		}
		if (bLocated){
			var paramValues=[];
			var arrParams=scenario.params;
			var idInput="";
			var jqInput="";
			var paramValue="";
			if ((typeof arrParams!=="undefined")&&(Array.isArray(arrParams))){
				arrParams.forEach(param=>{
					idInput=scenarioId+"_"+param.name;
					jqInput=$("#"+idInput);
					paramValue=jqInput.val();
					paramValues.push({name:param.name,value:paramValue});
				});
			}
			idInput="selEnv_"+scenarioId;
			jqInput=$("#"+idInput);
			var theEnvironment=jqInput.val();
			var thePlan=testPlans.actualTestPlan();
			thePlan.addScenario(scenarioId,paramValues,theEnvironment);
		}
		self.updateHtml();
	}
	refresh(oAccStatus){
		super.refresh(oAccStatus);
		var self=this;
		self.nAgents=oAccStatus.nAgents;
		self.loadedAgents=oAccStatus.loadedAgents;
		self.busyAgents=oAccStatus.busyAgents;
		if (typeof oAccStatus.testPlans!=="undefined"){
			oAccStatus.testPlans.forEach(testPlan=>{
				var oldPlan=testPlans.get(testPlan.id);
				if (typeof oldPlan==="undefined"){
					testPlans.addFromJson(testPlan);
				}
			});
		}
	}
	getAgentSelectorHtml(testPlan){
		var self=this;
		var selName=testPlan.id+"_"+"selAgent";
		var sOptions="";
		self.status.agents.forEach(function(agent){
			var sSelected="";
			if (agent.status!="UNLOADED"){
				if (testPlan.mapAgents.has(agent.name)){
					sSelected="SELECTED";
				}
				sOptions+='<option value="'+agent.name+'" '+sSelected+'>'+agent.name+'</option>';
			}
		});
		var sHtml='<select id="'+selName+'" multiple>';
		sHtml+=sOptions;
		sHtml+='</select>';
		return sHtml;
	}
	addTestPlanRowHtml(testPlan){
		var self=this;
		var sScenarios="";
		var indScenario=0;
		testPlan.scenarios.forEach(scenario=>{
			sScenarios+=(sScenarios===""?"":"<br>");
			sScenarios+='<button onclick="adjustScenarioCall(\''+testPlan.id+'\','+indScenario+')">Adjust</button>';
			sScenarios+=scenario.key+" "+ scenario.environment+ (scenario.params.length==0?"":" " + JSON.stringify(scenario.params));
			indScenario++;
		});
		var sActions="";
		sActions+=`Manage at Level <select id="selManager_`+testPlan.id+`">
					  `+(self.interfaceType==="ACC"?"":'<option value="CONTROLLER">Controller</option>')+`
					  <option value="ACCS">ACCs</option>
					</select>`;
		sActions+=`<br>Group Test by <select id="selGroupScenarios_`+testPlan.id+`">
					  <option SELECTED value="1">1 Scenario/Agent</option>
					  <option value="2">2 Scenario/Agent</option>
					  <option value="4">4 Scenario/Agent</option>
					  <option value="8">8 Scenario/Agent</option>
					  <option value="16">16 Scenario/Agent</option>
					</select>`;				
		sActions+='<br><button onclick="executePlan(\''+testPlan.id+'\')">Execute Now</button>';
		sActions+='<br><button onclick="schedulePlan(\''+testPlan.id+'\')">Schedule</button>';
		sActions+='<br><button onclick="savePlanToFile(\''+testPlan.id+'\')">Save to File</button>';
		if ((self.interfaceType!=="ACC")||(bLinkedToController)){
			sActions+='<br><button onclick="saveTestPlanInController(\''+testPlan.id+'\')">Save in Controller Level</button>';
		}
		var sOptions="";
		sOptions+='<input type="checkbox" id="'+testPlan.id+'_chkSequential" value="sequential">Sequential Execution';
		sOptions+=self.getAgentSelectorHtml(testPlan);
		sOptions+='<hr>';
		sOptions+='<input type="checkbox" id="'+testPlan.id+'_chkSelector" CHECKED>Selector <textarea id="inputAgentSelector_'+testPlan.id+'" >'+testPlan.agentSelector+'</textarea>';
		sOptions+='<br><button onclick="checkTestPlanAgentSelector(\''+testPlan.id+'\')">Test Selector</button>'
		sOptions+='<button onclick="updateTestPlanOptions(\''+testPlan.id+'\')">Update</button>'
		$("#tblTestPlans").append("<tr><td>"
			+testPlan.status+"</td><td>" 
			+testPlan.id+"</td><td>" 
			+sScenarios+'</td><td>' 
			+sOptions+"</td><td>" 
			+sActions+"</td>" 
			+"</tr>");
		
	}
	addTestPlanExecutionRowHtml(execution,theStatus,theType){
		var self=this;
		var sStatus=theStatus+" - " + theType+" - ";
		if ((execution.pendingScenarios.length+execution.runningScenarios.length)==0){
			sStatus+="FINISHED "+execution.finishedScenarios.length + " SCENARIOS";
		} else {
			sStatus+=" Running:"+execution.runningScenarios.length
						+" / Pending:"+execution.pendingScenarios.length
						+" / Finished:"+execution.finishedScenarios.length;
		}
//		var arrResults=[];
		var sResults="";
		execution.finishedScenarios.forEach(scenario=>{
			var oResult=scenario.executionResult;
			while (Array.isArray(oResult)&&(oResult.length==1)){
				oResult=oResult[0];
			}

//			arrResults.push(scenario.executionResult);
			sResults+=(sResults!==""?"<hr>":"")+scenario.environment+" - " + scenario.key;
			sResults+='<br>'+oResult.state+" " + oResult.time +"s";
			sResults+='<br>Executed in ACC '+oResult.executedInACC+" Agent " + oResult.executedByAgent +" ("+oResult.executedByAgentNumber +")";
			sResults+='<br>'+oResult.name;
			sResults+='<br><button onclick="showTestPlanResults(\''+execution.executionId+'\',\''+scenario.executionId+'\')">Result details</button>';
		});
		//JSON.stringify(arrResults,null,2)

		$("#tblTestPlanExecutions").append("<tr><td>"
			+execution.id+ "<br>"+execution.executionId+"</td><td>" 
			+sStatus+"</td><td>" 
			+sResults+"</td>" 
			+"</tr>");
	}
	updateTestPlansListHtml(){
		var self=this;
		var tableRows=$("#tblTestPlans > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		if (testPlans.count()>0){
			testPlans.getAll().forEach(testPlan=>{
				self.addTestPlanRowHtml(testPlan);
			});
		}
	}
	getTestPlanResults(planExecutionId,scenarioExecutionId){
		var tpExecutions=self.status.executions;
		var theStatus=["running","finished"];
		var types=["normal","priority","scheduled"];
		for (var i=0;i<theStatus.length;i++){
			var tmpStatus=theStatus[i];
			for (var j=0;j<types.length;j++){
				var tmpType=types[j];
				var executions=tpExecutions[tmpType][tmpStatus];
				for (var k=0;k<executions.length;k++){
					var execution=executions[k];
					if (execution.executionId==planExecutionId){
						for (var m=0;m<execution.finishedScenarios.length;m++){
							var scenario=execution.finishedScenarios[m];
							if (scenario.executionId==scenarioExecutionId){
								var oResult=scenario.executionResult;
								while (Array.isArray(oResult)&&(oResult.length==1)){
									oResult=oResult[0];
								}
								return oResult;
							}
						}
					}
				}
			}
		}
		return "";
	}
	updateTestPlanResultsHtml(){
		var self=this;
		var tableRows=$("#tblTestPlanExecutions > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		var tpExecutions=self.status.executions;
		var theStatus=["running","finished"];
		var types=["normal","priority","scheduled"];
		theStatus.forEach(tmpStatus=>{
			types.forEach(tmpType=>{
				tpExecutions[tmpType][tmpStatus].forEach(execution=>{
					self.addTestPlanExecutionRowHtml(execution,tmpStatus,tmpType);
				});
			});
		});
		types.forEach
		if (testPlans.count()>0){
			testPlans.getAll().forEach(testPlan=>{
			});
		}
	}
	addScenarioRowHtml(scenario){
		var self=this;
		var sParams="";
		sParams+=`Environment <select id="selEnv_`+scenario.key+`">
					  <option value="INTEGRA">INTEGRA</option>
					  <option value="DES">DES</option>
					  <option value="PRE">PRE</option>
					  <option value="PRO">PRO</option>
					</select>`;
		if ((typeof scenario.params!=="undefined")&&(scenario.params!=="")){
			scenario.params.forEach(param=>{
				sParams+=(sParams!==""?"<br>":"");
				sParams+=param.name+'<input id="'+scenario.key+"_"+param.name+'" type="text" value="'+param.defaultValue+'"/>';
				sParams+="<br><i>"+param.description+"</i>";
			});
		}
		var sActions="";
		sActions+='<button onclick="executeTest(\''+scenario.key+'\')">Execute</button>';
		sActions+='<br><button onclick="addToPlan(\''+scenario.key+'\')">Add to Plan</button>';
		$("#tblScenarios").append("<tr><td>"
			+scenario.key+"</td><td>" 
			+scenario.name+'</td><td>' 
			+scenario.agents.length+"</td><td>" 
			+"</td><td>" 
			+sParams+ "</td><td>" 
			+sActions+"</td>" 
			+"</tr>");
	}
	updateScenarioListHtml(){
		var self=this;
		var tableRows=$("#tblScenarios > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		if (typeof self.status.scenarios!=="undefined"){
			self.status.scenarios.forEach(scenario=>{
				self.addScenarioRowHtml(scenario);
			});
		}
	}
	getAgentScenarioListHtml(agent){
		var scenarios=agent.scenarios;
		var sOptions="";
		var sHtml="";
		if ((typeof scenarios!=="undefined")&&(scenarios.length>0)) {
			var selScenariosName="selScenarios_"+agent.name;
			debugger;
			function compare( a, b ) {
				  if ( a.key < b.key ){
				    return -1;
				  }
				  if ( a.key > b.key ){
				    return 1;
				  }
				  return 0;
			}
			scenarios.sort( compare );
			for (const option of scenarios){
				sOptions+='<option value="'+option.key+'">'+option.key+"-"+option.name+'</option>';
			}
			var sHtml="";
			sHtml+='<button onclick="runScenarios(\''+agent.name+'\',\''+selScenariosName+'\')">run</button>';
			sHtml+='<select id="'+selScenariosName+'" multiple  size="10">';
			sHtml+=sOptions;
			sHtml+='</select>';
			sHtml+='<button onclick="runScenarios(\''+agent.name+'\',\''+selScenariosName+'\')">run</button>';
		}
		return sHtml;
	}
	getAgentProcessResultsHtml(agent){
		var sHtml="";
		var i=0;
		agent.processedScenarios.forEach(results=>{
			var j=0;
			if (Array.isArray(results)){
				results.forEach(result=>{
					sHtml+="<hr> Name:"+result.name+" Result:"+result.state+" Time:"+result.time;
					sHtml+='<button onclick="showResultDetail(\''+agent.name+'\','+i+','+j+')">details</button>';
					j++;
				});
			} else {
				var result=results;
				sHtml+="<hr> Name:"+result.name+" Result:"+result.state+" Time:"+result.time;
				sHtml+='<button onclick="showResultDetail(\''+agent.name+'\','+i+','+j+')">details</button>';
				j++;
			}
			i++;
		});
		return sHtml;
	}		
	getAgentFeedbackHtml(agent){
		var sResult="";
		oResult.takingScreenshots=agent.bTakingScreenshot;
		oResult.screenshotFrequency=agent.screenshotFrequency;
		if ((agent.status!=="UNLOADED")&&
			(agent.status!=="LOADING")){
			sResult+='<button id="btnOpenWindowScreenshotsAgent_'+agent.name+'" onclick="openScreenshotsWindow(\''+agent.name+'\')" >Open Screenshot Window</button>';
			sResult+='<button id="btnTraceExecutionTreeAgent_'+agent.name+'" onclick="traceExecutionTree(\''+agent.name+'\')" >Trace Execution Tree</button>';
			sResult+='<hr>';
			sResult+='Selenium Interface Name<input id="inputInterfaceNameAgent_'+agent.name+'" >';
			sResult+='<br><button id="btnSaveInterfacesAgent_'+agent.name+'" onclick="saveInterface(\''+agent.name+'\',\'inputInterfaceNameAgent_'+agent.name+'\')" >Save Interface</button>';
		}
		if (agent.takingScreenshots){
			/*sResult="Taking Screenshots every "+oResult.screenshotFrequency+" milliseconds";
			sResult+='<input id="freqScreenshotAgent_'+agent.name+'" ><button id="btnChangeFreqScreenshotsAgent_'+agent.name+'" onclick="startScreenShots(\''+agent.name+'\',\'freqScreenshotAgent_'+agent.name+'\')" >Change Freq</button>';
			sResult+='<button id="btnStopScreenshotsAgent_'+agent.name+'" onclick="stopScreenShots(\''+agent.name+'\')" >Stop</button>';
			*/
/*		} else {
			sResult="Not Taking Screenshots";
			sResult+='<input id="freqScreenshotAgent_'+agent.name+'" value="5000"><button id="btnStartScreenshotsAgent_'+agent.name+'" onclick="startScreenShots(\''+agent.name+'\',\'freqScreenshotAgent_'+agent.name+'\')" >Start</button>';
*/		}
		if (agent.feedback===""){
			//no feedback info
		} else if (agent.feedback.type=="message"){
			sResult=agent.feedback.text;
		} else if (agent.feedback.type=="inputbox"){
			sResult+=(sResult!==""?"<hr>":"");
			sResult+=agent.feedback.text;
			var sType="";
			if (agent.feedback.password){
				sType='type="password"';
			}
			sResult+='<input '+sType+' id="inputAgent_'+agent.name+'" ><button id="btnSendInputToAgent_'+agent.name+'" onclick="sendFeedback(\''+agent.name+'\',\'inputAgent_'+agent.name+'\')" >send</button>';
		} 	
		return sResult;
	}
	getAgentPropertiesHtml(agent){
		var self=this;
		debugger;
		self.pushHtml();
		var bFirst=true;
		var bSeleniumExists=false;
		if (typeof agent.properties!=="undefined"){
			agent.properties.forEach(property=>{
				if (!bFirst) self.addHtml("<br>");
				bFirst=false;
				self.addHtml(property.name+":"+property.value);
				self.addHtml('<button id="btnRemoveAgentProperty_'+agent.name+'" onclick="sendDeleteProperty(\''+agent.name+'\',\''+property.name+'\')" >del</button>');
				if (property.name=="seleniumhub"){
					bSeleniumExists=true;
				}
			});
		}
		if (bSeleniumExists){
			self.addHtml('<br><label><input type="checkbox" checked id="'+"cbSeleniumHub"+"_"+agent.name+'" value=cbSeleniumHub_"'+agent.name+'" onclick="addSeleniumHubProperty(\''+agent.name+'\')" > Selenium Hub 127.0.0.1:4444</label>');
		}else {
			self.addHtml('<br><label><input type="checkbox" id="'+"cbSeleniumHub"+"_"+agent.name+'" value=cbSeleniumHub_"'+agent.name+'" onclick="addSeleniumHubProperty(\''+agent.name+'\')" > Selenium Hub 127.0.0.1:4444</label>');
		}
		self.addHtml('<hr><b>New Property</b>');
		self.addHtml('<br>Name:<input id="inputAgentNewPropertyName_'+agent.name+'" >');
		self.addHtml('<br>Value:<input id="inputAgentNewPropertyValue_'+agent.name+'" >');
		self.addHtml('<br><button id="btnSendNewPropertyToAgent_'+agent.name+'" onclick="sendNewProperty(\''+agent.name+'\',\'inputAgentNewPropertyName_'+agent.name+'\',\'inputAgentNewPropertyValue_'+agent.name+'\')" >add</button>');
		return self.popHtml();
	}
	addAgentRowHtml(agent){
		var self=this;
		$("#tblAgents").append("<tr><td>"
			+agent.name+"</td><td>" 
			+agent.status+'<button id="'+"btnLoadAgent"+"_"+agent.name+'" '
			+(agent.status=="UNLOADED"?
				' onclick="loadAgent(\''+agent.name+'\')" type="button">Load'
				: ' onclick="reloadAgent(\''+agent.name+'\')" type="button">Reload')
			+'</button><br><b>Properties</b><br>'
			+self.getAgentPropertiesHtml(agent)+"<br>"
			+'</td><td>' 
			+agent.busy+"</td><td>" 
			+agent.dynamicAssign+"</td><td>" 
			+self.getAgentScenarioListHtml(agent)+"</td><td>" 
			+(agent.runningScenario!==""?agent.runningScenario.id+ " - " + agent.runningScenario.status:"")+"</td><td>" 
			+self.getAgentProcessResultsHtml(agent)+"</td><td>" 
			+self.getAgentFeedbackHtml(agent)+"</td>" 
			+"</tr>");
	}
	updateAgentListHtml(){
		var self=this;
		var tableRows=$("#tblAgents > tbody > tr");
		var numRows=tableRows.length;
		if (numRows>0){
			tableRows.remove();
		}
		self.status.agents.forEach(function(agent){
			self.addAgentRowHtml(agent);
		});
	}
	getAgentDetail(agentName){
		var self=this;
		var oResult="";
		self.status.agents.forEach(function(agent){
			if (agent.name==agentName){
				oResult=agent;
			}
		});
		return oResult;
	}
	updateHtml(){
		super.updateHtml();
		var self=this;
		self.updateAgentListHtml();
		self.updateScenarioListHtml();
		self.updateTestPlansListHtml();
		self.updateTestPlanResultsHtml();
	}
}
