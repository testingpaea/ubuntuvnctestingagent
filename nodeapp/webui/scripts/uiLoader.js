//$("#agentDetails").append('<span id="testDynUi">testing</span>');
/*$.getScript(accServerUrl+"/scripts/engineInfo.js");
$.getScript(accServerUrl+"/scripts/accInfo.js");
$.getScript(accServerUrl+"/scripts/controllerInfo.js");
$.getScript(accServerUrl+"/scripts/uiTestPlan.js");
*/
var arrScripts=[
	accServerUrl+"/scripts/engineInfo.js",
	accServerUrl+"/scripts/accInfo.js",
	accServerUrl+"/scripts/controllerInfo.js",
	accServerUrl+"/scripts/TestPlanClass.js",
	accServerUrl+"/scripts/uiTestPlanManager.js"
];
arrScripts.forEach(script=>{
	$.ajax({
		async: false,
		url: script,
		dataType: "script"
	});
});
var generalInfo="";
var bRegistered=false;
var bLinkedToController=false;
async function postToAcc(action,data){
	debugger;
	var url = accServerUrl+action;
	var postData=data;
	if (typeof data==="undefined"){
		postData={};
	}
	postData.webuiId=webuiId;
	var sData=JSON.stringify(postData);
	var response ="";
	if (sData.length>100000) {
		var formData  = new FormData();
		formData.append("jsonContent", sData);
		response = await fetch(url, {
			  method: 'post',
	/*		  headers: {
	//			'Content-type': 'application/json'
				'Content-type': 'multipart/form-data'
			  },
	*/		  body: formData
			});
	} else {
		response = await fetch(url, {
			  method: 'post',
			  headers: {
				'Content-type': 'application/json'
			  },
			  body: sData
			});
	}
	let oResult = await response.json();
    return oResult;
	
}


async function updateSystem(systemName){
	debugger;
	try {
		await postToAcc('/actions/updateSystem',{system:systemName});
	} catch (err){
		console.log("Error Processing updateSystem call:"+ err.toString());
	}
	const delay = ms => new Promise(res => setTimeout(res, ms));
  	if (systemName=="all"){
		await delay(20000);
	  	console.log("Waited 20s for all system reload");
	 }
	await delay(10000);
  	console.log("Waited 10s");
	location.reload(true);
}


async function connectToController(){
	debugger;
	var controllerLink=$("#inputControllerLink").val();
	var oResult=await postToAcc('/actions/connectToController',{controllerLink:controllerLink});
	if (oResult.result=="OK"){
		log("Trying to connect to Controller");
		var ctrlrDetail=$("#controllerDetail");
		ctrlrDetail.html("Controller Loaded");
		ctrlrDetail.show();
		bLinkedToController=true;
		//$("#controllerInfo").show();
	} else {
		log("Error connecting to controller");
	}
}
function addToPlan(scenarioId){
	generalInfo.planScenario(scenarioId);
}

function newTestPlan(){
	var thePlan=testPlans.newTestPlan();
	generalInfo.updateHtml();
}

async function executePlan(planId){
	debugger;
	var testPlan=testPlans.get(planId);
	var bUseSelector=$("#"+planId+"_chkSelector").prop('checked');
	var selectorText="";
	var arrAgents=[];
	if (bUseSelector){
		selectorText=$("#inputAgentSelector_"+planId).val();
	} else {
		$('#'+planId+'_selAgent option:selected').each(function() {
			arrAgents.push($(this).val())
		});	
	}
	var sPlanManager=$("#selManager_"+planId +" option:selected").val();
	var sGroupScenarios=$("#selGroupScenarios_"+planId +" option:selected").val();
	var planExecution=testPlan.getStatus();
	planExecution.nGroupScenarios=parseInt(sGroupScenarios);
	planExecution.agentSelector=selectorText;
	planExecution.useSelector=bUseSelector;
	planExecution.agents=arrAgents;
	planExecution.managerType=sPlanManager;
	console.log("Execute Plan "+planId+" "+JSON.stringify(planExecution,undefined,3));
	var oResult=await postToAcc('/actions/executeTestPlan',planExecution);
}
async function executeTest(scenarioId){
	var thePlan=testPlans.newTestPlan();
	generalInfo.planScenario(scenarioId);
	executePlan(thePlan.id);
	
	
}

function checkTestPlanAgentSelector(planId){
	var testPlan=testPlans.get(planId);
	debugger;
	var selText=$("#inputAgentSelector_"+planId).val();
	var useSelector=$("#"+planId+"_chkSelector").is(":checked");
	testPlan.useSelector=useSelector;
	testPlan.agentSelector=selText;
	var mapSelected=new Map();
	generalInfo.status.agents.forEach(function(agent){
		var vResult=testPlan.isAgentSelected(agent);
		if (vResult){
			mapSelected.set(agent.name,agent);
		}
	});
	$('#'+planId+'_selAgent option').each(function() {
		var sAgentId=$(this).val();
		if (mapSelected.has(sAgentId)){
			$(this).prop('selected', true);
		} else {
			$(this).prop('selected', false);
		}
	});	
	
}

function updateTestPlanOptions(planId){
	debugger;
	var testPlan=testPlans.get(planId);
	var selText=$("#inputAgentSelector_"+planId).val();
	testPlan.agentSelector=selText;
	var optionList = [];
	$('#'+planId+'_selAgent option').each(function() {
		optionList.push($(this).val())
	});	
	testPlan.agents=optionList;
	
}
async function saveTestPlanInController(planId){
	var testPlan=testPlans.get(planId);
	var oResult=await postToAcc('/actions/saveTestPlan',testPlan);
	if (oResult.result=="OK"){
		log("Test plan " +planId +  " saved");
	} else {
		log("Error saving test plan "+planId);
	}
}
async function sendFeedback(agentName,inputName){
	var oValue=$('#'+inputName).val();
	var oResult=await postToAcc('/actions/feedbackToAgent',{agentName:agentName,value:oValue});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" feedback!");
	} else {
		log("ERROR on Agent "+agentName+" feedback!");
	}
}

async function addSeleniumHubProperty(agentName){
	var bChecked=$('#'+"cbSeleniumHub"+"_"+agentName).attr('checked');
	if (!bChecked){
		sendNewProperty(agentName,"seleniumhub","127.0.0.1:4444");
	} else {
		sendDeleteProperty(agentName,"seleniumhub");
	}
}

async function sendNewProperty(agentName,inputPropertyName,inputPropertyValue){
	var propName=$('#'+inputPropertyName).val();
	if (typeof propName==="undefined") propName=inputPropertyName;
	var propValue=$('#'+inputPropertyValue).val();
	if (typeof propValue==="undefined") propValue=inputPropertyValue;
	var oResult=await postToAcc('/actions/addPropertyToAgent',{agentName:agentName,property:{name:propName,value:propValue}});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" property added!");
	} else {
		log("ERROR on Agent "+agentName+" property add!");
	}

	
}
async function sendDeleteProperty(agentName,propertyName){
	var oResult=await postToAcc('/actions/delAgentProperty',{agentName:agentName,propertyName:propertyName});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" property deleted!");
	} else {
		log("ERROR on Agent "+agentName+" property delete!");
	}
}


async function loadAgent(agentName){
	var oResult=await postToAcc('/actions/loadAgent',{agentName:agentName});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" Loading!");
	} else {
		log("ERROR on Agent "+agentName+" Loading!");
	}
}

async function reloadAgent(agentName){
	var oResult=await postToAcc('/actions/reloadAgent',{agentName:agentName});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" Reloading!");
	} else {
		log("ERROR on Agent "+agentName+" Reloading!");
	}
}		
async function runScenarios(agentName,selName){
	debugger;
	var selScenarios=[];
	$.each($("#"+selName+" option:selected"), function(){            
		selScenarios.push($(this).val());
	});
	selScenarios.forEach(function(elem){
		log("Selected:"+elem);
	});
	var oResult=await postToAcc('/actions/runScenarios',{agentName:agentName,scenarios:selScenarios,clearResults:true});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" running "+selScenarios.length+" scenarios!");
	} else {
		log("ERROR on Agent "+agentName+" start running!");
	}
}
async function traceExecutionTree(agentName){
	debugger;
	var oResult=await postToAcc('/actions/traceExecutionTree',{agentName:agentName});
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		log("Agent "+agentName+" trace execution tree");
	} else {
		log("ERROR on Agent "+agentName+" trace execution tree");
	}
}

function openScreenshotsWindow(agentName){
	debugger;
	var otherWindow= window.open("/html/screenshots/screenshot.html?agentName="+agentName , '_blank');
}
async function saveInterface(agentName,fldInterfaceName){
	debugger;
	var itfName=$('#'+fldInterfaceName).val();
	var oResult=await postToAcc('/actions/saveSeleniumInterface',{agentName:agentName,interfaceName:itfName});
	console.log(JSON.stringify(oResult));
	alert(agentName+" <--SAVED INTERFACE --> "+itfName);
}

async function showJsonInNewTab(oJson){
	debugger;		
	var html = `
	<html>
		<head>
			<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
		</head>
		<body onload="PR.prettyPrint()">
			<pre class="prettyprint">`+
				 JSON.stringify(oJson,null,3)+`
			</pre>
		</body>
	</html>`;
	var blobResult = new Blob([html], {type : "text/html"});
	var blobUrl = window.URL.createObjectURL(blobResult);
	var otherWindow= window.open(blobUrl , '_blank');
}

async function showResultDetail(agentName,iProcessedScenario,iResult){
	debugger;
	var oAgent=generalInfo.getAgentDetail(agentName);
	var arrResults=oAgent.processedScenarios[iProcessedScenario];
	var oResult=arrResults[iResult];
	showJsonInNewTab(oResult);
}
async function showTestPlanResults(planExecutionId,scenarioExecutionId){
	var oResult=generalInfo.getTestPlanResults(planExecutionId,scenarioExecutionId);
	if (oResult!=="") {
		showJsonInNewTab(oResult);
	} else {
		alert("There is not result yet");
	}
}
async function startScreenShots(agentName,freqFieldName){
	debugger;
	var sFreq=$('#'+freqFieldName).val();
	var nFreq=parseInt(sFreq);
	var oResult=await postToAcc('/actions/changeScreenshot',{agentName:agentName,action:"start",freq:nFreq});
	console.log("Start result:"+JSON.stringify(oResult));
}
async function stopScreenShots(agentName){
	debugger;
	var oResult=await postToAcc('/actions/changeScreenshot',{agentName:agentName,action:"stop"});
	console.log("Stop result:"+JSON.stringify(oResult));
}
async function openScreenShotsWindow(agentName){
	debugger;
}

pushEventHandler.setDefaultHandler(async function(actionName,actionValues){
	var self=this;
	var oResult=await postToAcc("/actions/msgReceived",{action:actionName,values:actionValues});
	return oResult;
});

pushEventHandler.setPreprocessHandler(async function(actionName,actionValues){
	var self=this;
	var oResult=await postToAcc("/actions/preprocessMessage",{action:actionName,values:actionValues});
	return oResult;
});


pushEventHandler.setMultipartHandler(async function(actionName,actionValues){
	var self=this;
	var oResult=await postToAcc("/actions/multipartMessageProcess",{action:actionName,values:actionValues});
	return oResult;
});


pushEventHandler.setHandler("ACC-UPDATEUI",async function(actionName,actionValues){
	log("UPDATEUI");
	var oResult=await postToAcc('/actions/getStatus');
	log("UPDATEUI result:"+JSON.stringify(oResult));
	if (oResult.result=="OK"){
		var baseInfo=oResult.status;
		var oInfo;
		if (nodeConfig.isACC){
			oInfo=new AccInfo(baseInfo);
		} else {
			oInfo=new ControllerInfo(baseInfo);
		}
		generalInfo=oInfo;
		generalInfo.updateHtml();
	}
});

pushEventHandler.setHandler("SW-COMMSWORKING",function(actionName,actionValues){
	if (!bRegistered){
		$("#lblWebuiStatus").text("Needs Register");
		$("#btnRegister").show();
		$("#btnRegister").click(function(){
			log("Registering "+(isGateway?"gateway":"webui")+" with id "+webuiId+" in ACC");
			updateSubscriptionOnServer(theSubscription);
		});
		updateSubscriptionOnServer(theSubscription);
	} else {
		log("Registered received commsworking");
		//processAck(actionValues);
	}
	
});


pushEventHandler.setHandler("ACC-REGISTERED",function(actionName,actionValues){
	var msgContent=actionValues.msgContent;
	if (typeof msgContent.systemDetails!=="undefined"){
		nodeConfig=msgContent.systemDetails;
	}
	var regWebuiId=msgContent.webuiId;
	log("Registering the "+(isGateway?"gateway":"webui")+" with uuid:"+regWebuiId);
	if (regWebuiId==webuiId){
		$("#lblWebuiStatus").text("Registered "+webuiId);
		if (msgContent.gatewayId!=webuiId){
			$("#gatewayStatus").show();
			if ((typeof msgContent.gatewayId!=="undefined")&&(msgContent.gatewayId!=="")){
				$("#lblGatewayStatus").text("Registered "+msgContent.gatewayId);
			} else {
				$("#lblGatewayStatus").text("Unregistered");
			}
		} else {
			$("#gatewayStatus").hide();
		}		
	}
	if ((typeof nodeConfig.gatewayLink!=="undefined")&&(nodeConfig.gatewayLink!=="")){
		$("#gatewayInfo").append('Gateway Message Link <span id="MessageLink">"'
					+nodeConfig.gatewayLink.substring(0,16)+"..."
					+'"</span><button onclick="copyToClipboard(\''+nodeConfig.gatewayLink+'\')">Copy to Clipboard</button>');
	} else {
		$("#gatewayInfo").html("");
	}
//	processAck(actionValues);   //the webui preprocess sends all of acks
	bRegistered=true;
	$("#btnRegister").hide();
	$("#btnUnregister").show();
	$( "#agentDetails" ).load( accServerUrl+'/html/indexBody.html',function(){
			if (nodeConfig.isACC){
				$("#ConnectToController").show();
			} else {
				$("#ConnectToController").hide();
			}
			$("#btnRefresh").click(async function(){
				await pushEventHandler.raise("ACC-UPDATEUI",{});
			});
		});
});


async function updateSubscriptionOnServer(subscription) {
  if (subscription) {
	console.log(JSON.stringify(subscription));
	var data = {subscription:subscription,gateway:isGateway,force:true};
	oResult=await postToAcc('/actions/registerWebUi',data);
	console.log(JSON.stringify(oResult));
	if (oResult.result=="OK"){
		if (isGateway!=oResult.gateway){
			log("The webui is gateway:"+isGateway+" but the registering process says it´s "+oResult.gateway);
		} else {
			webuiId=oResult.webuiId;
		}
	} else {
			log("The webui is gateway:"+isGateway+" but the ACC has another gateway registered");
	}
  } else {
	  console.log("No subscription");
  }
}

