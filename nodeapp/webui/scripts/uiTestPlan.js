var uiTestPlans=class uiTestPlans{
	constructor(){
		var self=this;
		self.testPlans=[];
	}
	newTestPlan(){
		var self=this;
		var tpNew=new uiTestPlan();
		self.testPlans.push(tpNew);
		return tpNew;
	}
	actualTestPlan(){
		var self=this;
		if (self.testPlans.length<=0){
			return self.newTestPlan();
		} else {
			return self.testPlans[self.testPlans.length-1];
		}
	}
	addScenario(key,params,environment){
		var self=this;
		var actPlan=self.actualTestPlan();
		actPlan.addScenario(key,params,environment);
	}
	addFromJson(oJsonTestPlan){
		var self=this;
		var newPlan=self.newTestPlan();
		newPlan.id=oJsonTestPlan.id;
		newPlan.status=oJsonTestPlan.status;
		newPlan.scenarios=oJsonTestPlan.scenarios;
		newPlan.agentSelector=oJsonTestPlan.agentSelector;
		oJsonTestPlan.agents.forEach(agentId=>{
			newPlan.agents.push(agentId);
			newPlan.mapAgents.set(agentId,agentId);
		});
	}
	getAll(){
		return this.testPlans;
	}
	exists(planId){
		return (typeof this.get(planId)!=="undefined");
	}
	get(planId){
		var self=this;
		var plans=self.testPlans;
		for (var i=0;(i<plans.length);i++){
			if (plans[i].id==planId){
				return plans[i];
			}
		}
	}
	count(){
		return this.testPlans.length;
	}
}
var testPlans=new uiTestPlans();