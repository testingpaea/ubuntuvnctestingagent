var uiTestPlans=class uiTestPlans{
	constructor(){
		var self=this;
		self.testPlans=[];
	}
	newTestPlan(oJsonTestPlan){
		var self=this;
		var tpNew=new TestPlan(oJsonTestPlan);
		self.testPlans.push(tpNew);
		return tpNew;
	}
	actualTestPlan(){
		var self=this;
		if (self.testPlans.length<=0){
			return self.newTestPlan();
		} else {
			return self.testPlans[self.testPlans.length-1];
		}
	}
	addScenario(key,params,environment){
		var self=this;
		var actPlan=self.actualTestPlan();
		actPlan.addScenario(key,params,environment);
	}
	addFromJson(oJsonTestPlan){
		var self=this;
		var newPlan=self.newTestPlan(oJsonTestPlan);
	}
	getAll(){
		return this.testPlans;
	}
	exists(planId){
		return (typeof this.get(planId)!=="undefined");
	}
	get(planId){
		var self=this;
		var plans=self.testPlans;
		for (var i=0;(i<plans.length);i++){
			if (plans[i].id==planId){
				return plans[i];
			}
		}
	}
	count(){
		return this.testPlans.length;
	}
}
var testPlans=new uiTestPlans();