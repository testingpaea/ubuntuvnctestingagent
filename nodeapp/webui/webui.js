const http = require('http');
const https = require('https');
const url = require('url');
const fs = require('fs');
const path = require('path');
const atob = require('atob');
const webpush = require('web-push');
const getRandomValues = require('get-random-values');
const PushMessager=require("./pushModes/webpushPush");
const agentManager= require('../agentManager/agentManager');
const screenshotManager=require("../screenshotManager/screenshotManager");
const Scenario = require('../agentManager/scenario')
const messageHandler=require('../messages/messageHandler');
const TestPlan= require('../agentManager/testPlan');
const genIdentification = require('../helpers/identification.lib');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const zlib = require('zlib');
const multer  = require('multer');
const child_process= require('child_process');
const port = 8080;

const { spawn } = require('child_process')
var theGatewayBrowser="";
//const cookieParser = require('cookie-parser');



// VAPID keys should only be generated only once.
//var vapidKeys = webpush.generateVAPIDKeys();

//var vapidKeys={"publicKey":"BApBFXP7dcM4h0ysrwW6KSWD1vrBTTTjt4zpS3C9An7v8OeLQcccvfV2T_JrB-59lTWW1W8DRxy3Zb0fU1ucFdI"
//			,"privateKey":"wv_UxgBnyMlpn5UYfCukn0J8z0DVLUKxhJIee-3p-nI"}
/*var vapidKeys={"publicKey":"BE55l1CUAaxl2P5jfLt917-rbsgMnX8UuztGh3ycEDgTtdUBGxGT_b35JSfQnhaXcdx63D0G6mVfNlAhBnSF3-0"
			,"privateKey":"IFo8jNkX-t5oP2fnqITEsr6BntFUtJ-me4L7nhMt1bw"}
*/
var vapidKeys="";
var vapidKeysFile="./vapid.keys";

var controllerLink="";
var controllerLinkFile="./controllerLink/controllerLink.txt";

if (agentManager.isController){
	vapidKeysFile="./vapidKeys/controller.vapid.keys";
} else {
	vapidKeysFile="./vapidKeys/acc.vapid.keys";
}

try {
  if (fs.existsSync(vapidKeysFile)) {
	console.log("file "+ vapidKeysFile +" exists");
	var contents = fs.readFileSync(vapidKeysFile, 'utf8');
	vapidKeys=JSON.parse(contents);
  }
} catch(err) {
  console.log(err)
}
if (vapidKeys===""){
	console.log("file "+ vapidKeysFile +" does not exists... generating new keys");
	vapidKeys = webpush.generateVAPIDKeys();
	let data = JSON.stringify(vapidKeys);  
	fs.writeFileSync(vapidKeysFile, data); 
}
console.log("Vapid File "+JSON.stringify(vapidKeys));    




var upload = multer({ 
		dest: '/tmp/',
		limits: {
			fieldSize: 16 * 1024 * 1024,
		  },		
	});
var app = express();
app.use(bodyParser.json());
//app.use(cookieParser());

var arrayRandomValues = new Uint8Array(10);
getRandomValues(arrayRandomValues);
var sSecretString='just a long random string';
sSecretString+=arrayRandomValues.toString('base64');
console.log("Secret String:"+sSecretString);

app.use(session({
    secret: sSecretString, // just a long random string
    resave: true,
    saveUninitialized: true
}));



app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
  if (req.method === 'OPTIONS') {
    return res.send(200);
  } else {
    return next();
  }
});
app.use('/scripts', express.static('/usr/src/app/webui/scripts'));
app.use('/html', express.static('/usr/src/app/webui/html'));
app.use('/htmlapp', express.static('/usr/src/app/features/kimboapp/html'));

app.post('/sendNotification', function(req, res) {
    const subscription = req.body.subscription;
	var oResult={};
	var pushMessager="";
	if ((typeof req.body.webuiId==="undefined")||(req.body.webuiId==="")){
		pushMessager=new PushMessager(subscription,vapidKeys,agentManager.sourceElement);
		agentManager.chromiumUnregisteredWebUIs.set(pushMessager.destination,pushMessager);
		pushMessager.sendMessage(
						"ACC-UPDATEUI",
						{
							theEvent:"Regitering New WebUI:"+pushMessager.destination
						},
						false,undefined,true
						);
	} else {
		pushMessager=agentManager.getWebUI(req.body.webuiId);
	}
	if (pushMessager!==""){
		oResult.result="OK";
		oResult.webuiId=pushMessager.destination;
		var theAgentManager=agentManager;
//		setTimeout(function(){
			//console.log("Sending Notification Message");
			pushMessager.sendMessage(
				req.body.msgType,
				req.body.msgContent,
				req.body.msgNeedsAck,
				function(){
					if (req.body.msgNeedsAck){
						if ((!theAgentManager.chromiumGatewayRegistered)&&
							(oResult.webuiId==theAgentManager.chromiumGatewayPushMessager.destination)){
							console.log("First ACK received from the Gateway:"+oResult.webuiId+ " --->" +req.body.msgType +" -->" +theAgentManager.chromiumGatewayRegistered);
							console.log("The Gateway is registered!")
							theAgentManager.chromiumGatewayRegistered=true;
						}
					}
				});
//		});
	} else {
		oResult.result="ERROR";
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify(oResult);
	res.end(data);	
  });

app.get('/', function (req, res) {
	res.send('Hello World!');
	res.end();
});

app.get("/vapidPublicKey", function (req, res) {
	console.log("Getting public key");
	res.setHeader('Content-type', 'text/plain' );
//	res.write(" const applicationServerPublicKey = '"+vapidKeys.publicKey+"';");
	res.write(vapidKeys.publicKey);
	res.end();
});
app.get("/systemDetails",function(req,res){
	console.log("Getting System Details");
	res.setHeader('Content-type', 'text/plain' );
	var oResult=agentManager.getSystemInfo();
	res.write(JSON.stringify(oResult));
	res.end();
});
app.post("/actions/executeTestPlan", function (req, res) {
	var executionInfo = req.body;
	console.log("Executing test plan:" + JSON.stringify(executionInfo,undefined,2)+ " \n controller:" + agentManager.isController);
	var oTestPlan=new TestPlan(executionInfo);
	var executionId=genIdentification('TestPlan');
	oTestPlan.executionId=executionId;
	oTestPlan.scenarios.forEach(scenario=>{
		scenario.executionId=genIdentification('Scenario');
		scenario.planExecutionId=executionId;
	});
	agentManager.runTestPlan(oTestPlan);
	res.setHeader('Content-type', 'text/plain' );
	res.write(JSON.stringify({result:"OK"}));
	res.end();
});

app.post("/actions/msgReceived",upload.none(), function (req, res) {
	
	var action = "";
	var values="";
	var body = req.body;
	if (typeof body.jsonContent!=="undefined"){
		var oBody=JSON.parse(body.jsonContent);
		action=oBody.action;
		values=oBody.values;
	} else {
		action=body.action;
		values=body.values;
	}
	
	;
	  // at this point, `body` has the entire request body stored in it as a string
	//console.log("=============================================");
	//console.log("=============================================");
	//console.log("Msg Data:"+JSON.stringify(body));
	messageHandler.processMessage({action:action,value:values});
	res.setHeader('Content-type', 'text/plain' );
	res.write(JSON.stringify({result:"OK"}));
	res.end();
	
	//console.log("=============================================");
	//console.log("=============================================");
});
app.post("/actions/connectToController",async function (req,res){
	var body = req.body;
	controllerLink=body.controllerLink;
	await agentManager.connectToController(controllerLink);
	fs.writeFile(controllerLinkFile,controllerLink, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	    console.log("The gateway link file was updated!");
	}); 

	res.setHeader('Content-type', 'text/plain' );
	res.write(JSON.stringify({result:"OK"}));
	res.end();
});
app.post("/actions/saveTestPlan",function (req,res){
	var testPlan = req.body;
	//debugger;
	fs.writeFile('./testPlans/'+testPlan.id+".tp", JSON.stringify(testPlan, null, 4));
/*	var controllerLink=body.controllerLink;
	var output = LZUTF8.decompress(controllerLink, {inputEncoding:"Base64"});
	var oControllerInfo=JSON.parse(output);
	var pushMessager=new PushMessager(oControllerInfo.subscription,oControllerInfo.vapidKeys,app.myId,oControllerInfo.destination);
	agentManager.controllerPushMessager=pushMessager;
    console.log("Sending message to The  controller");
	var sysInfo=agentManager.getSystemInfo();
	var statusInfo=agentManager.getStatus();
    agentManager.controllerPushMessager.sendMessage("ACC-CONNECT",{gatewayLink:sysInfo.gatewayLink,detail:statusInfo},false,false,false);
*/	res.setHeader('Content-type', 'text/plain' );
	res.write(JSON.stringify({result:"OK"}));
	res.end();
});

app.get("/actions/sendMessage", function (req, res) {
	  var body = req.body;
	  // at this point, `body` has the entire request body stored in it as a string
	  //console.log("Msg Data:"+JSON.stringify(body));
	  if (agentManager.chromiumGatewayPushMessager!=""){
		  console.log("Sending message to The  gateway");
		  agentManager.chromiumGatewayPushMessager.send({action:"Gateway Registered Locally"});
	  } else {
		  console.log("The  gateway is not loaded");
	  }
//	  setTimeout(function(){
		  res.setHeader('Content-type', 'text/plain' );
		  res.write(JSON.stringify({result:"OK"}));
		  res.end();
//	  });
});
app.post("/actions/getStatus", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var ackResult=false;
	var webuiId=body.webuiId;
	var webui=agentManager.getWebUI(webuiId);
	var oResult;
	if (webui===""){
		oResult={result:"ERROR",description:"The "+webuiId+" is not registered"};
	} else {
		var actualStatus=agentManager.getStatus();
		oResult={result:"OK",status:actualStatus};
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify(oResult);
	res.end(data);	
});
app.post("/actions/feedbackToAgent", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	var oValue=body.value;
	var agent=agentManager.getAgent(agentName);
	if (agent===""){
		res.setHeader('Content-type', 'application/json');
		var data=JSON.stringify({result:"ERROR"});
		res.end(data);	
		return;
	}
	agent.sendMessage("FEEDBACK",oValue);
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});
app.post("/actions/addPropertyToAgent", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	var oProperty=body.property;
	agentManager.addPropertyToAgent(agentName,oProperty);
//	agent.sendMessage("ADDPROPERTY",{agentName:agentName,property:oProperty});
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});
app.post("/actions/delAgentProperty", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	var propertyName=body.propertyName;
	agentManager.delAgentProperty(agentName,propertyName);
//	agent.sendMessage("ADDPROPERTY",{agentName:agentName,property:oProperty});
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});


app.post("/actions/loadAgent", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	await agentManager.loadAgent(agentName);
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});
app.post("/actions/reloadAgent", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	await agentManager.reloadAgent(agentName);
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});

app.post("/actions/runScenarios", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	var scenarios=body.scenarios;
	var agent=agentManager.getAgent(agentName);
	if (agent===""){
		res.setHeader('Content-type', 'application/json');
		var data=JSON.stringify({result:"ERROR"});
		res.end(data);	
		return;
	}
	agent.arrProcessedScenarios=[];
	scenarios.forEach(function(scenario){
		agent.addScenarioToRun(new Scenario(scenario));
	});
	agent.run();
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});

app.post("/actions/traceExecutionTree", async function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	var agentName=body.agentName;
	var agent=agentManager.getAgent(agentName);
	if (agent===""){
		res.setHeader('Content-type', 'application/json');
		var data=JSON.stringify({result:"ERROR"});
		res.end(data);	
		return;
	}
	agent.traceExecutionTree();
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});



app.post("/actions/changeScreenshot", function (req, res) {
	debugger;
	//console.log("Change Screenshot behaviour:" + JSON.stringify(body,undefined,2)+ " \n controller:" + agentManager.isController);
	var body = req.body;
	var agentName=body.agentName;
	var action=body.action;
	if (action=="start") {
		screenshotManager.startTakingScreenshots(agentName);
	} else if(action=="stop"){
		screenshotManager.stopTakingScreenshots(agentName);
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
	
});

app.post("/actions/getScreenshot", async function (req, res) {
	var params = req.body;
	//console.log("Getting Screenshot:" + JSON.stringify(params,undefined,2)+ " \n controller:" + agentManager.isController);
	var agentName=params.agentName;
	var screenshot=await screenshotManager.getScreenshot(agentName);
	if ((typeof screenshot==="undefined")||(screenshot==="")){
		fs.readFile('/usr/src/app/webui/html/screenshots/peppa.jpg', function(err, data) {
			if (err) throw err; // Fail if the file can't be read.
			res.writeHead(200, {'Content-Type': 'image/jpeg'});
			res.end(data); // Send the file data to the browser.
		});
	 } else {
		res.writeHead(200, {'Content-Type': 'image/png'});
		res.end(Buffer.from(screenshot)); // Send the file data to the browser.
	}
});
app.post("/actions/setScreenshot",upload.none(), async function (req, res) {
	debugger;
	var params="";
	if (typeof req.body.jsonContent!=="undefined"){
		params = JSON.parse(req.body.jsonContent);
	} else {
		params = req.body;
	}
	//console.log("Getting Screenshot:" + JSON.stringify(params,undefined,2)+ " \n controller:" + agentManager.isController);
	var agentName=params.agentName;
	var screenshot=params.screenshot;
	await screenshotManager.setScreenshot(screenshot,agentName);
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});
app.post("/actions/saveSeleniumInterface", async function (req, res) {
	var params = req.body;
	//console.log("Getting Screenshot:" + JSON.stringify(params,undefined,2)+ " \n controller:" + agentManager.isController);
	var agentName=params.agentName;
	agentManager.saveSeleniumInterface(params.agentName,params.interfaceName);
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});

//su -c /usr/src/app/updateNodeApp $ENV_USER
//su -c /usr/src/app/updateKimbo $ENV_USER
//su -c /usr/src/app/updateKimboApp $ENV_USER
//	<button id="btnReloadNodeSystem" onclick="updateSystem('node')" >Update Node System</button>
//	<button id="btnReloadKimboLibrary" onclick="updateSystem('kimbo')" >Update Kimbo Lib</button>
//	<button id="btnReloadKimboApp" onclick="updateSystem('kimboapp')" >Update kimboapp</button>
//	<button id="btnReloadAll" onclick="updateSystem('all')" >Update All System</button> 

app.post("/actions/updateSystem", async function (req, res) {
	debugger;
	console.log("Updating System");
	var body = req.body;
	var systemName=body.system;
	if (systemName=="node"){
		agentManager.updateSystem("updateNodeApp");
	} else if (systemName=="kimbo"){
		agentManager.updateSystem("updateKimbo");
	} else if (systemName=="kimboapp"){
		agentManager.updateSystem("updateKimboApp");
	} else if (systemName=="all"){
		console.log("Updating All System");
		agentManager.updateSystem("updateAllSystem");
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify({result:"OK"});
	res.end(data);	
});




async function processAck(msg){
	//console.log("Ack to message:"+JSON.stringify(msg));
	var ackResult=true;
	if (typeof msg!=="undefined"){
		var ackDest=msg.msgSource;
		if ((typeof msg.msgNeedsAck!=="undefined")&&msg.msgNeedsAck){
			if (ackDest==agentManager.sourceElement){ // message form the same nodejs 
				var webui=agentManager.getWebUI(msg.msgDestination);
				if (webui!==""){
					ackResult=await webui.ack(msg.msgId);
				}
			} else if (agentManager.mapACCs.has(msg.msgSource)){
				//debugger;
				var pmACC=agentManager.mapACCs.get(msg.msgSource)
				pmACC.pushMessager.sendACK(msg.msgId);
			}
		}
	}
	var oResult={result:(ackResult?"OK":"ERROR")};
	return oResult;
}
var multipartMessagesBySession=new Map();

async function decompress(b64Cad){
	var buffer=new Buffer.from(b64Cad, 'base64');
	var fncResolve="";
	var fncReject="";
	var prmResult=new Promise((resolve, reject) => {
		fncResolve=resolve;
		fncReject=reject;
	});
	zlib.gunzip(buffer, function (error, result) {
		//debugger;
		//console.log(result);
	   if (error) {
		   fncReject(error);
	   } else {;
			fncResolve(result.toString('utf8'));
	   }
	})
	return prmResult;
}

async function processMultipart(msg,theSession){
	var sessionId=theSession.id;
	var multiPartMessages;
/*	if (!multipartMessagesBySession.has(sessionId)){
		multiPartMessages=new Map();
		multipartMessagesBySession.set(sessionId,multiPartMessages);
	} else {
		multiPartMessages=multipartMessagesBySession.get(sessionId);
	}
*/	
	multiPartMessages=multipartMessagesBySession;
	var oJson=msg;
	var msgId=oJson.srcMsgId;
	var nParts=oJson.parts;
	var iPart=oJson.partNumber;
	var partContent=oJson.partContent;
	var oTempPartManager;
	if (!multiPartMessages.has(msgId)){
		oTempPartManager={msgId:msgId,nParts:nParts,nPartsFilled:0,parts:[]};
		multiPartMessages.set(msgId,oTempPartManager);
		for (var i=0;i<nParts;i++){
			oTempPartManager.parts.push({status:"empty",content:""});
		}
	} else {
		oTempPartManager=multiPartMessages.get(msgId);
	}
	oTempPartManager.nPartsFilled++;
	oTempPartManager.parts[iPart].status="filled";
	oTempPartManager.parts[iPart].content=partContent;
	var oResult={bFullMsgLoaded:false,fullMsg:""};
	if (oTempPartManager.nPartsFilled==oTempPartManager.nParts){
		//debugger;
		multiPartMessages.delete(msgId);
		var fullContent="";
		for (var i=0;i<oTempPartManager.nParts;i++){
			var part=oTempPartManager.parts[i];
			if (part.status!="filled"){
				//debugger;
				console.log("Error with multipart message");
			}
			fullContent+=part.content;
		}
		//debugger;
		
		console.log("decompressing multipart content");
		var sDecompressed=await decompress(fullContent);
		console.log("parsing JSON multipart content:"+sDecompressed.length);
		var oFullMsg=JSON.parse(sDecompressed);
		console.log("JSON multipart content PARSED!");
		oResult.bFullMsgLoaded=true;
		oResult.fullMsg=oFullMsg;
	}
	return oResult;
}

app.post("/actions/preprocessMessage", upload.none(), async function (req, res) {
//	debugger;
	var actionValues = "";
	var fullMsg="";
	if (typeof req.body.jsonContent!=="undefined"){
		actionValues=JSON.parse(req.body.jsonContent);
		fullMsg=actionValues;
	} else {
		actionValues=req.body.values;
		fullMsg=req.body;
	}
	//console.log("Reg Data:"+JSON.stringify(actionValues));
	//var oResult={bFullMsgLoaded:false,fullMsg:""};
	var oResult={bFullMsgLoaded:true,fullMsg:req.body};
	if ((typeof actionValues!=="undefined")&&
	    (actionValues.msgType=="MULTIPART_PART")){
		//debugger;
		var theSession=req.session;
		var oAckResult=processAck(actionValues); // confirm reception as soon as possible
		oResult=await processMultipart(actionValues.msgContent,theSession);
		if (oResult.bFullMsgLoaded){
			var auxFullMsg=oResult.fullMsg;
			oResult.fullMsg={action:auxFullMsg.msgType,values:auxFullMsg};
		}
	}
	var oAckResult=processAck(oResult.fullMsg.values); 
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify(oResult);
	res.end(data);	
});

app.post("/actions/directConnectionCheck",async function(req,res){
	var params=req.body;
	var oResult={enabled:true
				, srcIp:params.srcIp};
	oResult.dstIp=agentManager.ip;
	oResult.bidirectional=false;
	if ((typeof params.secondCall==="undefined")||(!params.secondCall)){
		oResult.bidirectional=true;
		try {
			var connResult=await agentManager.postToURL(
									"https://"+params.srcIp+":8080/actions/directConnectionCheck",
									{message:"Checking HTTPS Direct Comms",secondCall:true});
		} catch (error){
			oResult.bidirectional=false;
		}
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify(oResult);
	res.end(data);	
});

	
	
app.post("/actions/registerWebUi", function (req, res) {
	var body = req.body;
	//console.log("Reg Data:"+JSON.stringify(body));
	// at this point, `body` has the entire request body stored in it as a string
	var pushSubscription=body.subscription;
	var pushMessager="";	
	var bWasUnregistered=false;
	pushMessager=agentManager.getWebUI(req.body.webuiId);
	if (pushMessager===""){
		pushMessager=new PushMessager(pushSubscription,vapidKeys,agentManager.sourceElement);
	} else if (agentManager.chromiumUnregisteredWebUIs.has(pushMessager.destination)){
		agentManager.chromiumUnregisteredWebUIs.delete(pushMessager.destination);
		bWasUnregistered=true;
	}
	var bIsGateway=body.gateway;
	var bForce=body.force;
	var bRegistered=true;
	if (bIsGateway){
		console.log("Is the GateWay");
		if (bForce||(agentManager.chromiumGatewayPushMessager=="")){
			agentManager.chromiumGatewayPushMessager=pushMessager;
		} else {
			bRegistered=false;
		}
	} else {
		console.log("Is not the GateWay");
		if (bWasUnregistered){
			agentManager.chromiumRegisteredWebUIs.set(pushMessager.destination,pushMessager);
		}
	}
	console.log("Sending message to register webui (gateway:"+bIsGateway+")");
	var oResult={};
	var theAgentManager=agentManager;
	if (bRegistered){
		oResult={
			result:"OK",
			gateway:bIsGateway,
			webuiId:pushMessager.destination
		};
//		setTimeout(function(){
			console.log("Sending MSG");
			pushMessager.sendMessage(
				"ACC-REGISTERED",
				{
					gateway:bIsGateway,
					webuiId:pushMessager.destination,
					gatewayId:(agentManager.chromiumGatewayPushMessager===""?"":agentManager.chromiumGatewayPushMessager.destination)
					,systemDetails:agentManager.getSystemInfo()
				},
				true,
				function(){
					console.log("Webui "+pushMessager.destination+" registering process finalized!");
					if ((!theAgentManager.chromiumGatewayRegistered)&&
							(pushMessager.destination==theAgentManager.chromiumGatewayPushMessager.destination)){
							console.log("The Gateway is registered!")
							theAgentManager.chromiumGatewayRegistered=true;
					}

					pushMessager.sendMessage(
						"ACC-UPDATEUI",
						{
							theEvent:"New "+(bIsGateway?"Gateway":"WebUI")+ " registered:"+pushMessager.destination
						},
						false,undefined,true
						);
				});
//		});
	} else {
		oResult={
			result:"Error",
		};
	}
	res.setHeader('Content-type', 'application/json');
	var data=JSON.stringify(oResult);
	res.end(data);
});
/*

var fncServer=function (req, res) {
	//debugger;
  console.log(`${req.method} ${req.url}`);
  // parse URL
  
  var theUrl=new URL("https://"+req.headers.host+req.url);
  var bIsGateway=theUrl.searchParams.get('gateway')=="true";
  const parsedUrl = url.parse(req.url);
  // extract URL path
  let pathname = "webui"+`${parsedUrl.pathname}`;
  var urlQuery=parsedUrl.query;
  console.log("Path Name:"+pathname);
  if (pathname=="webui/") {
	  pathname="webui/index.html";
  }
  console.log("Path Name:"+pathname+" is Gateway:"+bIsGateway);
  // based on the URL path, extract the file extention. e.g. .js, .doc, ...
  const ext = path.parse(pathname).ext;
  // maps file extention to MIME typere
  const map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.wav': 'audio/wav',
    '.mp3': 'audio/mpeg',
    '.svg': 'image/svg+xml',
    '.pdf': 'application/pdf',
    '.doc': 'application/msword'
  };
  if (pathname=="webui/scripts/applicationServerPublicKey.js"){
	console.log("Getting public key");
	res.setHeader('Content-type', map[ext] || 'text/plain' );
	res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
	res.write(" const applicationServerPublicKey = '"+vapidKeys.publicKey+"';");
	res.end();
  } else if (pathname=="webui/actions/msgReceived"){
	let body = [];
	req.on('data', (chunk) => {
	  body.push(chunk);
	}).on('end', () => {
	  body = Buffer.concat(body).toString();
	  // at this point, `body` has the entire request body stored in it as a string
	  console.log("Msg Data:"+body);
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		res.setHeader('Content-type', map[ext] || 'text/plain' );
		res.write(JSON.stringify({result:"OK"}));
		res.end();
	});
  } else if (pathname=="webui/actions/registerWebUi"){
	console.log("Registering Agent");
	console.log("Is Gateway:"+bIsGateway);
	let body = [];
	req.on('data', (chunk) => {
		body.push(chunk);
	}).on('end', async () => {
		body = Buffer.concat(body).toString();
		console.log("Req Data:"+body);
		// at this point, `body` has the entire request body stored in it as a string
		var pushSubscription=JSON.parse(body);
		var pushMessager=new PushMessager(pushSubscription,webpush,vapidKeys);
		if (bIsGateway){
			console.log("Is the GateWay");
			if (agentManager.chromiumGatewayPushMessager==""){
				agentManager.chromiumGatewayPushMessager=pushMessager;
				pushMessager.send({action:"Gateway Registered Locally"});
			}
		} else {
			console.log("Is not the GateWay");
			agentManager.chromiumWebUiMessager.push(pushMessager);
			console.log("Sending message");
			pushMessager.send({action:"WebUI Registered Locally"});
		}
	});
	res.setHeader('Content-type', 'application/json');
	res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
	var data=JSON.stringify({result:"OK"});
	res.end(data);
  } else {
	  fs.exists(pathname, function (exist) {
		if(!exist) {
		  // if the file is not found, return 404
		  res.statusCode = 404;
		  res.end(`File ${pathname} not found!`);
		  return;
		}

		// if is a directory search for index file matching the extention
		if (fs.statSync(pathname).isDirectory()) pathname += '/index' + ext;

		// read file from file system
		fs.readFile(pathname, function(err, data){
		  if(err){
			res.statusCode = 500;
			res.end(`Error getting the file: ${err}.`);
		  } else {
			// if the file is found, set Content-type and send data
			res.setHeader('Content-type', map[ext] || 'text/plain' );
			res.end(data);
		  }
		});
	  });
  }
};
https://saedga.bitbucket.io/?localUrl=http://localhost:58080

//http.createServer(options,fncServer).listen(parseInt(port));
*/
/*app.listen(parseInt(port), function () {
  console.log(`Server listening on port ${port}`);
});
*/
const options = {
  key: fs.readFileSync('webui/keys/key.pem'),
  cert: fs.readFileSync('webui/keys/cert.pem')
};

https.createServer(options, app).listen(parseInt(port), function () {
  console.log('Example app listening on port '+parseInt(port)+' Go to https://localhost:'+parseInt(port)+'/');
})

//console.log(require('path').resolve("."));
var theUrl="https://localhost:8080/html/indexDirect.html?gateway=true";
/*if (!agentManager.isController){
	theUrl="https://localhost:8080/html/indexDirect.html";
}*/

try {
	child_process.execFileSync('./killGateway');
} catch (err){
	console.log(err);
}

const child = spawn(
					'./launchChromiumAsUser'
						,[":1"
						,"--test-type --hide-crash-restore-bubble"
						,"--no-sandbox"
						,"--ignore-certificate-errors"
						,"--user-data-dir=/tmp/chromiumGateway"
						,"--unsafely-treat-insecure-origin-as-secure=https://localhost:8080"
						,"--disk-cache-dir=/dev/null"
						,"--disk-cache-size=1"
						,theUrl]);
						
theGatewayBrowser=child;

async function fncCheckController(){
	//console.log("Check the Controller Link Changes");
	if (!agentManager.isController){
		//console.log("Is not Controller");
		if (agentManager.chromiumGatewayRegistered){
			//console.log("The Gateway is Registered.... continue registering controller");
			try {
				//console.log("Try inner");
				if (fs.existsSync(controllerLinkFile)) {
					var auxControllerLink= fs.readFileSync(controllerLinkFile, 'utf8');
					if ((auxControllerLink!=controllerLink)||(!agentManager.controllerRegistered)){
						controllerLink=auxControllerLink;
						await agentManager.connectToController(controllerLink);
						console.log("Conected?!");
						debugger;
					} else {
						console.log("The controllerLink is equal and the node is registered");
					}
					
				}
			} catch(err) {
				console.log(err);
			}
		} else {
			console.log("The Gateway is NOT Registered.... waitting for registering of gateway first");
		}
		setTimeout(fncCheckController,30000);	
	}
}
fncCheckController();