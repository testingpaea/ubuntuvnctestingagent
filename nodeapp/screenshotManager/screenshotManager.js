
var ScreenshotManager=class ScreenshotManager{
	constructor(instanceId){
		var self=this;
		self.screenshots=new Map();
	}
	getScreenshotData(agentName){
		var self=this;
		var shotData={
			agentName:agentName,
			screenshotFrequency:5000,
			screenshot:"",
			lastScreenshotConsumedByACC:"",
			lastScreenshotConsumedByController:"",
			controllerRequestsCounter:0,
			accRequestsCounter:0,
			sendingScreenshot:false,
			takingScreenshot:false,
		}		
		if (self.screenshots.has(agentName)){
			return self.screenshots.get(agentName);
		} 
		self.screenshots.set(agentName,shotData);
		return shotData;
	}
	async getScreenshot(agentName){
		var self=this;
		var scrsResult="";
		const agentManager = require('../agentManager/agentManager');
		var shotData=self.getScreenshotData(agentName);
		scrsResult=(typeof shotData.screenshot==="undefined"?"":shotData.screenshot);
		self.updateScreenshot(agentName,shotData.screenshotFrequency);
		return scrsResult;
	}
	waitForNextScreenshotForController(agentName){
		var self=this;
		var shotData=self.getScreenshotData(agentName);
		if (shotData.lastScreenshotConsumedByController===""){
			return -1; // dont wait
		}
		if ((shotData.screenshotFrequency==="")||(typeof shotData.screenshotFrequency==="undefined")){
			shotData.screenshotFrequency=5000;
		}
		var timeScope=(1*shotData.screenshotFrequency);
		var nMillis=((shotData.lastScreenshotConsumedByController+timeScope)-(new Date()).getTime());
		return nMillis;
	}
	waitForNextScreenshotForACC(agentName){
		var self=this;
		var shotData=self.getScreenshotData(agentName);
		if (shotData.lastScreenshotConsumedByACC===""){
			return -1; // don´t wait
		}
		if ((shotData.screenshotFrequency==="")||(typeof shotData.screenshotFrequency==="undefined")){
			shotData.screenshotFrequency=5000;
		}
		var timeScope=(1*shotData.screenshotFrequency);
		var nMillis=((shotData.lastScreenshotConsumedByACC+timeScope)-(new Date()).getTime());
		return nMillis;
	}
	async updateScreenshot(agentName,freq,bController){
		var self=this;
		const agentManager = require('../agentManager/agentManager');
		var agent=agentManager.getAgent(agentName);
		
		var shotData=self.getScreenshotData(agentName);
		if (typeof freq!=="undefined") shotData.screenshotFrequency=freq;
		var bUpdateNeeded=false;
		if (agentManager.isController){
			shotData.controllerRequestsCounter++;
			bUpdateNeeded=(self.waitForNextScreenshotForController(agentName)<0);
			if ((!shotData.takingScreenshot)&&bUpdateNeeded){
				var theACC=agentManager.mapACCs.get(agent.accId);
				if (typeof theACC!=="undefined"){
					var pushMessager=theACC.pushMessager;
					var params={
						agentName:agentName
						,freq:shotData.screenshotFrequency
					};
					shotData.takingScreenshot=true;
					await pushMessager.sendMessage("TAKE SCREENSHOT",params,false,undefined,false);
				}
			}
		} else {
			shotData.accRequestsCounter++;
			bUpdateNeeded=self.waitForNextScreenshotForACC(agentName)<0;
			if ((bUpdateNeeded)&&(!shotData.takingScreenshot)){
				shotData.takingScreenshot=true;
				agent.takeScreenshot();
			}
		}
	}
	async setScreenshot(screenshot,agentName){
		var self=this;
		const agentManager = require('../agentManager/agentManager');
		var shotData=self.getScreenshotData(agentName);
		
		console.log("SETTING SCREENSHOT ACC:"+agentManager.isACC+" CTLR:"+agentManager.isController);
		
		shotData.screenshot=screenshot;
		shotData.takingScreenshot=false;

		if (agentManager.isController){
			shotData.controllerRequestsCounter=0;
			shotData.lastScreenshotConsumedByController=(new Date()).getTime();
		} else {
			var prevRequests=shotData.accRequestsCounter;
			shotData.accRequestsCounter=0;
			if (prevRequests<=1){
				shotData.lastScreenshotConsumedByACC=(new Date()).getTime();
			} else {
				var mWait=self.waitForNextScreenshotForACC(agentName);
				if (mWait<0) mWait=undefined;
				setTimeout(function(){
					self.updateScreenshot(agentName,shotData.screenshotFrequency);
				},mWait);
			}
			if ( (agentManager.controllerPushMessager.source!=="")&&
				 (typeof agentManager.controllerPushMessager.source!=="undefined")
				 ){ // there is a controller and is consuming screenshots
				if (shotData.sendingScreenshot){
					console.log("The ACC is sending this screenshot yet");
				} else {
					var mWaitController=self.waitForNextScreenshotForController(agentName);
					var bUpdateControllerNow=(mWaitController<0)&&(shotData.controllerRequestsCounter>0);
					if (bUpdateControllerNow){
						console.log("Calling to Send Screenshot to Controller");
						try {
							await self.sendScreenshotToController(agentName,screenshot);
						} catch (theError){
							console.log("Error Sending Screenshot to Controller:"+theError);
						}
						console.log("End of Calling to Send Screenshot to Controller");
						shotData.controllerRequestsCounter=0;
						shotData.lastScreenshotConsumedByController=(new Date()).getTime();
					}
				}
			} else {
				console.log("There is not controller registered!");
			}
		}
	}
	async sendScreenshotToController(agentName,screenshot){
		var self=this;
		var shotData=self.getScreenshotData(agentName);
		shotData.sendingScreenshot=true;
		const agentManager = require('../agentManager/agentManager');
		var pm=agentManager.controllerPushMessager;
		
		var fncResolve;
		var fncReject;
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		var onScreenshotTransferred=function onScreenshotTransferred(){
			console.log("Screenshot sending ends");
			debugger;
			shotData.sendingScreenshot=false;
			fncResolve();
		}
		
		var oData={agentName:agentName,screenshot:screenshot};
		
		if ((typeof pm.httpConnection!=="undefined")&& (pm.httpConnection!=="")&&
			(pm.httpConnection.enabled)){
			self.postScreenshotToController(oData,shotData,pm.httpConnection,onScreenshotTransferred);
		} else {
			self.pushScreenshotToController(oData,shotData,pm,onScreenshotTransferred);
		}
		return prmResult;
	}
	async pushScreenshotToController(oData,shotData,pushMessager,callback){
		var self=this;
		console.log("Screenshot PUSH sending starting");
		pushMessager.sendMessage("SCREENSHOT",oData,true,callback,false);
		console.log("Screenshot PUSH sending started");
	}
	async postScreenshotToController(oData,shotData,httpConnection,callback){
		var self=this;
		const agentManager = require('../agentManager/agentManager');
		debugger;
		console.log("Screenshot POST sending starting");
		var url=httpConnection.controllerBaseUrl+"/actions/setScreenshot";
		await agentManager.postToURL(url,oData,true);
		//httpConnection.sendMessage("SCREENSHOT",oData,true,callback,false);
		debugger;
		callback();
		console.log("Screenshot POST sending started");
	}


}
var screenshotManager=new ScreenshotManager();

module.exports=screenshotManager;
