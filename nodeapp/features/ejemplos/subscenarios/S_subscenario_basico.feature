Feature: Subescenario Basico de ejemplo

@subScenario @APP:PRUEBA @ISSUE:TEST-09-01
Scenario: Subescenario Basico de ejemplo
	Given assign "(#( (parseInt($actAnidamiento)+1)+'' )#)" to variable "actAnidamiento"
	Given assign "$actAnidamiento" to variable "innerAnidamiento"
	Given if "$actAnidamiento < $maxAnidamiento" then
		Given the scenario "Subescenario Basico de ejemplo"
	Given else
				Given show trace "No anidar mas"
	Given end if
	Given show progress box "Prueba Basica de llamada a subscenario anidamiento:$innerAnidamiento" for "2" seconds
	
	
