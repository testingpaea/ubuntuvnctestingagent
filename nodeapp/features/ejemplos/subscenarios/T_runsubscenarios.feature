Feature: Ejemplo de ejecucion de subscenarios

@PRUEBA @ISSUE:TEST-09
Scenario: Ejemplo de ejecucion de subscenarios 
	Given in app "PRUEBA" 
	Given set "5" to new global variable "maxAnidamiento"	
	Given set "0" to new global variable "actAnidamiento"	
	Given the scenario "Subescenario Basico de ejemplo"
	Given the scenario "Subescenario Con Parametros de ejemplo" where variables '{"$waitTime":"4","$messageText":"Texto pasado al subscenario"}'
	
#	Given set "(#( ['A1','A2','A3','A4','A5','A6','A7','A8','A9','A10'] )#)" to new variable "arrAs"
#	Given loop for each element "arrElement" in "$arrAs" do
#		Given the scenario "Subescenario Basico de ejemplo"
#		Given debugger
#		Given the scenario "Subescenario Con Parametros de ejemplo" where variables '{"$waitTime":"2","$messageText":"Prueba dentro del While.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"}'
#	Given end loop  	
	
	
	
