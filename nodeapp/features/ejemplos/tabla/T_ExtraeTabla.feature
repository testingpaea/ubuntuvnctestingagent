Feature: Extrae todas las filas de una tabla

@PRUEBA @ISSUE:TEST-10
Scenario: Extrae todas las filas de una tabla 
	Given close all browsers
	Given in app "PRUEBA" 
  Given launch browser to "http://192.168.1.120:18080/ejemplo_tabla.html" maximized
  Given show progress box "Esperando que cargue" for "3" seconds
  
	Given wait for load of interface "prueba tabla"
	Given get all table values of "Tabla de Prueba"
	Given assign returned value to "tblAux"
	
  Given show progress box "Crear Objetos" for "3" seconds
	Given use table "tblAux" to create clones of object "DatosFila" wich index "id" is "Empty" filling attributes "id,Contact,Country"
	Given remove object "DatosFila" with id "Empty"
	
	
	Given assign to variable "nFilas" the number of stored objects "DatosFila"
	Given assign to variable "arrIDs" the list of IDs of stored objects "DatosFila"


	
  Given show progress box "Va a empezar el loop" for "20" seconds
	Given loop for each element "arrElement" in "$arrIDs" do
		Given debugger
		Given the scenario "Subescenario para procesar un objeto de la lista datos de prueba" where variables '{"$idObjeto":"$arrElement","$messageText":"Prueba dentro del While.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"}'
		
	Given end loop
	
  Given show progress box "Extraer attributos" for "20" seconds
	Given load from interface the attributes of object "DatosFila" wich id is "Alfreds Futterkiste"
	
		

 
