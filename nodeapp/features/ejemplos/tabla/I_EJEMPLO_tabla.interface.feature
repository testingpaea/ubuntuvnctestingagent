Feature: Definicion del Interface Web de pruebas con una tabla

@Interface @APP:PRUEBA
 Scenario: Definicion del Interface Web de pruebas con una tabla
	Given add "selenium" interface "prueba tabla" with 
	"""
{
    "testLoaded": {
        "element": "BODY",
        "testWay": "VISIBLE"
    },
    "elements": [
        {
            "id": "Tabla de Prueba",
            "selector": {
                "type": "xpath",
                "expression": "//*/table",
                "frameNumbers": []
            },
            "focusable": false,
            "caption": "",
            "HTMLtag": "table",
            "innerHTML": ""
        },{
            "id": "BODY",
            "selector": {
                "type": "xpath",
                "expression": "//*/body",
                "frameNumbers": []
            },
            "focusable": false,
            "caption": "BODY"
        }
    ],
    "relatedScreens": [],
    "extendsScreen": ""
}
	"""