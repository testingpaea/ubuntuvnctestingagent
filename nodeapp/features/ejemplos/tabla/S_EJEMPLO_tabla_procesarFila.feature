Feature: Subescenario para procesar filas de la tabla de ejemplo

@subScenario @APP:PRUEBA @ISSUE:TEST-10-01
@PARAMS:[{"name":"idObjeto","description":"Identificador_del_Objeto_a_Procesar","defaultValue":"Empty"}]
@PARAMS:[{"name":"messageText","description":"Texto","defaultValue":"Simulando30"}]
Scenario: Subescenario para procesar un objeto de la lista datos de prueba 
 	Given debugger
	Given show progress box "$idObjeto ---\r\n $messageText" for "$waitTime" seconds
  Given the scenario "show progressbox" where variables '{"$waitTime":"8","$messageText":"Pasando a otro subscenario por variable:$idObjeto"}'
