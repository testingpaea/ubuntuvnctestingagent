Feature: Definicion de un objeto persistente para almacenar en base de datos

@DataObject
Scenario: Definicion de un objeto persisntente para almacenar en base de datos
	Given add persistent storable object "BDDatosFila" with id "Empty" and values from json
	"""
	{
	"nombre":"",
	"apellido":"",
	"Dates":{
		"init":"",
		"end":""
		},
	 "persistence":{
	 		"provider": "rpaCoffee_mysql",
	 		"storename":"PERSONA",
	 		"attrsUniqueId":["id"],
	 		"attrsAlias":[],
	 		"attrsExcluded":["Dates"]
	 }
	}
	"""
