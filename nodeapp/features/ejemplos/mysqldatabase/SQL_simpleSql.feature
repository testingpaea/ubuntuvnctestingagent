Feature: Definicion de una consulta SQL simple. Obtener todos los registros de la tabla PERSONA

@SQL
 Scenario: Obtener todos los registros de la tabla PERSONA
	Given add sql id "Obtener todos los registros de la tabla PERSONA" with
	"""
	{
	"description":"Obtener todos los registros de la tabla PERSONA",
	"sql":"SELECT * FROM PERSONA"
	}
	"""
