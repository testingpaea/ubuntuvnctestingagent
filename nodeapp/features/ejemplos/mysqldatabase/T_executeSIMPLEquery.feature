Feature: Ejecutar una consulta sobre una base de datos

@ISSUE:TEST-11
  Scenario: Ejecuta la consulta simple sobre Mysql
	Given save in variable "sqlResult" the result of execute, in database "rpaCoffee_mysql", the query "Obtener todos los registros de la tabla PERSONA"
	Given set to new variable "tblAux" multiline text
			"""
				[
					["5","Jose","Perez"],
					["6","Pedro","Gil"],
					["7","Pepa","Gonzalez"]
					]
			"""
		Given use table "tblAux" to create clones of object "BDDatosFila" wich index "id" is "Empty" filling attributes "id,nombre,apellido"
		Given remove object "BDDatosFila" with id "Empty"
		Given remove storage for objects "BDDatosFila"
		Given create storage for objects "BDDatosFila"
		Given persist all objects "BDDatosFila"
	