Feature: Muestra una ventana de progreso indeterminado espera un poco y la cierra

@ISSUE:TEST-08
@PARAMS:[{"name":"waitTime","description":"Tiempo","defaultValue":"30"}]
@PARAMS:[{"name":"messageText","description":"Texto","defaultValue":"VentanaProgresoEjemplo"}]
Scenario: muestra ventana de progreso y la cierra un número de segundos despues 
	Given show progress box "$messageText"
	Given wait "$waitTime" seconds
	Given close progress box
	Given show progress box "(Second Form) $messageText" for "$waitTime" seconds
