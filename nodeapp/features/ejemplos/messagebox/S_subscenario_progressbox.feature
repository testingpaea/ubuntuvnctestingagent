Feature: Subescenario Muestra un mensaje durante un tiempo determinado

@subScenario @ISSUE:TEST-08-01
@PARAMS:[{"name":"waitTime","description":"Tiempo","defaultValue":"30"}]
@PARAMS:[{"name":"messageText","description":"Texto","defaultValue":"Simulando30"}]
 Scenario: show progressbox 
	Given show progress box "$messageText" for "$waitTime" seconds
