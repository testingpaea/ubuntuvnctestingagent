Feature: Ejecutar una consulta sobre mongodb

@ISSUE:TEST-15
  Scenario: Ejecuta la consulta simple sobre mongoDB
  Given empty storage "PERSONA" of database "test_mongodb"
	Given create a new object variable "arrObjsToMongo" with json
	"""
		[
		{
		"nombre":"Pepe",
		"apellido":"Gomez",
		"numPos":3
		,"CampoAdicional":"A"
		},{
		"nombre":"Juana",
		"apellido":"Sanz",
		"numPos":4
		,"CampoAdicional":"B"		
		},{
		"nombre":"Jose",
		"apellido":"Perez",
		"numPos":5
		,"CampoAdicional":"C"		
		},{
		"nombre":"Antxon",
		"apellido":"Aguirre",
		"numPos":6
		,"CampoAdicional":"D"		
		}
		]
	"""
	Given new object "MongoBDObjDatos" from "$arrObjsToMongo"
	Given save in variable "queryResult" the result of execute, in database "test_mongodb", the query "Obtener todos los objetos de la coleccion PERSONA"
	Given add object "MongoBDObjDatos" with id "00001" and values from json
	"""
		{
		"nombre":"Pedro",
		"apellido":"Gomez",
		"numPos":1,
		"Dates":{
			"init":"01/01/2001",
			"end":"31/12/2001"
			}
		}
	"""
	Given add object "MongoBDObjDatos" with id "00002" and values from json
	"""
		{
		"nombre":"Maria",
		"apellido":"Sanchez",
		"numPos":2,
		"Dates":{
			"init":"01/01/2002",
			"end":"31/12/2002"
			}
		}
	"""
	Given persist all objects "MongoBDObjDatos"
	