Feature: Definicion de una consulta simple para mongo. Obtener todos los objetos de la coleccion PERSONA

@QUERY   
Scenario: Obtener todos los objetos de la coleccion PERSONA
	Given add query id "Obtener todos los objetos de la coleccion PERSONA" with
	"""
	{
	"description":"Obtener todos los objetos de la coleccion PERSONA",
	"collection":"PERSONA",
	"action":"find",
	"postAction":"toArray",
	"actionParam1":{ "nombre": "pedro" },
	"actionParam2":"",
	"actionParam3":""
	}
	"""
