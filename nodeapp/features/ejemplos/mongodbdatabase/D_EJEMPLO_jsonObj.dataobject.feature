Feature: Definicion de un objeto persistente de ejemplo para almacenar en mongoDB

@DataObject
Scenario: Definicion de un objeto persistente de ejemplo para almacenar en mongoDB
	Given add persistent storable object factory "MongoBDObjDatos" with this sample
	"""
	{
	"nombre":"",
	"apellido":"",
	"Dates":{
		"init":"",
		"end":""
		},
	 "persistence":{
	 		"provider": "test_mongodb",
	 		"storename":"PERSONA",
	 		"attrsUniqueId":["id"],
	 		"attrsAlias":[],
	 		"attrsExcluded":["Dates"]
	 }
	}
	"""
	Given add function to "MongoBDObjDatos" object
	"""
		async function getIdIniciativa(){
			debugger;
			var self=this;
			console.log("test Function");
			console.log(JSON.stringify(self));
		}
	"""
	