Feature: Definicion del grafo de navegacion entre interfaces de Ejemplo

@InterfaceNavigation @APP:PRUEBA
 Scenario: Definicion del grafo de navegacion entre interfaces de Ejemplo
	Given add interface navigation "navegacion_example" with 
	"""
{
	"cuantias de actuacion coffee": {
		"to": {
			"actuacion coffee": {
				"element": "Datos generales"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"contratos coffee": {
				"element": "Contratos Asociados"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subvenciones coffee": {
				"element": "Subvenciones asociadas"
			}
		}
	},
	"actuacion coffee": {
		"to": {
			"cuantias de actuacion coffee": {
				"element": "Recursos economicos"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"contratos coffee": {
				"element": "Contratos Asociados"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subvenciones coffee": {
				"element": "Subvenciones asociadas"
			}
		}
	},
	"actuaciones coffee": {
		"to": {
			"actuacion coffee": {
				"element": "Buscar y Primera Fila"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subproyectos instrumentales coffee": {
				"element": "Subproyectos instrumentales"
			},
			"subproyectos coffee": {
				"element": "Subproyectos"
			}
		}
	},
	"contrato coffee": {
		"to": {
			"cuantias de actuacion coffee": {
				"element": "Recursos economicos"
			},
			"actuacion coffee": {
				"element": "Datos generales"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"contratos coffee": {
				"element": "Contratos Asociados"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			}
		}
	},
	"contratos coffee": {
		"to": {
			"cuantias de actuacion coffee": {
				"element": "Recursos economicos"
			},
			"actuacion coffee": {
				"element": "Datos generales"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"contrato coffee": {
				"element": "Buscar y Primera Fila"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			}
		}
	},
	"cl@ve home": {
		"to": {
			"cl@ve login": {
				"element": "Cl@ve Permanente"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			}
		}
	},
	"coffee home": {
		"to": {
			"cl@ve home": {
				"element": "Identificarse con Cl@ve"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			}
		}
	},
	"home coffee identificado": {
		"to": {
			"Principal Coffee": {
				"element": "Mecanismo de Recuperación y Resiliencia"
			}
		}
	},
	"cl@ve login": {
		"to": {
			"home coffee identificado": {
				"element": "Humano"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			}
		}
	},
	"Principal Coffee": {
		"to": {
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"subproyectos instrumentales coffee": {
				"element": "Subproyectos instrumentales"
			},
			"subproyectos coffee": {
				"element": "Subproyectos"
			}
		}
	},
	"subproyectos instrumentales coffee": {
		"to": {
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subproyectos coffee": {
				"element": "Subproyectos"
			}
		}
	},
	"cuantias de subproyecto coffee": {
		"to": {
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subproyecto coffee": {
				"element": "Datos generales"
			},
			"subproyectos coffee": {
				"element": "Subproyectos"
			}
		}
	},
	"subproyecto coffee": {
		"to": {
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subproyectos coffee": {
				"element": "Subproyectos"
			}
		}
	},
	"subproyectos coffee": {
		"to": {
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subproyectos instrumentales coffee": {
				"element": "Subproyectos instrumentales"
			},
			"subproyecto coffee": {
				"element": "Buscar y Primera Fila"
			}
		}
	},
	"subvencion coffee": {
		"to": {
			"actuacion coffee": {
				"element": "Datos generales"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subvenciones coffee": {
				"element": "Subvenciones asociadas"
			}
		}
	},
	"subvenciones coffee": {
		"to": {
			"cuantias de actuacion coffee": {
				"element": "Recursos economicos"
			},
			"actuacion coffee": {
				"element": "Datos generales"
			},
			"actuaciones coffee": {
				"element": "Actuaciones"
			},
			"Principal Coffee": {
				"element": "CoFFEE"
			},
			"subvencion coffee": {
				"element": "Buscar y Primera Fila"
			}
		}
	}
}

	"""