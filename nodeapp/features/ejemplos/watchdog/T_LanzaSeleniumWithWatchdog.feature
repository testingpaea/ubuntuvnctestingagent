Feature: Arranca un Selenium manejando un Chromium-browser y el watchdog basico que muestra las peticiones recibidas

@ArrancaSeleniumWatchdog @ISSUE:TEST-16
  Scenario: Arranca un Selenium manejando un Chromium-browser y el watchdog basico que muestra las peticiones recibidas
	Given close all browsers
	Given in environment "PRE"
	Given launch browser to "https://google.com" maximized with performance watchdog every "5" seconds
  
	
