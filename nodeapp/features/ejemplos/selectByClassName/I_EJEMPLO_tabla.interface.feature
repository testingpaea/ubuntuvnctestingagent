Feature: Definicion del Interface Web de pruebas con una tabla y elementos con Class Name

@Interface @APP:PRUEBA
 Scenario: Definicion del Interface Web para pruebas de seleccion por Class Name
	Given add "selenium" interface "prueba ClassName" with 
	"""
{
    "testLoaded": {
        "element": "BODY",
        "testWay": "VISIBLE"
    },
    "elements": [
        {
            "id": "Tabla de Prueba",
            "selector": {
                "type": "xpath",
                "expression": "//*/table",
                "frameNumbers": []
            },
            "focusable": false,
            "caption": "",
            "HTMLtag": "table",
            "innerHTML": ""
        },{
            "id": "classTds",
            "selector": {
                "type": "css selector",
                "expression": ".twoClass",
                "frameNumbers": []
            },
            "focusable": false,
            "caption": "",
            "HTMLtag": "td",
            "innerHTML": ""
        },{
            "id": "BODY",
            "selector": {
                "type": "xpath",
                "expression": "//*/body",
                "frameNumbers": []
            },
            "focusable": false,
            "caption": "BODY"
        }
    ],
    "relatedScreens": [],
    "extendsScreen": ""
}
	"""