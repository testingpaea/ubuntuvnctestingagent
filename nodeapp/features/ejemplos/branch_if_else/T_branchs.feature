Feature: realiza dos IFs 

@ISSUE:TEST-01
  Scenario: realiza dos IFs anidados
	
	Given assign "(#( true )#)" to variable "if1Var"
	Given assign "(#( false )#)" to variable "if2Var"

	Given show trace "Starting the Test: if1Var= $if1Var if2Var= $if2Var "

	Given if "$if1Var !== $if2Var" then
	  Given show trace "IF 1 ... enter (#( $if1Var !== $if2Var )#)"
	  
		Given if "$if1Var===true" then
			Given show trace "IF 1.1 $if1Var===true"

		Given else if "$if2Var===true" then 
			Given show trace "IF 1.1 else if ($if2Var===true)"

		Given else
			Given show trace "IF 1.1 else "
		Given end if
		
		Given show trace "IF 1 other step "
	Given else if "$if2Var===false" then 
		Given show trace "IF 1 else if ($if2Var===false)"
	Given else
		Given show trace "IF 1 else"
	Given end if
	
	Given show trace "Out of IF 1"
	
