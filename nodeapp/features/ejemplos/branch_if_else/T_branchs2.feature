Feature: realiza varios ifs anidados 

@ISSUE:TEST-01-2
  Scenario: realiza IFs anidados con valores aleatorios
	
	Given assign "(#( Math.random()*100 )#)" to variable "if1Var"
	Given assign "(#( Math.random()*100 )#)" to variable "if2Var"

	Given show trace "Starting the Test: if1Var= $if1Var"
	Given show trace "Starting the Test: if2Var= $if2Var"

	Given if "$if1Var < 25 " then
		Given show trace "start -if1Var= $if1Var- - if1var <25"
		
		Given if "$if2Var < 25 " then
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <25"
		Given else if "$if2Var<50" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <50"
		Given else if "$if2Var<75" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <75"
		Given else 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <100"
		Given end if
	
		Given show trace "End -if1Var= $if1Var- - if1var <25"
	Given else if "$if1Var<50" then 
		Given show trace "start -if1Var= $if1Var- - if1var <50"
		Given if "$if2Var < 25 " then
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <25"
		Given else if "$if2Var<50" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <50"
		Given else if "$if2Var<75" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <75"
		Given else 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <100"
		Given end if
	
		Given show trace "End -if1Var= $if1Var- - if1var <50"

	Given else if "$if1Var<75" then 
		Given show trace "start -if1Var= $if1Var- - if1var <75"
		Given if "$if2Var < 25 " then
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <25"
		Given else if "$if2Var<50" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <50"
		Given else if "$if2Var<75" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <75"
		Given else 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <100"
		Given end if
	
		Given show trace "End -if1Var= $if1Var- - if1var <75"
	Given else 
		Given show trace "start -if1Var= $if1Var- - if1var <100"
		Given if "$if2Var < 25 " then
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <25"
		Given else if "$if2Var<50" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <50"
		Given else if "$if2Var<75" then 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <75"
		Given else 
			Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
		Given end if
	
		Given show trace "End -if1Var= $if1Var- - if1var <100"
	Given end if
	
	Given show trace "End all if structures"
	
