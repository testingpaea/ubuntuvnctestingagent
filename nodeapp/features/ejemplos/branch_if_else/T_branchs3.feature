Feature: realiza varios ifs y bucles

@ISSUE:TEST-01-3
  Scenario: realiza IFs anidados con valores aleatorios y bucles
	
	Given assign "(#( Math.random()*100 )#)" to variable "if1Var"
	Given assign "(#( Math.random()*100 )#)" to variable "if2Var"

	Given show trace "Starting the Test: if1Var= $if1Var"
	Given show trace "Starting the Test: if2Var= $if2Var"

	Given if "$if1Var < 25 " then
		Given show trace "start -if1Var= $if1Var- - if1var <25"
		
		Given set "(#( Math.trunc(Math.random()*5) )#)" to new variable "iLoops"
		Given set "(#( 0 )#)" to new variable "iLoop"
		Given loop while "$iLoop<$iLoops" do
		  Given show trace "-if1Var= $if1Var -- Into the loop $iLoop of $iLoops"

			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <100"
			Given end if
			
			Given set "(#( (parseInt($iLoop)+1)+'' )#)" to variable "iLoop"
		Given end loop
		
	
		Given show trace "End -if1Var= $if1Var- - if1var <25"
	Given else if "$if1Var<50" then 
		Given show trace "start -if1Var= $if1Var- - if1var <50"
		
		Given set "(#( Math.trunc(Math.random()*5) )#)" to new variable "iLoops"
		Given set "(#( 0 )#)" to new variable "iLoop"
		Given loop while "$iLoop<$iLoops" do
		  Given show trace "-if1Var= $if1Var -- Into the loop $iLoop of $iLoops"

			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <100"
			Given end if
			
			Given set "(#( (parseInt($iLoop)+1)+'' )#)" to variable "iLoop"
		Given end loop
		
	
		Given show trace "End -if1Var= $if1Var- - if1var <50"
	Given else if "$if1Var<75" then 
		Given show trace "start -if1Var= $if1Var- - if1var <75"

		Given set "(#( Math.trunc(Math.random()*5) )#)" to new variable "iLoops"
		Given set "(#( 0 )#)" to new variable "iLoop"
		Given loop while "$iLoop<$iLoops" do
		  Given show trace "-if1Var= $if1Var -- Into the loop $iLoop of $iLoops"

			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <25"
				Given loop for var "loopNumerico" from 1 to 5 do
					Given show trace "Prueba loop numerico.... vuelta $loopNumerico, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop
			
			Given else if "$if2Var<50" then 		
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <50"
				Given loop for var "loopNumerico" from 1 to 5 do
					Given show trace "Prueba loop numerico.... vuelta $loopNumerico, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop
				
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <75"
				Given loop for var "loopNumerico" from 1 to 5 do
					Given show trace "Prueba loop numerico.... vuelta $loopNumerico, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <100"
				Given loop for var "loopNumerico" from 1 to 5 do
					Given show trace "Prueba loop numerico.... vuelta $loopNumerico, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop
			Given end if
			
			Given set "(#( (parseInt($iLoop)+1)+'' )#)" to variable "iLoop"
		Given end loop
		
	
		Given show trace "End -if1Var= $if1Var- - if1var <75"
	Given else 
		Given show trace "start -if1Var= $if1Var- - if1var <100"
		Given set "(#( ['A1','A2','A3','A4','A5','A6','A7','A8','A9','A10'] )#)" to new variable "arrAs"

		Given set "(#( Math.trunc(Math.random()*5) )#)" to new variable "iLoops"
		Given set "(#( 0 )#)" to new variable "iLoop"
		Given loop while "$iLoop<$iLoops" do
		  Given show trace "-if1Var= $if1Var -- Into the loop $iLoop of $iLoops"

			Given if "$if2Var < 25 " then
				Given show trace "Start-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
				Given loop for each element "arrElement" in "$arrAs" do
					Given show trace "Prueba loop array elements.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop  	
			
				Given show trace "End -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "Start -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
				Given loop for each element "arrElement" in "$arrAs" do
					Given show trace "Prueba loop array elements.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop  	
				Given show trace "End -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "Start -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
				Given loop for each element "arrElement" in "$arrAs" do
					Given show trace "Prueba loop array elements.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop  	
				Given show trace "End -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <75"
			Given else 
				Given show trace "Start -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
				Given loop for each element "arrElement" in "$arrAs" do
					Given show trace "Prueba loop array elements.... vuelta $arrElement, $LoopCycleIndex -- $LoopCycleElement"
				Given end loop  	
				Given show trace "End -if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
			Given end if
			Given set "(#( (parseInt($iLoop)+1)+'' )#)" to variable "iLoop"
		Given end loop
		
		Given show trace "End -if1Var= $if1Var- - if1var <100"
	Given end if
	
	Given show trace "End all if structures"
