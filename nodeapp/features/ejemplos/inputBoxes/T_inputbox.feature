Feature: muestra dos ventanas de entrada de datos

@ISSUE:TEST-05
  Scenario: muestra dos ventanas de entrada de datos
  	Given assign "" to variable "tmpPIN"
  	Given assign "" to variable "tmpUSER"

	Given user input window "Introduzca Nombre de Usuario" to variable "tmpUSER"
	Given user input password window "Introduzca PIN de la tarjeta" to variable "tmpPIN"
	Given show progress box "Usuario: $tmpUSER Password: $tmpPIN"
	Given wait 30 seconds
	Given close progress box
