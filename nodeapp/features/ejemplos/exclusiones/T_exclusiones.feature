Feature: Abre una seccion critica, espera 30 segundos y la vuelve a cerrar

@ISSUE:TEST-03
@PARAMS:[{"name":"waitTime","description":"Tiempo","defaultValue":"30"}]
@PARAMS:[{"name":"messageText","description":"Texto","defaultValue":"Simulando30"}]
Scenario: Abre una seccion critica, espera N segundos y la vuelve a cerrar
	Given start critical section
	Given debugger
	Given wait 2 seconds
	Given show progress box "$messageText"
	Given wait "$waitTime" seconds
	Given close progress box
	Given wait 2 seconds
	Given end critical section
