Feature: Hace que se muestre una ventana en otro agente

@ISSUE:TEST-04
@PARAMS:[{"name":"waitTime","description":"Tiempo","defaultValue":"30"}]
@PARAMS:[{"name":"messageText","description":"Texto","defaultValue":"VentanaProgresoEjemplo"}]
@PARAMS:[{"name":"agentSelector","description":"SeleccionAgente","defaultValue":""}]
Scenario: Hace que se muestre una ventana en otro agente
	Given set to new variable "theAgentSelector" multiline text
		"""
			return (
				agentProperties.has("solicitante")
				&&
				(
					(agentProperties.get("solicitante").value==true)
					||
					(agentProperties.get("solicitante")==true)
					||
					(agentProperties.get("solicitante")=="true")
				)
			);
		"""
	Given the scenario "muestra ventana de progreso y la cierra un número de segundos despues" using another agent where 
		"""
		{
			"CALL_INFO":{
				"environment":"PRE",
				"agentSelectorJSLines":"$theAgentSelector",
				"agentSelectorID":"Solicitante"
			},
			"PARAMS":{
				"waitTime":"$waitTime",
				"messageText":"$messageText",
				"name":"One Name"
			}
		} 
		"""
	Given show progress box "Escenario en otro agente finalizado Finalizado"
	Given wait "$waitTime" seconds
	Given close progress box
