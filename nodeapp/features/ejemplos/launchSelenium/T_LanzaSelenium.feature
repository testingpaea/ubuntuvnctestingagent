Feature: Arranca un Selenium manejando un Chromium-browser

@ArrancaSelenium @ISSUE:TEST-06
  Scenario: Arranca un Selenium manejando un Chromium-browser
	Given close all browsers
	Given in environment "PRE"
	Given launch browser to "https://google.com" maximized
  Given send keys "ctrl+shift+i"
  Given browser set headers '[{"pruebaheader":"valorpruebaheader"}]'
  Given browser fetch "GET" url "https://www.google.com/doodles/json/2023/10?hl=es"
	
