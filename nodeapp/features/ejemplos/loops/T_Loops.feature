Feature: realiza dos loops diferentes

@ISSUE:TEST-07
Scenario: realiza dos loops diferentes para mostrar el funcionamiento
	Given in app "PRUEBA"
  Given show trace "Probando Loops" 
  
  Given loop for var "loopNumericoSinEjecucion" from 1 to 0 do
			Given show trace "Prueba loop numerico sin Ejecucion.... vuelta: $loopNumerico, indice:$LoopCycleIndex -- elemento:$LoopCycleElement" 
	Given end loop
  	
	Given set "(#( ['A1','A2','A3','A4','A5','A6','A7','A8','A9','A10'] )#)" to new variable "arrAs"
	
	Given loop for each element "arrElement" in "$arrAs" do
		Given assign "(#( Math.random()*100 )#)" to variable "if1Var"
		Given assign "(#( Math.random()*100 )#)" to variable "if2Var"
		Given show trace "Inicio if1Var:$if1Var - if2Var:$if2Var Prueba dentro del While con ifs.... vuelta: $arrElement, indice: $LoopCycleIndex -- elemento: $LoopCycleElement"
		Given if "$if1Var < 25 " then
			Given show trace "start -if1Var= $if1Var- - if1var <25"
			
			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <25 -> if2var <100"
			Given end if
		
			Given show trace "End -if1Var= $if1Var- - if1var <25"
			
		Given else if "$if1Var<50" then 
			Given show trace "start -if1Var= $if1Var- - if1var <50"
			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <50 -> if2var <100"
			Given end if
		
			Given show trace "End -if1Var= $if1Var- - if1var <50"
	
		Given else if "$if1Var<75" then 
			Given show trace "start -if1Var= $if1Var- - if1var <75"
			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <75 -> if2var <100"
			Given end if
		
			Given show trace "End -if1Var= $if1Var- - if1var <75"
		Given else 
			Given show trace "start -if1Var= $if1Var- - if1var <100"
			Given if "$if2Var < 25 " then
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <25"
			Given else if "$if2Var<50" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <50"
			Given else if "$if2Var<75" then 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <75"
			Given else 
				Given show trace "-if1Var= $if1Var -- if2Var= $if2Var- : if1var <100 -> if2var <100"
			Given end if
		
			Given show trace "End -if1Var= $if1Var- - if1var <100"
		Given end if
		 
		Given show trace "Fin Prueba dentro del While con ifs.... vuelta: $arrElement, indice: $LoopCycleIndex -- elemento: $LoopCycleElement"
	Given end loop  	

	
	Given loop for each element "arrElement" in "$arrAs" do
		Given debugger
		Given show trace "Prueba dentro del While con anidamiento.... vuelta: $arrElement, indice: $LoopCycleIndex -- elemento: $LoopCycleElement"
		
		Given loop for var "loopNumerico" from 8 to 13 do
			Given show trace "Prueba loop numerico anidado.... vuelta: $loopNumerico, indice:$LoopCycleIndex -- elemento:$LoopCycleElement" 
		Given end loop
		
		Given show trace "Fin Prueba dentro del While con anidamiento.... vuelta: $arrElement, indice: $LoopCycleIndex -- elemento: $LoopCycleElement"
	Given end loop  	
	
	Given loop for each element "arrElement" in "$arrAs" do
		Given debugger
		Given show trace "Prueba dentro del While.... vuelta: $arrElement, indice: $LoopCycleIndex -- elemento: $LoopCycleElement"
		 
	Given end loop  	

	Given set "3" to new variable "vIni"
	Given set "7" to new variable "vEnd"

	
	Given loop for var "loopConTextos" from "$vIni" to "$vEnd" do
		Given show trace "Prueba loop con textos.... vuelta:$loopConTextos, indice: $LoopCycleIndex -- elemento: $LoopCycleElement" 
	Given end loop

	
	Given loop for var "loopNumerico" from 8 to 13 do
		Given show trace "Prueba loop numerico.... vuelta: $loopNumerico, indice:$LoopCycleIndex -- elemento:$LoopCycleElement" 
	Given end loop

	Given set "0" to new variable "iFirmante"
	Given loop while "(#( parseInt($iFirmante)<3 )#)" do
		Given debugger
		Given show trace "Prueba dentro del While.... vuelta: $iFirmante" 
		Given set "(#( (parseInt($iFirmante)+1)+'' )#)" to variable "iFirmante"
	Given end loop
	
	
	
	
