Feature: Subescenario de ejemplo recursivo con loop e if

@subScenario @APP:PRUEBA @ISSUE:TEST-07-01-01
@PARAMS:[{"name":"deepMax","description":"profundidad_maxima","defaultValue":"3"}]
@PARAMS:[{"name":"deepAct","description":"profundidad_actual","defaultValue":"0"}]

 Scenario: Subescenario de ejemplo recursivo con loop e if
  Given show trace "$deepAct -- Inicio Recursivo $deepMax -> $deepAct" indented 
	Given if "$deepAct < $deepMax" then 
		Given show trace "$deepAct -- start do outer if" indented
		Given set "(#( ['A1','A2','A3'] )#)" to new variable "arrAs"
		Given loop for each element "arrElement" in "$arrAs" do
			Given set "(#( Math.round(Math.random()*3) )#)" to new variable "if1Var"
			Given set "(#( Math.random()*100 )#)" to new variable "if2Var"
			Given show trace "$deepAct -- start do outer loops -- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented
	
			Given if "$if2Var < 25 " then
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <25 do recursive call" indented
				Given the scenario "Subescenario de ejemplo recursivo con loop e if" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <25 end of recursive call" indented
			Given else if "$if2Var<50" then 
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do $if1Var loops" indented
				Given loop for var "loopNumerico" from "0" to "$if1Var" do
					Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- Start: $LoopCycleIndex " indented
					Given the scenario "Subescenario de ejemplo recursivo con loop e if" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
					Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- End: $LoopCycleIndex " indented
				Given end loop
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50 end of loops" indented
			Given else 
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <100   do Nothing!" indented
			Given end if
			Given show trace "$deepAct -- end do outer loops -- End $LoopCycleIndex " indented
		Given end loop
		Given show trace "$deepAct -- end do outer if" indented
	Given end if
  Given show trace "$deepAct -- Fin recursivo $deepMax -> $deepAct" indented
	