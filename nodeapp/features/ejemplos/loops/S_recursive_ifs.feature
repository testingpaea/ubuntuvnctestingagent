Feature: Subescenario de ejemplo recursivo con loop e if

@subScenario @APP:PRUEBA @ISSUE:TEST-07-01-02
@PARAMS:[{"name":"deepMax","description":"profundidad_maxima","defaultValue":"3"}]
@PARAMS:[{"name":"deepAct","description":"profundidad_actual","defaultValue":"0"}]

 Scenario: Subescenario de ejemplo recursivo con solo ifs
  Given show trace "$deepAct -- Inicio Recursivo $deepMax -> $deepAct" indented 
	Given if "$deepAct < $deepMax" then 
		Given show trace "$deepAct -- start do outer if" indented
		Given set "(#( Math.round(Math.random()*3) )#)" to new variable "if1Var"
		Given set "(#( Math.random()*100 )#)" to new variable "if2Var"
		Given show trace "$deepAct -- start do inner ifs - if1Var:$if1Var -- if2Var:$if2Var" indented
		Given if "$if2Var < 25 " then
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <25 start do recursive call" indented
			Given the scenario "Subescenario de ejemplo recursivo con solo ifs" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <25 end of recursive call" indented
		Given else if "$if2Var<50" then 
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50 start do second if" indented
				Given the scenario "Subescenario de ejemplo recursivo con solo ifs" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50 end do second if" indented
		Given else 
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <100 start do third if" indented
				Given the scenario "Subescenario de ejemplo recursivo con solo ifs" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
			Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <100 end do third if" indented
		Given end if
		Given show trace "$deepAct -- end do inner ifs - if1Var:$if1Var -- if2Var:$if2Var" indented
		Given show trace "$deepAct -- end do outer if" indented
	Given end if
  Given show trace "$deepAct -- Fin recursivo $deepMax -> $deepAct" indented
	