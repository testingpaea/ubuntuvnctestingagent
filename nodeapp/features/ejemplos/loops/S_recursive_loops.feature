Feature: Subescenario de ejemplo recursivo con loop e if

@subScenario @APP:PRUEBA @ISSUE:TEST-07-01-03
@PARAMS:[{"name":"deepMax","description":"profundidad_maxima","defaultValue":"3"}]
@PARAMS:[{"name":"deepAct","description":"profundidad_actual","defaultValue":"0"}]

 Scenario: Subescenario de ejemplo recursivo solo con loops
  Given show trace "$deepAct -- Inicio Recursivo $deepMax -> $deepAct" indented 
	Given if "$deepAct < $deepMax" then 
		Given show trace "$deepAct -- start do outer if" indented
		Given set "(#( ['A1','A2','A3'] )#)" to new variable "arrAs"
		Given loop for each element "arrElement" in "$arrAs" do
			Given set "(#( Math.round(Math.random()*3) )#)" to new variable "if1Var"
			Given set "(#( Math.random()*100 )#)" to new variable "if2Var"
			Given show trace "$deepAct -- start do outer loops -- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented
			
			Given show trace "$deepAct -- start inner loop (First Loop)-- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented
			Given loop for var "loopNumerico" from "0" to "$if1Var" do
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- Start: $LoopCycleIndex " indented
				Given the scenario "Subescenario de ejemplo recursivo solo con loops" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- End: $LoopCycleIndex " indented
			Given end loop
			Given show trace "$deepAct -- End inner loop (First Loop)-- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented

			Given show trace "$deepAct -- start inner loop (Second Loop)-- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented
			Given loop for var "loopNumerico" from "0" to "$if1Var" do
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- Start: $LoopCycleIndex " indented
				Given the scenario "Subescenario de ejemplo recursivo solo con loops" where variables '{"$deepMax":"$deepMax","$deepAct":"(#( $deepAct+1 )#)"}'
				Given show trace "$deepAct -- if2Var= $if2Var- : -> if2var <50  do: $if1Var loops -- End: $LoopCycleIndex " indented
			Given end loop
			Given show trace "$deepAct -- End inner loop (Second Loop)-- Start $LoopCycleIndex -- if1Var:$if1Var -- if2Var:$if2Var" indented


			Given show trace "$deepAct -- end do outer loops -- End $LoopCycleIndex " indented
		Given end loop
		Given show trace "$deepAct -- end do outer if" indented
	Given end if
  Given show trace "$deepAct -- Fin recursivo $deepMax -> $deepAct" indented
	