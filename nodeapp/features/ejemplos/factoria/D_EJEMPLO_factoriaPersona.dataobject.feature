Feature: Definicion de un objeto persistente para almacenar en base de datos

@DataObject
Scenario: Definicion de una factoria de objetos persistentes para almacenar en base de datos
	Given add persistent storable object factory "Persona" with this sample
	"""
	{
	"nombre":"",
	"apellido":"",
	"Dates":{
		"init":"",
		"end":""
		},
	 "persistence":{
	 		"provider": "rpaCoffee_mysql",
	 		"storename":"PERSONA",
	 		"attrsUniqueId":["id"],
	 		"attrsAlias":[],
	 		"attrsExcluded":["Dates"]
	 }
	}
	"""
