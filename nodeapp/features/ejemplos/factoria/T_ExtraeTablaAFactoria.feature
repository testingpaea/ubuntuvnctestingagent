Feature: Extrae todas las filas de una tabla utilizando una factoria

@PRUEBA @ISSUE:TEST-12
Scenario: Extrae todas las filas de una tabla utilizando una factoria
	Given close all browsers
	Given in app "PRUEBA" 
  Given launch browser to "http://192.168.1.120:18080/ejemplo_tabla.html" maximized
  Given show progress box "Esperando que cargue" for "3" seconds
  
	Given wait for load of interface "prueba tabla"
	Given get all table values of "Tabla de Prueba"
	Given assign returned value to "tblAux"
	
  Given show progress box "Crear Objetos" for "3" seconds
	Given use table "tblAux" to create objects "Persona" filling attributes "id,Contact,Country"
	
	Given assign to variable "nFilas" the number of stored objects "DatosFila"
	Given assign to variable "arrIDs" the list of IDs of stored objects "DatosFila"

  Given show progress box "Extraer attributos" for "20" seconds
	Given load from interface the attributes of object "Persona" wich id is "Alfreds Futterkiste"
	
		

 
