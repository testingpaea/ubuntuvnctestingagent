const ScStore = require('kimbo/scenarioStore');

//const fncGet=require('./run.steps.js').getExecuteFeatures;
//Defining more steps (has to be in a .stepdef.js file)
const stepDefinitions = [
  {
    stepMethod: 'Given',
    stepPattern: 'acceder a {string}',
    stepTimeout: 40000,
    stepFunction: async function(appId){
		console.log("Acceder a "+appId);
		ScStore.getApplications().push(appId);
		console.log("Updated app "+appId);
		await ScStore.indirectScenarioCall("acceder","");
        return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'acceder al {string} de {string}',
    stepTimeout: 40000,
    stepFunction: async function(inModuleName,inAppId){
		var appId=await ScStore.processExpression(inAppId);
		var moduleName=await ScStore.processExpression(inModuleName);
		console.log("Acceder a "+appId+" modulo:"+moduleName);
		ScStore.getApplications().push(appId,{name:"@MODULE",value:moduleName});
		console.log("Updated app "+appId);
		await ScStore.indirectScenarioCall("acceder","");
		return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'acceder al modulo {string} de {string}',
    stepTimeout: 40000,
    stepFunction: async function(inModuleName,inAppId){
		var appId=await ScStore.processExpression(inAppId);
		var moduleName=await ScStore.processExpression(inModuleName);
		console.log("Acceder a "+appId+" modulo:"+moduleName);
		ScStore.getApplications().push(appId,{name:"@MODULE",value:moduleName});
		console.log("Updated app "+appId);
		await ScStore.indirectScenarioCall("acceder","");
		return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: '{string} accede al {string} de {string}',
    stepTimeout: 40000,
    stepFunction: async function(user,moduleName,appId){
		console.log("el usuario "+user+" accede a "+appId+" modulo:"+moduleName);
		await ScStore.indirectScenarioCall("acceder aplicacion,modulo,usuario",{"$usuario":user,"$modulo":moduleName,"$aplicacion":appId});
		return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: '{string} accede al modulo {string} de {string}',
    stepTimeout: 40000,
    stepFunction: async function(user,moduleName,appId){
		console.log("el usuario "+user+" accede a "+appId+" modulo:"+moduleName);
		await ScStore.indirectScenarioCall("acceder aplicacion,modulo,usuario",{"$usuario":user,"$modulo":moduleName,"$aplicacion":appId});
		return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'User input {string} to variable {string}',
    stepTimeout: 40000,
    stepFunction: async function(qText,tgtVariableName){
	  const {question}=require('../../../helpers/basicFunctions.lib');
	  var qResult=await question(qText);
      console.log("["+qResult+"]");
		var tgtVarName="$"+tgtVariableName;
		var upperClosure=ScStore.getUpperClosure();
		if (!ScStore.existsVar(tgtVarName)){
			upperClosure.addVar(tgtVarName,qResult);
		} else {
			upperClosure.setVar(tgtVarName,qResult);
		}
	  
	}
  }
];

module.exports = stepDefinitions;
