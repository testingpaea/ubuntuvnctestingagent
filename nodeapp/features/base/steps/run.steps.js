//Enabling the package (this has to be in a .steps.js file)
var {After, Before,setDefinitionFunctionWrapper,AfterAll,BeforeAll} = require("@cucumber/cucumber");	
var arity=require('util-arity');
const messageHandler=require('../../../messages/messageHandler');
const OtherWorld = require('kimbo/OtherWorld');
const ScStore = require('kimbo/scenarioStore');
const enableScenarioCalling = require('kimbo');
const fs = require('fs')
debugger;
ScStore.arrExtensions=["applications"
                           ,"selenium"
                           ,"selenium/browserJS"
                           ,"watchdog"
						   ,"xdotool"
						   ,"interfaces"
						   ,"interfaces/navigation"
						   ,"selected"
						   ,"multiscreen"
						   ,"events"
						   ,"persist"
						   ,"providers/database/sqldb/mysql"
						   ,"providers/database/nosqldb/mongodb"
						   ,"smartcard"
						   ,"exclusions"
						   ,"files"
						   ,"xlsx"
						   ,{name:"users",path:"../../extensions/users"}
						   ,{name:"agentSelectors",path:"../../extensions/agentSelectors"}
						   //,{name:"paralleltesting",path:"../../features/extensions/paralleltesting"}
						   ]
debugger;					   
const path = '/usr/src/app/features/kimboapp/extensions/customExtensions.json';
try {
  if (fs.existsSync(path)) {
	var arrExtensions=require(path);
	for (let auxExtension of arrExtensions){
		ScStore.arrExtensions.push(auxExtension);
	}
  }
} catch(err) {
  console.error(err)
}
						   

enableScenarioCalling(true,ScStore.arrExtensions);
//ScStore.getSelenium().newInterfacesSavePath="/usr/src/agente/ExportedInterfaces";
ScStore.getSelenium().newInterfacesSavePath="/usr/src/app/ExportedInterfaces";
ScStore.persistExecutionNodes={objectType:"Persist execution traces of agent"};

messageHandler.setIdentification("Cucumber","Cucumber Instance");
messageHandler.getTestList= async function getTestList(){
	return this.sendMessage("GET TEST LIST");
}
messageHandler.getNextScenario=async function getNextScenario(){
	return this.sendMessage("GET NEXT SCENARIO");
}
messageHandler.getScreensInfo=async function getScreensInfo(){
	return this.sendMessage("GET SCREENS INFO");
}
messageHandler.lockSystem=async function lockSystem(){
	return await this.sendMessage("LOCK SYSTEM");
}
messageHandler.unlockSystem=async function unlockSystem(){
	return await this.sendMessage("UNLOCK SYSTEM");
}
messageHandler.finishedAll=async function finishedAll(bWasCompiling,scenarios,results){
	console.log("FINISHED ALL MESSAGE");
	var bCompileFase=false;
	debugger;
	if ((typeof bWasCompiling!=="undefined")&&(bWasCompiling)){
		bCompileFase=true;
		console.log("checking if vnc is loaded");
		var hasVNC=ScStore.ensureVNC();
		console.log("vnc loaded:"+hasVNC);
	}
//		var results=ScStore.executionTree;
	return this.sendMessage("FINISHED",{endCompilation:bCompileFase,scenarios:scenarios,results:results},false);
}
messageHandler.addEventHandler("NEXT SCENARIO",async function(testInfo){
	debugger;
	console.log("Run the scenario:"+JSON.stringify(testInfo));
	var selector=ScStore.getSelected();
	selector.refresh();
	//using key of the test to locate scenario
	if (!(typeof testInfo.key!=="undefined")){
		testInfo.key=testInfo.id;
	}
	var srcScenario=selector.mapScenariosByTag.get(testInfo.key);
	var scenario=srcScenario.clone();
	scenario.executionId=testInfo.executionId;
	scenario.planExecutionId=testInfo.planExecutionId;
	
	
	console.log("SCENARIO:"+JSON.stringify(scenario));
	selector.addScenario(testInfo.key);
	selector.dynActualScenario=scenario;
	while (ScStore.getClosures().closures.length>1){
		ScStore.getClosures().closures.pop();
	}
	while (ScStore.getClosures().runtimeClosures.length>1){
		ScStore.getClosures().runtimeClosures.pop();
	}
	ScStore.actExecutionNode="";
	ScStore.executionTree=[];
	var closures=ScStore.getClosures();
	closures.newClosure("Scenario Parameters");
	if (typeof testInfo.params!=="undefined"){
		for (const param of testInfo.params){
			closures.addVar(param.name,param.value);
		};
	}
	if (typeof testInfo.environment!=="undefined"){
		closures.addVar("environment",testInfo.environment);
	}

	if (typeof testInfo.params==="undefined"){
		testInfo.params=[];
	}
	testInfo.params.push({name:"executionId",value:testInfo.executionId});
	testInfo.params.push({name:"planExecutionId",value:testInfo.planExecutionId});

	//ScStore.newExecutionNode(testInfo.key,testInfo.params);
	ScStore.indirectScenarioCall(scenario);
	messageHandler.finishProcessingActualMessage();
});

messageHandler.addEventHandler("SET AGENT NAME",async function(agentProps){
	debugger;
	var nodeProps=agentProps.properties;
	ScStore.agentName=agentProps.name;
	for (prop of nodeProps){
		var vKey=prop[0];
		var vValue=prop[1];
		ScStore.agentProperties.set(vKey,vValue);
	}
	messageHandler.finishProcessingActualMessage();
});
messageHandler.addEventHandler("FEEDBACK",async function(paramsReceived){
	debugger;
	console.log(JSON.stringify(paramsReceived));
	ScStore.getXdotool().writeText(paramsReceived);
	ScStore.getXdotool().sendKeys("Return");
	messageHandler.finishProcessingActualMessage();
});
messageHandler.addEventHandler("DEFERRED SCENARIO FINISHED",async function(paramsReceived){
	debugger;
	console.log(JSON.stringify(paramsReceived));
	var executionId=paramsReceived.executionId;
	var executionResult=paramsReceived.result;
	var agentSelectors=ScStore.getAgentSelectors();
	var scenarioInfo="";
	if (agentSelectors.waitingDeferred.has(executionId)){
		scenarioInfo=agentSelectors.waitingDeferred.get(executionId);
	} else if (agentSelectors.waitingDeferred.has("")){
		scenarioInfo=agentSelectors.waitingDeferred.get("");
	}
	if (scenarioInfo!==""){
		scenarioInfo.onFinish(executionResult);
	} else {
		debugger;
		console.log("Error.... the execution "+executionId+" was not waitingDeferred");
	}
	messageHandler.finishProcessingActualMessage();
});
messageHandler.addEventHandler("TAKE SCREENSHOT",async function(){
	debugger;
	ScStore.getMultiscreen().screenshot();
	messageHandler.finishProcessingActualMessage();
});

messageHandler.addEventHandler("TRACE EXECUTION TREE",async function(){
	debugger;
	ScStore.traceExecutionTree();
	messageHandler.finishProcessingActualMessage();
});


messageHandler.addEventHandler("SAVE SELENIUM INTERFACE",async function(params){
	debugger;
	var fileName=params.filename;
	await ScStore.getSelenium().listInteractiveElements(fileName);
	messageHandler.finishProcessingActualMessage();
});


ScStore.getEvents().addEvent("finishedAll",async function(finishParams){
	var bWasCompiling=finishParams.compiling;
	var scenarios=finishParams.scenarios;
	var results=finishParams.results;
	await messageHandler.finishedAll(bWasCompiling,scenarios,results);
});

ScStore.getEvents().addEvent("finished",async function(scenarioTag){
	var selector=ScStore.getSelected();
	var auxScnTag=scenarioTag;
	if (Array.isArray(auxScnTag)){
		auxScnTag=auxScnTag[0];
	}
	if ((selector.dynamic)&&(auxScnTag==selector.dynActualScenario.id)){
		console.log("FINISHED DYNAMIC SCENARIO:"+selector.dynActualScenario);
		debugger;
		await messageHandler.finishedAll(false,[],[ScStore.getExecutionInfo()]);
		setTimeout(function(){
			messageHandler.getNextScenario();
		},2000);
	}
//	await messageHandler.finished(bWasCompiling);
});
ScStore.getEvents().addEvent("exclusion_start",async function(){
	if (typeof process.send==="function"){
		var callResult=await messageHandler.lockSystem();
	}
});
ScStore.getEvents().addEvent("exclusion_end",async function(){
	if (typeof process.send==="function"){
		var callResult=await messageHandler.unlockSystem();
	}
});
ScStore.getEvents().addEvent("progressBox",async function(oParams){
	messageHandler.sendMessage("PROGRESSBOX",oParams,false);
});
ScStore.getEvents().addEvent("inputBox",async function(oParams){
	messageHandler.sendMessage("INPUTBOX",oParams,false);
});


ScStore.getEvents().addEvent("getCredentials",async function(objCredentialNeeded){
	return await messageHandler.sendMessage("GET CREDENTIALS",objCredentialNeeded,true);
});

ScStore.getEvents().addEvent("Run deferred scenario",async function(objDeferredScenarioInfo){
	console.log("Run Deferred. Sending message to agent");
	var result=await messageHandler.sendMessage("RUN DEFERRED SCENARIO",objDeferredScenarioInfo,true);
	console.log("Run Deferred. Message processed:"+JSON.stringify(result));
	return result;
});
ScStore.getEvents().addEvent("screenshot",async function(oScreenshot){
	debugger;
	console.log("Sending Screenshot. ");
	var result=await messageHandler.sendMessage("SCREENSHOT",oScreenshot,true);
	console.log("Sending Screenshot. Message processed:"+JSON.stringify(result));
	return result;
});


console.log("Eventes Enabled:"+ScStore.getEvents().enabled);
if (ScStore.getEvents().enabled){
	// Asynchronous Callback
	BeforeAll({ timeout: 30000}, function (callback) {
		console.log("Waiting debuggers");
		setTimeout(function(){
	  console.log("BEFORE ALL");
	  // perform some shared setup
	  var fncGetList=async function (){
		var callResult=await messageHandler.getTestList();
		console.log("callResult:"+JSON.stringify(callResult));
		var selScenarios=ScStore.getSelected();
		selScenarios.setLists(callResult.value,["@User"]);
		selScenarios.setTagSelector("@ISSUE");
		console.log("ssTests:"+JSON.stringify(selScenarios));
	  }
	  var fncGetScreensInfo=async function(callback){
		var callResult=await messageHandler.getScreensInfo();
		console.log("callResult:"+JSON.stringify(callResult));
		var screenManager=ScStore.getMultiscreen();
		screenManager.setTotalScreens(callResult.value.totalScreens);
		screenManager.setActualScreen(callResult.value.actualScreen);
		console.log("screen Info:"+JSON.stringify(screenManager));
		debugger;
		if (screenManager.totalScreens>0){
			screenManager.enabled=true;
		} else {
			screenManager.enabled=false;
		}
	  }
	  var fncGetRunningParams=async function(){
		  await fncGetList();
		  await fncGetScreensInfo();
		  callback();
		  console.log("END BEFORE ALL");
		  callback();
	  }
	  fncGetRunningParams();
	  // execute the callback (optionally passing an error when done)
		},10000);
	});
}

ScStore.setNotRootUserTag("@User","User");
ScStore.setNotRootUserTag("@AgentSelector","AgentSelector");


/*
var {getTestCases,getTestCasesFromFilesystem} = require('cucumber');	


setTimeout(async function(){
	debugger;
	if (typeof getTestCases!=="undefined"){
		console.log("exists getTestCases");
	}
	if (typeof getTestCasesFromFilesystem!=="undefined"){
		console.log("exists getTestCasesFromFilesystem");
	}
	var {getTestCases,getTestCasesFromFilesystem} = require('cucumber');	
	if (typeof getTestCases!=="undefined"){
		console.log("exists getTestCases");
	}
	if (typeof getTestCasesFromFilesystem!=="undefined"){
		console.log("exists getTestCasesFromFilesystem");
	}
	var _gherkin = require('gherkin');
    var events = _gherkin.generateEvents(`
		Feature: Definición de la funcion de seleccion de un agente REGISTRADOR

		@AgentSelector
		 Scenario: Definición de la funcion de seleccion de un agente REGISTRADOR
			Given add agent selector "Registrador" with
			"""
				return (
					agentProperties.has("registrador")
					&&
					(
						(agentProperties.get("registrador").value==true)
						||
						(agentProperties.get("registrador").value=="true")
						||
						(agentProperties.get("registrador")==true)
						||
						(agentProperties.get("registrador")=="true")
					)
				);
			"""		
		`, '/dynamic1.feature', {}, undefined);

//	var _gherkin2 = _interopRequireDefault(_gherkin);
	
	var result=await getTestCases();
},60000);
*/
//YOU CAN ADD TRADITIONAL STEP DEFINITIONS HERE...

/*
var sharedObject={
		name:"Shared Object over all steps and scenaros",
		update:function(attrName,attrValue){
			this[attrName]=attrValue;
			this.log();
		},
		log:function(){
			console.log(JSON.stringify(this));
		}
	};
Before(function (testCase,callback) {
  console.log("setting sharedObject to test:"+testCase.name+" .. its compiling:"+getCompiling());
  var world = this;
  world.RCG_sharedObject=sharedObject;
  callback();
});
*/
/*

Before("@subScenario",function (theScenario,callback) {
	console.log("Before Execution of subscenario test case:"+theScenario.pickle.name + " will be skipped:"+getCompiling());
	console.log(JSON.stringify(theScenario));
	if (getCompiling()){
		console.log("Skipped");
		return callback(false,'skipped');
	} else {
		console.log("Not skipped");
		return callback();
	}
});
After("@subScenario",function (theScenario,callback) {
	console.log("After Execution of subscenario test case:"+theScenario.pickle.name + " will be skipped:"+getCompiling());
	return callback();
});
*/



/*
setDefinitionFunctionWrapper(function (fn,options={}) {
  console.log("========================================");
  console.log("========================================");
  console.log("========================================");
  console.log("Start function wrapper");
  console.log("function:"+fn);
  var fnSource=""+fn;
  console.log("length:"+fn.length);
  console.log("function type:"+typeof fn);
  console.log("function name:"+fn.name);
  console.log("options:"+JSON.stringify(options));
  var fncName=fn.name;
  if (fn.length==2) {
	  return function(a,b){
		  console.log("-----------START WRAPPED EXECUTION---------");
		  console.log("Wrapped step arguments:"+JSON.stringify(arguments));
		  console.log("Wrapped code:"+fnSource);
		  var previousResult=fn(a,b);
		  console.log("Wrapped result:"+JSON.stringify(previousResult));
		  console.log("-----------END WRAPPED EXECUTION---------");
		  return previousResult;
	  };
  } else {
	  return fn;
  }  
});
*/