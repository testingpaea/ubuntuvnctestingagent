Feature: Definicion de un objeto para almacenar los datos de los procesos del agente

@DataObject 
Scenario: Definicion de un objeto para almacenar los datos de los procesos del agente
	Given add persistent storable object factory "Persist execution traces of agent" with this sample 
	"""
	{
	 "persistence":{
	 		"provider": "testingAgent_mongodb",
	 		"storename":"traces",
	 		"attrsUniqueId":["id"],
	 		"attrsAlias":[],
	 		"attrsExcluded":["ScStore","childs","parent","params","state"],
	 		"attrsAuto":{"SESSION_ID":"$RPA_SESSION_ID"}
	 			 		
		}
	}
	"""
	