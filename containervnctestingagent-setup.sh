#!/bin/bash
echo user passed $withUser - password $withPassword
export ENV_USER=${withUser:-sae}
export ENV_PASSWORD=${withPassword:-$ENV_USER}
export ENV_WITHDEBUG=${withDebug:-""}
export NUM_AGENTS=${withNumAgents:-4}
export APP_GITSERVER=${kimboappServer:-"https://saedga@bitbucket.org/testingpaea"}
export APP_NAME=${kimboappName:-kimboapp}


/usr/bin/containervncnodejs-setup.sh

export archType=$(dpkg --print-architecture)
if [ "amd64" = "$archType" ]; then
	cp /usr/src/app/package_amd64.json /usr/src/app/package.json
else
	cp /usr/src/app/package_arm64.json /usr/src/app/package.json
fi
cd /tmp
rm -rf /tmp/chromiumGateway/
tar xzvf chromiumGatewayConfig.tgz
chown $ENV_USER:$ENV_USER /tmp/chromiumGateway -R

cd /usr/src/app
echo now running npm install
su -c "npm install" 

chmod 777 xdotooldisp
chmod 777 updateKimbo
chmod 777 updateNodeApp
chmod 777 launchChromiumAsUser
chmod 777 killGateway
chmod 777 launchChromium
chmod 777 launchGateway
chmod 777 updateAllSystem
chmod 777 updateAllSubsystems


echo =====================
echo === upper setup =====
echo =====================

mkdir /usr/src/app/vapidKeys
mkdir /usr/src/app/webui/keys

ln -s /run/secrets/testingAgent-key.pem /usr/src/app/webui/keys/key.pem
ln -s /run/secrets/testingAgent-cert.pem /usr/src/app/webui/keys/cert.pem


echo Creating Link /usr/src/app/main.log /home/$ENV_USER/main.log
touch /usr/src/app/main.log
#ls -lisa /usr/src/app/m*
chmod 666 /usr/src/app/main.log
#ls -lisa /usr/src/app/m*
ln -s "/usr/src/app/main.log" "/home/$ENV_USER/main.log"
#ls -lisa "/home/$ENV_USER/main.log"

rm -rf /usr/src/app/features/kimboapp

cd /tmp
rm -rf "/tmp/$APP_NAME"
echo "Clonning $APP_GITSERVER/$APP_NAME.git"
git clone "$APP_GITSERVER/$APP_NAME.git"
chown "$ENV_USER:$ENV_USER" /tmp/$APP_NAME -R
cp -RT /tmp/$APP_NAME/kimboapp /usr/src/app/features/kimboapp

chmod 777 /usr/src/app/features/kimboapp/updateKimboApp
ln -s "/usr/src/app/features/kimboapp/updateKimboApp" "/usr/src/app/updateKimboApp"
chmod 777 /usr/src/app/updateKimboApp

chown "$ENV_USER:$ENV_USER" /usr/src/app -R

cd /usr/src/app

echo "updating all subsystems"
su -c /usr/src/app/updateAllSubsystems $ENV_USER



 
