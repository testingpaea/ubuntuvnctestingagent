var PushEventHandler=class PushEventHandler{
	constructor(){
		var self=this;
		self.eventsRegistry=new Map();
	}
	raise(actionName,actionValues){
		var self=this;
		var bWithHandler=self.eventsRegistry.has(actionName);
		log("Raised event:"+actionName+" with handler:"+bWithHandler+" data:"+JSON.stringify(actionValues))
		if (bWithHandler){
			self.eventsRegistry.get(actionName)(actionName,actionValues);
		}
	}
	setHandler(actionName,fncEventManage){
		var self=this;
		self.eventsRegistry.set(actionName,fncEventManage);
	}
}
var pushEventHandler=new PushEventHandler();
