var accServerUrl="";
var isGateway="";
var webuiId="";
var now=function now(){
    return Date.now();

}
var initTime=now();
var url_string = window.location.href;
var url = new URL(url_string);
var accServerUrl=url.searchParams.get("accServer");
if ((typeof accServerUrl==="undefined")
	||(accServerUrl==="")
	||(accServerUrl==null)){
	accServerUrl="http://localhost:8080";
}
var isGateway=url.searchParams.get("gateway");
if ((typeof isGateway==="undefined")
	||(isGateway==="")
	||(isGateway==null)){
	isGateway=false;
} else {
	isGateway=(isGateway=="true");
}

var nServiceWorker="";
var fncUpdateDetails=async function(){
	const response = await fetch(accServerUrl+'/systemDetails');
	var systemDetails=await response.text();
	var oConfig=JSON.parse(systemDetails);
	var appTitle=oConfig.title+" "+(isGateway?"Gateway":"Webui");
	document.title = appTitle;
	nServiceWorker=oConfig.nServiceWorker;
	$("#APPHeader").text(appTitle);
	if (isGateway) {
		$("#lblWebuiType").text("Gateway");
	} else {
		$("#lblWebuiType").text("Webui");
	}
	$("#btnRegister").hide();
	$("#btnUnregister").hide();
    $('<iframe />', {
        name: 'frame1',
        id: 'frame1',
        src: accServerUrl
    }).appendTo('body');
}
fncUpdateDetails();

var theSubscription="";
var lastMsgTime=0;
var idMsgSended=0;
function isGatewayHasComms(){
	return ((now()-lastMsgTime)<(1000*60*5));	
}


var serviceWorkerRegistration="";
// Register a Service Worker.

if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('service-worker'+nServiceWorker+'.js').then(function(registration){
		//navigator.serviceWorker.register(accServerUrl+'/scripts/sw.js').then(function(registration){
			serviceWorkerRegistration=registration;
		});
	});
}

navigator.serviceWorker.ready
.then(function(registration) {
  // Use the PushManager to get the user's subscription to the push service.
  return registration.pushManager.getSubscription()
  .then(async function(subscription) {
    // If a subscription was found, return it.
    if (subscription) {
      return subscription;
    }

    // Get the server's public key	
    const response = await fetch(accServerUrl+'/vapidPublicKey');
    //const response = await fetch(accServerUrl+'/scripts/applicationServerPublicKey.js');
    const vapidPublicKey = await response.text();
    log("vapid:"+vapidPublicKey);
    // Chrome doesn't accept the base64-encoded (string) vapidPublicKey yet
    // urlBase64ToUint8Array() is defined in /tools.js
    const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);

    // Otherwise, subscribe the user (userVisibleOnly allows to specify that we don't plan to
    // send notifications that don't have a visible effect for the user).
    return registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: convertedVapidKey
    });
  });
}).then(function(subscription) {
  // Send the subscription details to the server using the Fetch API.
  log("Here the sending of subscription....");
  log(JSON.stringify(subscription));
  theSubscription=subscription;
  /*fetch('./register', {
    method: 'post',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({
      subscription: subscription
    }),
  });*/
  document.getElementById('doIt').onclick = function() {
	    const delay = document.getElementById('notification-delay').value;
	    const ttl = document.getElementById('notification-ttl').value;
	    sendAliveAsk(delay,ttl,true);
  };
  var fncWaitForGatewayInitializationMessage=function(){
	  if (!isGatewayHasComms()){
		  sendAliveAsk(5,0);
		  var timeToFail=Math.round((5*60)-((now()-initTime)/1000));
		  if (timeToFail<0){
			  log("Trying to unregister");
			  serviceWorkerRegistration.unregister().then(function(wasUnregistered) {
				  if (wasUnregistered){
					  log("Sucesfully unregistered");
				  } else {
					  log("Can not unregister");
				  }
				  location.reload(true);
			  });  
		  } else {
			  $("#lblCommsStatus").text("Checking comms with the gateway...Time to Restart:"+timeToFail+" seconds");
			  setTimeout(fncWaitForGatewayInitializationMessage,30000);
		  }
	  } else {
		  sendAliveAsk(5,0,true);
		  log("There are comms with the gateway");
		  setTimeout(fncWaitForGatewayInitializationMessage,60000);
	  }
  }
  
  fncWaitForGatewayInitializationMessage();

});
async function sendAliveAsk(delay,ttl,withAck){
	idMsgSended++;
	var msgToSend={
	        subscription: theSubscription,
	        delay: delay,
	        ttl: ttl,
	        webuiId:webuiId,
	        msgId: idMsgSended,
	        msgType: "B2G-CHECKINGCOMMS",
	        msgContent:"Are you connected?",
	        msgNeedsAck:withAck
	      };
	log("Sending Msg:"+idMsgSended+" "+JSON.stringify(msgToSend));
    // Ask the server to send the client a notification (for testing purposes, in actual
    // applications the push notification is likely going to be generated by some event
    // in the server).
    let response = await fetch(accServerUrl+'/sendNotification', {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(msgToSend),
    });
	let oResult = await response.json();
	if (oResult.result=="ERROR"){
		log("Error sending notification");
	} else {
		webuiId=oResult.webuiId;
	}
	var timeToFail=Math.round((5*60)-((now()-initTime)/1000));
	$("#lblCommsStatus").text("Checking Comms with "+webuiId+" time to fail:"+timeToFail);
}

if('serviceWorker' in navigator){
    // Handler for messages coming from the service worker
    navigator.serviceWorker.addEventListener('message', function(event){
		var sData="Browser Received Message from Service Worker. "; 
		var data=event.data;
    	if (typeof data!=="string"){
    		sData+="Object:"+ JSON.stringify(data);
    	} else {
    		sData+="String:"+data;
    		data=JSON.parse(data);
    	}
        log(sData);
        if (data.msgType=="SW-PUSHRECEIVED"){
        	var bWasComms=isGatewayHasComms();
        	lastMsgTime=now();
        	//if (!bWasComms){
        		$("#lblCommsStatus").text("Stabilized");
        		//log("Comms reactivated");
        		pushEventHandler.raise("SW-STABILIZED");
        	//}
        	var msgContent=JSON.parse(data.msgContent);
        	if (msgContent.msgType=="B2G-CHECKINGCOMMS"){
        		log("Response of COMMS alive ask");
        		pushEventHandler.raise("SW-COMMSWORKING",msgContent);
        	} else if ((typeof msgContent.msgDestination==="undefined")
        			||(msgContent.msgDestination==webuiId)
        			||(msgContent.msgDestination=="ALL")){
        			pushEventHandler.raise(msgContent.msgType,msgContent);
        	}
        } else if (data.msgType=="SW-ALIVE"){
        	log("The Gateway is alive... with comms:"+isGatewayHasComms());
    		pushEventHandler.raise("SW-GATEWAYLIVES");
        }
    });
}
$.getScript(accServerUrl+"/scripts/uiLoader.js");
